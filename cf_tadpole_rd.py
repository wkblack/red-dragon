########################################################################
# compare tadpole and red dragon number counts

import theta_tools as tt
import rd_beta

from matplotlib import pyplot as plt
import numpy as np

########################################################################

out_fname = 'cf_tt_rd.npy'

def count_up(z_vals=[.1,.5,.699],dZ=.001):
  " count differences btw datasets "
  m_g,m_r,m_i,m_z,zz = rd_beta.read_fields()
  Z_vals,N_tt,N_rd = np.zeros((3,len(z_vals)))
  
  for ii in range(len(z_vals)):
    print("z|[%0.3f,%0.3f)" % (z_vals[ii],z_vals[ii]+dZ))
    mask_Z = (z_vals[ii]<=zz) * (zz<z_vals[ii]+dZ)
    g,r,i,z,Z = [field[mask_Z] for field in [m_g,m_r,m_i,m_z,zz]]
    Z_vals[ii] = np.median(Z)
    N_tt[ii] = np.sum(tt.P_red(Z,[g,r,i]))
    N_rd[ii] = np.sum(rd_beta.P_red(Z,[g,r,i,z]))
  
  np.save(out_fname,[Z_vals,N_tt,N_rd])


def plot_up():
  " plot up differences "
  Z_vals,N_tt,N_rd = np.load(out_fname)
  plt.subplot(1,2,1)
  plt.scatter(Z_vals,N_tt,5)
  plt.scatter(Z_vals,N_rd,5)
  plt.xlabel('Redshift')
  plt.ylabel('Number counts')
  plt.yscale('log')
  
  plt.subplot(1,2,2)
  plt.scatter(Z_vals,N_rd/N_tt,5)
  plt.xlabel('Redshift')
  plt.ylabel('red dragon counts / tadpole counts')
  
  plt.tight_layout()
  plt.show()


if __name__=='__main__':
  if 0: 
    print("Counting")
    count_up(z_vals=np.arange(.05,.7,.001))
  if 1: 
    print("Plotting")
    plot_up()
  print("~fin")

