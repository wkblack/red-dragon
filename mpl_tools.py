########################################################################
# set up matplotlib and tools to aid it

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# set up golden aspect ratio
import numpy as np
golden_ratio = (1+np.sqrt(5))/2.
golden_aspect = 5*np.array([golden_ratio,1])
double_height = np.array([1,2]) * golden_aspect
double_width = np.array([2,1]) * golden_aspect

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# set up pyplot defaults
import matplotlib
from matplotlib import pyplot as plt
plt.rcParams['errorbar.capsize'] = 2
plt.rcParams['image.cmap'] = 'cividis'
plt.rcParams['figure.autolayout'] = 'True'
plt.rcParams['figure.figsize'] = golden_aspect
plt.rcParams['font.size'] = 15
plt.rcParams['savefig.dpi'] = 1000
plt.rcParams['savefig.format'] = 'pdf'
plt.rcParams['scatter.edgecolors'] = 'none'
# grab color data
mpl_colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
ones = np.ones(2) # handly little shortcut

from mpl_toolkits.axes_grid1 import make_axes_locatable
""" # make colorbars for subplots
divider = make_axes_locatable(ax1)
cax = divider.append_axes('right', size='5%', pad=0.05)
fig.colorbar(im1, cax=cax, orientation='vertical')
"""

