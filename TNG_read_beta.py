# read in vetted (L > 0.2 L*) TNG300-1 LGalaxies catalog

########################################################################
# imports

import h5py
import numpy as np

from matplotlib import pyplot as plt

# cosmology
h = .7 # unitless Hubble constant
H0 = 100 * h # (km/s)/Mpc
c = 299792.458 # km/s


########################################################################
# import file & fields

fname = '/global/cscratch1/sd/wkblack/TNG300-1/lgalaxies/LGalaxies_067_vetted.hdf5'

Mf = 10**10/h # Msol, factor all masses here are divided by

with h5py.File(fname,'r') as ds:
  # read in attributes
  Z = ds.attrs['Z']
  emstar = ds.attrs['m_star (absolute)']
  m_star = ds.attrs['m_star (apparent)']
  mu = ds.attrs['mu']
  # read in main fields
  bands = g,r,i,z = np.transpose(ds['Mag'][:,16:20])
  SFR = ds['StarFormationRate'][:] # Msol / yr
  M_star = ds['StellarMass'][:] * Mf # Msol
  sSFR = SFR / M_star # 1/yr; specific star formation rate
  # read in auxilliary fields
  cmc = ds['Central_M_Crit200'][:] * Mf # Msol
  mc = ds['M_Crit200'][:] * Mf # Msol
  rmc = ds['Central_R_Crit200'][:] / h # ckpc
  rc = ds['R_Crit200'][:] / h # ckpc

bands_true = gt,rt,it,zt = bands + mu


########################################################################
# functions


def agm(a,b,tol=1e-10):
  ii = 0
  while np.abs(a-b) > tol:
    ii += 1
    a,b = (a+b)/2., np.sign(a) * np.sqrt(a*b)
    assert(ii < 16)
  return (a+b)/2.


def measure_GV(plotting=False):
  " return estimate on dividing sSFR value "
  assert(1==0) # this won't work, due to the m* limit in this sample.
  x = np.log10(sSFR[sSFR>0])
  if plotting:
    # plot layered histogram of sSFR value
    for ii in range(10):
      plt.hist(x,2**ii,alpha=.5)
    plt.show()
  from sklearn.mixture import GaussianMixture
  mns_0 = np.flip([-12.5,-10]).reshape(-1,1)
  gmm = GaussianMixture(2,means_init=mns_0)
  x_pred = np.array(gmm.fit_predict(x.reshape(-1,1))).astype(bool)
  assert(np.mean(x[x_pred]) < np.mean(x[~x_pred]))
  a,b = max(x[x_pred]), min(x[~x_pred])
  return agm(a,b)


########################################################################
# synthetic error formulation

def simulate_error(bands):
  """
  return synthetic errors based on mean error at a given magnitude
  
  Parameters
  ----------
  bands = [g, r, i, z] (true apparent magnitude)
  
  Returns
  -------
  errors = [g_err, r_err, i_err, z_err] mag
  """
  a = 0.017 # pm .004; quadratic slope of log error
  b = .295 # pm .01; linear slope of log error
  c = [-1.350, -1.295, -1.140, -0.937] # pm <= .005; log error at ZP
  c = np.outer(c,np.ones(len(bands[0])))
  x = bands - ZP
  return 10**(a*x**2 + b*x + c)


def apply_error(bands):
  " approximate error as Gaussian normal about zero "
  err_mag = simulate_error(bands)
  return bands + np.random.normal(0,err_mag)


bands_err = ge,re,ie,ze = simulate_error(bands_true)
bands_obs = go,ro,io,zo = apply_error(bands_true)


########################################################################
# plots

lg_sSFR_label = r'$\log_{10} {\rm sSFR \cdot yr}$'

def plot_CM():
  plt.scatter(i,g-r,1,c=sSFR<10**-11,cmap='coolwarm',lw=0,alpha=.5)
  plt.xlabel('i')
  plt.ylabel('g-r')
  plt.tight_layout()
  plt.show()


def plot_CC():
  plt.scatter(r-i,g-r,1,c=sSFR<10**-11,cmap='coolwarm',lw=0,alpha=.5)
  plt.xlabel('r-i')
  plt.ylabel('g-r')
  plt.tight_layout()
  plt.show()


def plot_CC_CM(errors=False):
  i_max = m_star - 2.5 * np.log10(0.2) # ~ m_* + 1.75
  if errors:
    gx,rx,ix,zx = bands_obs
  else:
    gx,rx,ix,zx = bands_true
  fig, axs = plt.subplots(1, 2, sharey=True) # gridspec_kw={'width_ratios': [1, 2]}
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # plot up CC space
  axs[0].scatter(rx-ix,gx-rx,1,lw=0,alpha=.125,
                 c=sSFR<10**-11,cmap='coolwarm') 
  axs[0].set_xlabel(r'$(r-i)$')
  # axs[0].set_xlim(.1,1.2)
  x_qtls = np.quantile(rx-ix,[.01,.99])
  axs[0].set_xlim(x_qtls[0]-.1,x_qtls[1]+.1)
  axs[0].set_ylabel(r'$(g-r)$')
  # axs[0].set_ylim(.5,3.5)
  y_qtls = np.quantile(gx-rx,[.01,.99])
  axs[0].set_ylim(y_qtls[0]-.1,y_qtls[1]+.1)
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # plot up CM space
  axs[1].scatter(ix,gx-rx,1,lw=0,alpha=.125,
                 c=sSFR<10**-11,cmap='coolwarm')
  axs[1].set_xlabel(r'$m_i$')
  x_qtl = np.quantile(ix,.001)
  axs[1].set_xlim(x_qtl-.1,i_max)
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # tidy up and display
  plt.tight_layout()
  plt.show()


def plot_hist(pivot=-11):
  x = np.log10(sSFR[sSFR>0])
  plt.hist(x,100,log=True,histtype='step',color='k')
  # set up axes
  plt.xlabel(lg_sSFR_label)
  plt.show()


if __name__ == '__main__':
  # plot_CM()
  # plot_CC()
  # plot_hist()
  pass
