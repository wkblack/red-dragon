# program for testing the fitting of Red Dragon
# using TNG quenched boolean

########################################################################
# imports

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# remove deprecation warnings
import warnings # to shut up sklearn
warnings.filterwarnings("ignore",category=DeprecationWarning)

exec(open("mpl_tools.py").read()) # import matplotlib tools and defaults
exec(open("kllr_tools.py").read()) # import KLLR and tools to aid it

import h5py
import pandas as pd

from glob import glob
from m_star_model import m_star as R14
from sklearn.mixture import GaussianMixture as GM

import os 
from os.path import isfile
os.environ['OPENBLAS_NUM_THREADS'] = '1'

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# cosmology bits
h = .7 # unitless Hubble constant
H0 = 100 * h # (km/s)/Mpc
c = 299792.458 # km/s


########################################################################
# fitting functions

lbl_lg_sSFR = r'$\log_{10} \, {\rm sSFR} \cdot {\rm yr}$'
lbl_lg_DOR = r'$\log_{10} {\rm DOR}$'
lbl_YJS = r'$\mathcal{J}_{\rm Youden}$'
lbl_ACC = r'Accuracy'
lbl_BA = r'Balanced Accuracy'

def score(set1,set2): 
  " returns percent incorrectly matched (zero is best score) "
  assert(len(set1)==len(set2))
  return np.sum(np.abs(set1.astype(int)-set2.astype(int)))/len(set1)


def cc_line_mask(cx,cy,inv_sl,x0,y0):
  """
  returns mask for CC fitting
  cx,cy = color on x & y axis
  inv_sl = inverse slope of fit line
  x0,y0 = intercept point of fit line, between components
  """
  dx,dy = (cx - x0), (cy - y0)
  return np.sign(inv_sl) * np.sign(dx) * (dy/dx-1/inv_sl) < 0


def fit_CC(data,use_sSFR=None,plotting=False,fit_clean=False,optimize=True,
           fname_out='parameters/CC_fit.h5',N_fit=3):
  " Draw hard line through color--color space "
  if (use_sSFR is not None) and fit_clean: # clean selection of data
    mrr,mrd,msr,mrb = make_masks(data,use_sSFR)
    mask = mrr + mrb # select only those w/ sSFR > 1 sigma from pivot
  else:
    mask = np.ones(len(data[data["cols"][0]])).astype(bool)
  bands = [data[c][mask] for c in data["cols"]]
  X = np.transpose(-np.diff(bands,axis=0))
  # rate how far each point is from the mean
  mn,sg = [fxn(X,axis=0) for fxn in [np.mean,np.std]]
  sigs = (X-mn)/sg # std dev away from means
  sig_tot = np.sqrt(np.sum(sigs**2,axis=1))
  # fit with Gaussian Mixture for first pass
  fit = GM(N_fit).fit(X)
  pred = fit.predict(X)
  sort = np.argsort([max(sig_tot[pred==ii]) for ii in range(N_fit)])
  # find means and movement
  mns = np.array([fit.means_[ii] for ii in sort])
  if 1: # use equal likelihood point between GM means
    X_line = np.linspace(mns[0],mns[1],1000) # 100 is plenty
    Xl_pred = fit.predict(X_line)
    idx = np.where(Xl_pred==sort[0])[0].max()
    mn_mn = X_line[idx]
  else: # take mean between GM means
    mn_mn = np.mean(mns[:2],axis=0) # mean position between G means
  df_mn = np.diff(mns,axis=0)[0] # gains between means
  assert(df_mn[1] != 0) # avoid divide by zero
  slope = df_mn[2]/df_mn[1] # slope of line between means
  sl0 = -1/slope # 90 degree opposite slope
  prs = np.array([1/sl0,mn_mn[1]]) # default pars
  if optimize: # make fit parameters as good as possible
    if use_sSFR is None:
      truth = pred==sort[0] # use prediction from GMM
    else:
      truth = data['sSFR'][mask] < use_sSFR
    def get_score(pars):
      mask = cc_line_mask(X[:,1],X[:,2],*pars,mn_mn[2])
      return Classification(truth,mask).get_YJS()
    # set up grid
    dx = 5
    dy = .1
    for ii in range(3): # zoom in to best fit
      xlim = prs[0]-dx,prs[0]+dx
      ylim = prs[1]-dy,prs[1]+dy
      x,y = np.mgrid[xlim[0]:xlim[1]:25j,ylim[0]:ylim[1]:25j]
      coords = np.vstack((x.ravel(),y.ravel())).T
      out = np.array([get_score(c) for c in coords])
      optimal = coords[out==max(out)][0]
      if plotting: # show minimized position
        plt.imshow(np.transpose(np.reshape(out,(25,25))),
                   origin='lower',extent=[*xlim,*ylim],aspect='auto')
        plt.scatter(prs[0],prs[1],color='k',marker='o')
        plt.scatter(optimal[0],optimal[1],color='k',marker='x')
        plt.xlim(plt.xlim())
        plt.ylim(plt.ylim())
        plt.scatter(1/sl0,mn_mn[1],color='r',marker='.') # allow offscreen
        plt.colorbar(label=lbl_YJS)
        plt.xlabel(r'inverse slope')
        plt.ylabel(r'$c_{x,0}$')
        plt.show()
      dx /= 3
      dy /= 3
      prs = optimal
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  if plotting: # plot results
    for ii in range(N_fit):
      col = ['r','b','g'][ii]
      jj = pred==sort[ii] # mask data to current element
      plt.scatter(X[jj,1],X[jj,2],2,alpha=.25,lw=0,color=col)
      plt.scatter(mns[ii,1],mns[ii,2],50,color='k',marker='o')
      plt.scatter(mns[ii,1],mns[ii,2],30,color=col,marker='x')
    plt.scatter(mn_mn[1],mn_mn[2],color='k',marker='x')
    plt.xlabel(r'$g-r$')
    xlim = plt.xlim(np.quantile(X[:,1],[.01,.99]))
    xv = np.linspace(*xlim)
    plt.plot(xv,(xv-prs[1])/prs[0]+mn_mn[2],'k')
    plt.ylabel(r'$r-i$')
    plt.ylim(np.quantile(X[:,2],[.01,.99]))
    plt.title(data["lbl_Z"])
    plt.show()
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # save fit parameters, details about redshift, m_star, etc. 
  with h5py.File(fname_out,'w') as ds:
    ds.attrs['Z'] = data['Z_mid']
    ds.attrs['dset'] = data['dset']
    ds.attrs['use_sSFR'] = use_sSFR if use_sSFR is not None else -1
    ds.attrs['x0'] = prs[1]
    ds.attrs['y0'] = mn_mn[2]
    ds.attrs['isl'] = prs[0]
    ds.attrs["Note"] = "output parameter fit for CC fitting"
    ds.attrs["Equation"] = "[r-i] - y0 = ([g-r] - x0) / isl"
    ds.attrs["pars"] = prs[0], prs[1], mn_mn[2]
    ds.attrs["classification"] = -np.ones(4) # TP, FP, FN, TN
  print("Saved",fname_out)


def cm_line_mask(mx,cy,sl,x0,y0):
  """
  returns mask for CM fitting
  mx,cy = magnitude (x-axis) and color (y-axis)
  sl = slope
  x0,y0 = intercept at m_star, color pivot between RS & BC there
  """
  return cy > (mx - x0) * sl + y0


def fit_CM(data,use_sSFR=None,plotting=False,fit_clean=False,optimize=True,
           fname_out='parameters/CM_fit.h5',N_fit=3):
  " Draw hard line through color--magnitude space "
  bands = [data[c][:] for c in data["cols"]]
  X = np.transpose(-np.diff(bands,axis=0))
  # rate how far each point is from the mean
  mn,sg = [fxn(X,axis=0) for fxn in [np.mean,np.std]]
  sigs = (X-mn)/sg # std dev away from means
  sig_tot = np.sqrt(np.sum(sigs**2,axis=1))
  # fit with Gaussian Mixture for first pass
  fit = GM(N_fit).fit(X)
  pred = fit.predict(X)
  sort = np.argsort([max(sig_tot[pred==ii]) for ii in range(N_fit)])
  # find means
  mns = np.array([fit.means_[ii] for ii in sort])
  mn_mn = np.mean(mns[:2],axis=0) # mean position between G means
  # find slopes and scatters, accounting for errors
  b_err = [data['err_'+c][:] for c in data["cols"]] # photometric errors
  tot_err = np.sqrt(np.sum(np.array(b_err)**2,axis=0)) # total photo err
  x0 = data['m_star'] - 2.5 * np.log10(.4) # decrease fit covariance
  x = bands[3] - x0 # i-band
  cy = X[:,1] # g-r
  # red sequence stats
  mask_RS = pred == sort[0] # RS
  zf_RS = np.polyfit(x[mask_RS],cy[mask_RS],1,w=1/tot_err[mask_RS]) # linear fit
  mr = slope_red = zf_RS[0] # apx slope of red seq
  br = offset_red = zf_RS[1] # offset of RS at m_*
  sr = scatter_red = np.std(cy[mask_RS] - np.poly1d(zf_RS)(x[mask_RS]))
  # blue cloud stats
  mask_BC = pred == sort[1] # BC
  zf_BC = np.polyfit(x[mask_BC],cy[mask_BC],1,w=1/tot_err[mask_BC]) # linear fit
  mb = slope_blue = zf_BC[0] # apx slope of red seq
  bb = offset_blue = zf_BC[1] # offset of BC at m_*
  sb = scatter_blue = np.std(cy[mask_BC] - np.poly1d(zf_BC)(x[mask_BC]))
  if optimize: 
    # combine to find pivot point
    midpoint = (br/sr**2+bb/sb**2)/(1/sr**2+1/sb**2) # Gaussian summing
    sig_mid = (br-bb)/(sb+sr) # proportions of each sigma to get to midpt
    midpoint = br - sig_mid * sr # midpoint using same sigmas
    y0 = midpoint
    m0 = (mr/sr**2+mb/sb**2)/(1/sr**2+1/sb**2) # Gaussian summing
  else:
    y0 = br - 2 * sr # Hao+09 selection, 2sig below RS mean
    m0 = mr # just use RS slope, as w/ Hao+09
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  # fit parameters
  prs = m0,y0
  if optimize: # calculate best pars
    if use_sSFR is None:
      truth = pred==sort[0] # use prediction from GMM
    else:
      truth = data['sSFR'][:] < use_sSFR # use sSFR values
    def get_score(pars):
      mask = cm_line_mask(bands[3],cy,pars[0],x0,pars[1])
      return Classification(truth,mask).get_YJS()
    # set up grid
    dx,dy = .2,.15
    for ii in range(3): # zoom in on best fit
      xlim = prs[0]-dx,prs[0]+dx
      ylim = prs[1]-dy,prs[1]+dy
      gx,gy = np.mgrid[xlim[0]:xlim[1]:25j,ylim[0]:ylim[1]:25j]
      coords = np.vstack((gx.ravel(),gy.ravel())).T
      out = np.array([get_score(c) for c in coords])
      optimal = coords[out==max(out)][0]
      if plotting: # show minimized position
        plt.imshow(np.transpose(np.reshape(out,(25,25))),
                   origin='lower',extent=[*xlim,*ylim],aspect='auto')
        plt.scatter(prs[0],prs[1],color='k',marker='o')
        plt.scatter(optimal[0],optimal[1],color='k',marker='x')
        plt.xlim(plt.xlim())
        plt.ylim(plt.ylim())
        plt.scatter(m0,y0,color='r',marker='.') # allow offscreen
        plt.colorbar(label=lbl_YJS)
        plt.xlabel(r'slope')
        plt.ylabel(r'$c_{y,0}$')
        plt.show()
      dx /= 3
      dy /= 3
      prs = optimal
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  if plotting:
    for ii in range(N_fit):
      col = ['r','b','g'][ii]
      jj = pred==sort[ii] # mask data to current element
      plt.scatter(x[jj]+x0,cy[jj],2,alpha=.25,lw=0,color=col)
    plt.xlabel(r'$m_i$')
    xlim = plt.xlim(np.quantile(x[jj]+x0,[.001,.99]))
    if 1: # plot lines and scatters for RS & BC
      xv = np.linspace(*xlim)
      plt.plot(xv,(xv-x0)*m0+y0,'r--',lw=.5)
      plt.plot(xv,(xv-x0)*prs[0]+prs[1],'k')
      for col,zf,sig in zip(['r','b'],[zf_RS,zf_BC],
                            [scatter_red,scatter_blue]):
        yf = np.poly1d(zf)(xv-x0)
        plt.plot(xv,yf,col)
        plt.fill_between(xv,yf-sig,yf+sig,color=col,lw=0,alpha=.125)
    plt.ylabel(r'$g-r$')
    ylim = plt.ylim(np.quantile(cy[jj],[.0001,.9999]))
    plt.plot(ones*data["m_star"],ylim,'r--',lw=.5)
    plt.plot(ones*x0,ylim,'grey',lw=.5)
    plt.show()
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # save fit parameters, details about redshift, m_star, etc. 
  with h5py.File(fname_out,'w') as ds:
    ds.attrs['Z'] = data['Z_mid']
    ds.attrs['dset'] = data['dset']
    ds.attrs['use_sSFR'] = use_sSFR if use_sSFR is not None else -1
    ds.attrs['x0'] = x0
    ds.attrs['y0'] = prs[1]
    ds.attrs['sl'] = prs[0]
    ds.attrs["Note"] = "output parameter fit for CM fitting"
    ds.attrs["Equation"] = "[g-r] = sl * (m_i - x0) + y0"
    ds.attrs["pars"] = prs[0], x0, prs[1]
    ds.attrs["classification"] = -np.ones(4) # TP, FP, FN, TN
  print("Saved",fname_out)


def fit_test_GM(data,N_fit=2,pivot_sSFR=10**-11.5,plotting=False,optimize=False,
                fname_out='parameters/GM_fit.h5',use_errors=False,use_secondaries=False):
  " fit data in multi-color space "
  bands = [data[c] for c in data["cols"]]
  if not use_secondaries:
    # use only primary colors
    X = np.transpose(-np.diff(bands,axis=0))
  else:
    # use secondary colors also
    u,g,r,i,z = bands
    X1 = np.transpose([u-g,g-r,r-i,i-z]) # -np.diff(bands,axis=0))
    X2 = np.transpose([u-r,u-i,u-z,g-i,g-z,r-z])
    X = np.concatenate([X1,X2],axis=1)
  if optimize:
    assert(len(bands)==5) # this was written for SDSS photometry
    ls = lg_sSFR = np.log10(data['sSFR'])
    values = np.transpose([-ls])
    X = np.append(X,values,axis=1)
  # fit with Gaussian Mixture for first pass
  if use_errors and not use_secondaries:
    print("Error inclusive predicting...")
    from rd_gamma import _band_err_to_covar as get_covar
    band_err = np.transpose([data['err_'+c] for c in data['cols']])
    assert(np.all(band_err>0))
    covar = get_covar(band_err)
    from test_GM_similarity import pygmmis_fit_predict as fit_predict
    pred = fit_predict(X,covar,N_fit)
  else:
    print("Error ignorant predicting...")
    fit = GM(N_fit).fit(X)
    pred = fit.predict(X)
  if optimize:
    # sort by lowest median sSFR
    sort = np.argsort([np.median(ls[pred==ii]) for ii in range(N_fit)])
  elif False:
    # use distance from mean + scatter to determine RS
    # rate how far each point is from the mean
    mn,sg = [fxn(X,axis=0) for fxn in [np.mean,np.std]]
    sigs = (X-mn)/sg # std dev away from means
    sig_tot = np.sqrt(np.sum(sigs**2,axis=1))
    # call that which is closer to the mean the RS
    sort = np.argsort([max(sig_tot[pred==ii]) for ii in range(N_fit)])
  else:
    medians = [np.median(X[pred==jj],axis=0) for jj in range(N_fit)]
    M = np.linalg.norm(medians,axis=1) # distance to color vector median
    sort = np.flip(np.argsort(M))
  if plotting:
    for ii in range(N_fit):
      plt.scatter(X[pred==sort[ii],1],X[pred==sort[ii],2],3,lw=0,
                  color=['r','g','b','y'][ii],alpha=.5/N_fit)
    plt.xlabel(r'$g-r$')
    xlim = plt.xlim(np.quantile(X[:,1],[.01,.99]))
    plt.ylabel(r'$r-i$')
    ylim = plt.ylim(np.quantile(X[:,2],[.01,.99]))
    plt.show()
    if 1: # plothistograms of sSFR
      sSFR = data['sSFR']
      for N_bins in [25,50]:
        bins = np.linspace(-13,-9,N_bins+1)
        for ii in range(N_fit):
          plt.hist(np.log10(sSFR[pred==ii]),bins,log=True,
                   histtype='step',color=mpl_colors[ii])
      plt.show()
  # classify and save output
  if False and N_fit == 3:
    # fitting for first two components as RS
    print("combining first two components for RS")
    P = np.array((pred == sort[0]) + (pred == sort[1])).astype(bool)
  else:
    P = pred == sort[0] # RS selection
  T = data["sSFR"] < pivot_sSFR
  c = Classification(T,P)
  v_RS = c.get_values()
  with h5py.File(fname_out,'w') as ds:
    ds.attrs['Z'] = data['Z_mid']
    ds.attrs['dset'] = data['dset']
    ds.attrs['use_sSFR'] = pivot_sSFR
    ds.attrs["Note"] = "output parameter fit for CM fitting"
    ds.attrs["Equation"] = "Gaussian Mixture Model"
    ds.attrs["pars"] = N_fit
    ds.attrs['classification'] = v_RS
  print("Saved",fname_out)


########################################################################
# classification methods

class Classification:
  " class to hold classifications of Truth "
  def __init__(self,T,P=None):
    # set fundamental counts 
    if P is not None: # read in from truth vs positives masks
      T = np.array(T).astype(bool)
      P = np.array(P).astype(bool)
      self.TP = len(T[T*P]) # count of true positives
      self.FP = len(T[(~T)*P]) # type I error
      self.FN = len(T[T*(~P)]) # type II error
      self.TN = len(T[(~T)*(~P)]) # count of true negatives
    else: # set up from 4-par input
      self.TP,self.FP,self.FN,self.TN = T
  
  def get_values(self):
    " return [TP,FP,FN,TN] "
    return np.array([self.TP, self.FP, self.FN, self.TN])
  
  def get_TPR(self,err=0):
    " return true positive rate, aka sensitivity / recall "
    return (self.TP + err * np.sqrt(self.TP)) / (self.TP + self.FN \
            - err * (np.sqrt(self.FN)) + np.sqrt(self.TP))
  
  def get_FPR(self):
    " return false positive rate, aka independent of prevalence "
    return self.FP/(self.TN+self.FP)
  
  def get_FNR(self):
    " return false negative rate "
    return self.FN/(self.TP+self.FN)
  
  def get_TNR(self,err=0):
    " return true negative rate, aka specificity (SPC) "
    return (self.TN + err * np.sqrt(self.TN)) / (self.TN + self.FP \
            - err * (np.sqrt(self.FP)) + np.sqrt(self.TN))
  
  def get_YJS(self,err=0):
    " return Youden's J statistic : sensitivity + specificity - 1 "
    return self.get_TPR(err) + self.get_TNR(err) - 1
  
  def get_DOR(self,err=0):
    " return diagnostic odds ratio "
    if self.FN * self.FP == 0:
      raise Warning("null FN or FP value; cannot calculate DOR")
      # artificially prevent divide by zero errors
      dor = (self.TP/(self.FN+1)) * (self.TN/(self.FP+1))
      err_ln_dor = np.sqrt(1/self.TP + 1/(self.FN+1) \
                         + 1/self.TN + 1/(self.FP+1))
    else:
      dor = (self.TP/self.FN) * (self.TN/self.FP)
      err_ln_dor = np.sqrt(1/self.TP + 1/self.FN + 1/self.FP + 1/self.TN)
    return dor * np.exp(err*err_ln_dor)
  
  def get_F1(self,err=0):
    " return F1 score "
    tp = self.TP + err * np.sqrt(self.TP)
    fe = err * (np.sqrt(self.FP) + np.sqrt(self.FN))
    return tp / (tp + .5*(self.FP+self.FN + fe))
  
  def get_ACC(self,err=0): 
    " return accuracy "
    # if the population isn't 50--50, it'll overweight one or the other.
    eT = np.sqrt(self.TP) + np.sqrt(self.TN)
    eF = np.sqrt(self.FP) + np.sqrt(self.FN)
    return (self.TP + self.TN + err * eT) / (self.TP + self.TN + \
            self.FP + self.FN - err * (eT + eF))
  
  def get_BA(self,err=0):
    " return balanced accuracy (TPR+TNR)/2 "
    return (self.get_TPR(err) + self.get_TNR(err))/2.
  
  def get_WM1(self):
    " constructed stat, ignoring TN "
    return self.TP - np.abs(self.FP-self.FN)


########################################################################
# test fits

def test_CC_fit(data,fname='parameters/CC_fit.txt',pivot_sSFR=10**-11.5):
  # read in data
  bands = [data[c] for c in data["cols"]]
  cols = -np.diff(bands,axis=0) # u-g, g-r, r-i, i-z
  cx,cy = cols[1],cols[2] # g-r and r-i
  # make masks
  with h5py.File(fname,'r') as ds:
    pars = ds.attrs['pars']
  P = mask_fit = cc_line_mask(cx,cy,*pars) # fit from hard line cut
  T = mask_sSFR = data["sSFR"] < pivot_sSFR # 'truth' from sSFR
  # classify and save
  c = Classification(T,P)
  with h5py.File(fname,'a') as ds:
    ds.attrs['classification'] = c.get_values()
  print("Updated",fname,'to',c.get_values())


def test_CM_fit(data,fname='parameters/CM_fit.txt',pivot_sSFR=10**-11.5):
  with h5py.File(fname,'r') as ds:
    pars = ds.attrs['pars']
  # read in data
  bands = [data[c] for c in data["cols"]]
  cols = -np.diff(bands,axis=0) # u-g, g-r, r-i, i-z
  mx,cy = bands[2],cols[1]
  # form masks
  P = mask_fit = cm_line_mask(mx,cy,*pars) # fit from hard line cut
  T = mask_sSFR = data["sSFR"] < pivot_sSFR # 'truth' from sSFR
  # classify and save
  c = Classification(T,P)
  with h5py.File(fname,'a') as ds:
    ds.attrs['classification'] = c.get_values()
  print("Updated",fname,'to',c.get_values())


def test_GM_fit(data,pars):
  pass


def tests_combined(fnames,labels=None,title=None,N_cols=3):
  " display table of classifications for each fit "
  N_rows = np.ceil(len(fnames)/N_cols).astype(int) # number of rows
  fig,axs = plt.subplots(N_rows, N_cols, sharey=True, sharex=True, 
                         figsize=double_height)
  for ii in range(len(fnames)):
    ax = axs[ii//N_cols,ii%N_cols]
    with h5py.File(fnames[ii],'r') as ds:
      c = Classification(ds.attrs['classification'])
    out = [[c.get_TPR(),c.get_FPR()],[c.get_FNR(),c.get_TNR()]]
    lbls_out = [["TPR","FPR"],["FNR","TNR"]]
    # print(ii,"out:",np.round(out,4))
    other = [c.get_YJS(), c.get_DOR(), c.get_F1(), c.get_ACC()]
    print(ii,"other:",np.round(other,4))
    ax.imshow(np.transpose(out),cmap='coolwarm_r')
    ax.axis("off")
    if labels is not None:
      ax.set_title(labels[ii])
    # add text
    for jj in range(4):
      y,x = jj//2,jj%2
      ax.text(x,y,f"{np.round(100*out[x][y],1)}%",
              verticalalignment='center',
              horizontalalignment='center')
  for ii in range(len(fnames),N_cols*N_rows):
    # turn off unused axes
    ax = axs[ii//N_cols,ii%N_cols]
    ax.axis("off")
  plt.suptitle(title)
  plt.show()


def tests_combined_v2(fnames,labels=[],title=None,sig=1,printing=False,
                      kind=None,fname_out=None,which="bACC"):
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  # set up iteration
  R = np.arange(len(fnames))
  if kind is None:
    # assume three components of each
    colors = np.array(mpl_colors)[R//3]
  else:
    # group by colors using input integer list
    colors = np.array(mpl_colors)[np.array(kind)[R]]
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  # iterate through each file
  for ii in R:
    with h5py.File(fnames[ii],'r') as ds:
      c = Classification(ds.attrs['classification'])
    if printing and len(labels) > 0:
      print(labels[ii],'classification:',c.get_values())
    if which == "bACC":
      fxns = [c.get_BA]
      fmts = ["o"]
      lbl_y = lbl_BA
      offset = 0
    elif which == "TR":
      fxns = [c.get_TPR,c.get_BA,c.get_TNR]
      fmts = ["^","o","v"]
      lbl_y = "True rates"
      offset = -1
    else:
      print("ERROR: Unset 'which' value to plot")
      return -1
    for jj,fxn in enumerate(fxns): # ,c.get_YJS,c.get_F1,c.get_DOR]):
      vals = y_min,y,y_max = [fxn(v) for v in [-sig,0,+sig]]
      ye = np.transpose([np.diff(vals)])
      x = ii + .2 * (jj + offset)
      plt.errorbar(x,y,ye,color=colors[ii],fmt=fmts[jj],capsize=5)
      # print(x,y,ye)
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  # tidy up and display
  plt.title(title)
  plt.gca().set_xticks(R)
  plt.gca().set_xticklabels(labels,rotation=45)
  plt.ylabel(lbl_y)
  # TODO: change to percentages? 
  # set y bounds
  ylim = plt.ylim(np.clip(plt.ylim(),0,1))
  if 0: # softcode bounds
    ylim = plt.ylim(min(ylim[0],.8),1)
  elif 0: # hardcode bounds
    ylim = plt.ylim(.75,1)
  if fname_out is not None:
    plt.savefig(fname_out)
    print("Saved",fname_out)
  plt.show()


########################################################################
# tools to do pre-analysis of data

def plot_sSFR(data,N=80,plot_many=False,plot_err=False):
  sv = [data['sSFR'+v] for v in ['_min','','_max']]
  mask = np.product(sv,axis=0) > 0
  x_min,x,x_max = [np.log10(sx[mask]) for sx in sv]
  xlim = plt.xlim(min(min(x_min),min(x)),-8) # max(x_max))
  bins = np.linspace(*xlim,N)
  if plot_many:
    for ii in range(10):
      bins_ii = np.linspace(*xlim,2**ii)
      plt.hist(x,bins_ii,alpha=16/256,color='b',log=True)
  else:
    n,b,p = plt.hist(x,bins,color='k',histtype='step',log=True)
    if plot_err:
      plt.hist(x_min,bins,color='r',histtype='step',log=True)
      plt.hist(x_max,bins,color='b',histtype='step',log=True)
  # tidy up and display
  plt.xlabel(lbl_lg_sSFR)
  plt.ylim(None,len(x)) # set max at total count of data
  plt.tight_layout()
  plt.show()


def plot_CC(data,sSFR_bins=[],mu_bins=[]):
  bands = [data[c][:] for c in data["cols"]]
  lg_sSFR = np.log10(data['sSFR'])
  lg_sSFR_sig = data['sSFR_max'] - data['sSFR_min']
  mu = data['logMass']
  mu_sig = data['maxLogMass'] - data['minLogMass']
  if 1: 
    mask = lg_sSFR_sig > 0
    plt.hist(np.log10(lg_sSFR_sig[mask]),100,log=True)
    plt.title(f"and {len(mask[mask])} have zero error")
    plt.show()
  if 1:
    plt.hist(mu_sig,100,log=True); plt.show()
  pass


########################################################################
# tools to read in data

SDSS_col_names = ['u','g','r','i','z']

def read_SDSS_old():
  df = pd.read_csv('SDSS_Zp1.csv',skiprows=1)
  bands = u,g,r,i,z = [df[c].values for c in SDSS_col_names]
  errs = ue,ge,re,ie,ze = [df['err_'+c].values for c in SDSS_col_names]
  Z,Z_err = [df[key].values for key in ['redshift','redshift_err']]
  data = {
    "u":u,"g":g,"r":r,"i":i,"z":z,
    "err_u":ue,"err_g":ge,"err_r":re,"err_i":ie,"err_z":ze,
    "Z":Z,"err_Z":Z_err
  }
  return data


def read_SDSS(Z_mid=.1,C09=True,mask_mu=True,mask_photo_err=False):
  f_dir = "/global/cscratch1/sd/wkblack/SDSS/"
  if C09: # use sSFR from Conroy+09
    f_tmp = "SDSS_Zp%i_C09.csv"
  else: # use SFR from Maraston+06
    f_tmp = "SDSS_Zp%i_all.csv"
  fname = f_dir + f_tmp % (Z_mid * 10)
  df = pd.read_csv(fname,skiprows=1)
  if C09:
    objid,ra,dec,Z,err_Z,u,g,r,i,z,err_u,err_g,err_r,err_i,err_z, \
     logMass,minLogMass,maxLogMass,sSFR,sSFR_min,sSFR_max \
      = [df[key].values for key in df.keys()]
    # convert sSFR values from Gyr to yr
    sSFR = 10**(sSFR - 9)
    sSFR_min = 10**(sSFR_min - 9)
    sSFR_max = 10**(sSFR_max - 9)
    # derive SFR values
    SFR = sSFR + logMass
    SFR_min = sSFR_min + minLogMass
    SFR_max = sSFR_max + maxLogMass
  else:
    objid,ra,dec,Z,err_Z,u,g,r,i,z,err_u,err_g,err_r,err_i,err_z, \
     SFR,SFR_min,SFR_max,logMass,minLogMass,maxLogMass \
      = [df[key].values for key in df.keys()]
    # derive sSFR valeus
    sSFR = SFR/10**logMass # 1/yr; specific star formation rate
    sSFR_min = SFR_min/10**maxLogMass
    sSFR_max = SFR_max/10**minLogMass
  # save Z limits
  Z_lim = Z_mid-.005,Z_mid+.005
  # set up masks
  mask = np.ones(len(Z)).astype(bool)
  if mask_mu:
    # makes sure errors exist for mass esimates
    mask *= (maxLogMass>0) * (maxLogMass>minLogMass)
  if mask_photo_err:
    # mask by photometric error
    mags = np.array([u,g,r,i,z])
    errs = np.array([err_u,err_g,err_r,err_i,err_z])
    if 0: 
      # exclude worst outliers, corrected by magnitude
      rel_err = np.log10(errs/10**(.2*mags))
      mn,st = [fxn(rel_err,axis=1) for fxn in [np.mean,np.std]]
      sig_rel = np.array([(rel_err[ii]-mn[ii])/st[ii] for ii in range(5)])
      mask_photo_err = np.product(sig_rel<=5,axis=0).astype(bool) # if any band has 5 sigma high error
    else:
      # make a hard cut on
      err_lim = .10*np.sqrt(5) # magnitude error cutoff
      errs_tot = np.sqrt(np.sum(errs**2,axis=0)) # total photo error
      mask_photo_err = errs_tot < err_lim
    mask *= mask_photo_err
  data = {
    # won't need ra, dec, objid
    "dset":"SDSS",
    "Z_lim":Z_lim,"Z_mid":Z_mid,"m_star":R14(Z_mid),
    "lbl_Z":r'$z|[%0.3g,%0.3g)$' % (Z_lim[0],Z_lim[1]),
    "cols":SDSS_col_names,
    "Z":Z[mask],"err_Z":err_Z[mask],
    "u":u[mask],"g":g[mask],"r":r[mask],"i":i[mask],"z":z[mask],
    "err_u":err_u[mask],"err_g":err_g[mask],"err_r":err_r[mask],"err_i":err_i[mask],"err_z":err_z[mask],
    "logMass":logMass[mask],"minLogMass":minLogMass[mask],"maxLogMass":maxLogMass[mask],
    "sSFR":sSFR[mask],"sSFR_min":sSFR_min[mask],"sSFR_max":sSFR_max[mask],
    "SFR":SFR[mask],"SFR_min":SFR_min[mask],"SFR_max":SFR_max[mask]
  }
  return data


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# set up variables to handle TNG files

f_dir_TNG = "/global/cscratch1/sd/wkblack/TNG300-1/"
f_tmp_TNG_combined = f_dir_TNG + "StellarPhot/combined_Zp%i.h5"
key_photo = 'Subhalo_StellarPhot_p07c_cf00dust_res_conv_z_rad30pkpc_red'
key_SFR = 'Subhalo/SubhaloSFR' # Msol/yr
key_Mass = 'Subhalo/SubhaloMassType' # Msol; [4] yeilds stellar mass
Mf_TNG = 10**10/h # Msol, factor all masses in TNG are divided by
N_gc = 600 # number of group catalogs

TNG_snaps = {
  # data linking redshifts (left) to snapshot number (right)
  20.05 : "000",
    2.0 : "033",
    1.0 : "050",
    0.5 : "067",
    0.4 : "072",
    0.3 : "078",
    0.2 : "084",
    0.1 : "091",
    0.0 : "099"
}


def add_error(bands):
  ZP = 17.3
  lg_slope = [ 0.23456981,  0.18536524,  0.18713662,  0.19475163,  0.2464522 ]
  lg_slope_err = [0.00083179, 0.00058399, 0.00054119, 0.0005958 , 0.00087968]
  lg_epsilon_0 = [-1.74200724, -2.21172122, -2.18156711, -2.09730564, -1.61928207]
  lg_epsilon_0_err = [0.00190781, 0.00042188, 0.00035484, 0.00057168, 0.00106389]
  u,g,r,i,z = bands
  bands_err = np.zeros(np.shape(bands)) # error in measurement (Gaussian)
  bands_obs = np.zeros(np.shape(bands)) # applied Gaussian normal random errors
  for ii in range(5):
    bands_err[ii] = 10**(lg_slope[ii] * (bands[ii] - ZP) + lg_epsilon_0[ii])
  bands_obs = bands + np.random.normal(0,bands_err)
  return bands_err, bands_obs


def plot_sSFR_error():
  " plot sSFR error bars and find relations "
  data = read_SDSS()
  u,g,r,i,z = bands = [data[c] for c in data["cols"]]
  sSFR_min,sSFR,sSFR_max = [data['sSFR' + k] for k in ["_min","","_max"]]
  mu_min,mu,mu_max = [data[k+'ogMass'] for k in ['minL','l','maxL']]
  mask = (sSFR_min < sSFR) * (sSFR < sSFR_max) * (sSFR > 10**-15)
  # plt.scatter(mu,sSFR_max-sSFR_min)
  x,x_min,x_max = [np.log10(s[mask]) for s in [sSFR,sSFR_min,sSFR_max]]
  plt.scatter(x,x-x_min,1,label=r'$\sigma_{\rm lg~\phi_{\min}}$')
  plt.scatter(x,x_max-x,1,label=r'$\sigma_{\rm lg~\phi_{\max}}$')
  # fit error 
  lm = kllr_model(kernel_width=.5)
  for y in [x-x_min,x_max-x]:
    xr,yrm,icpt,sl,sig = lm.fit(x,np.log10(y),nbins=1000)
    plt.plot(xr,10**yrm,'k')
    plt.fill_between(xr,10**(yrm-sig),10**(yrm+sig),alpha=.125,lw=0,color='k')
    N = get_counts(x,xr) # pseudocount per evaluation point
    err = sig/np.sqrt(N) # error on the mean
    plt.fill_between(xr,10**(yrm-5*err),10**(yrm+5*err),alpha=.125,lw=0,color='r')
    # use polyfit to estimate a best-fit line
    zf = np.polyfit(xr+11,yrm,1,w=1/err)
    print(zf)
    plt.plot(xr,10**np.poly1d(zf)(xr+11),'g',lw=2)
  # set up axes and display
  plt.legend()
  plt.xlabel(r'$\log_{10} {\rm sSFR \cdot yr}$')
  plt.ylim(1e-3,32)
  plt.yscale('log')
  plt.show()


def add_sSFR_error(sSFR,sig_low=.1,sig_high=.1):
  # TODO: handle zeros
  # set up local vars
  x = np.log10(sSFR)
  lg_sSFR_obs = np.copy(x) # observed sSFR value
  lg_sSFR_min = np.copy(x) # simulated lower error
  lg_sSFR_max = np.copy(x) # simulated upper error
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  # set up bounds
  mask_mid = (-14.5 <= x) * (x <= -11)
  # lower bounds
  lg_sSFR_min[x<-14.5] = -42
  lg_sSFR_min[mask_mid] = -42 + (31 - sig_low)/3.5 * (x[mask_mid] + 14.5)
  lg_sSFR_min[x>-11] = x[x>-11] - .17
  # upper bounds
  lg_sSFR_max[x<-14.5] = -12.5
  lg_sSFR_max[mask_mid] = -12.5 + (1.5 + sig_high)/3.5 * (x[mask_mid] + 14.5)
  lg_sSFR_max[x>-11] = x[x>-11] + .07
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  # generate random seed and observations
  seed = np.random.random(len(xv))
  # select evenly between upper and lower bounds
  lg_sSFR_obs = lg_sSFR_min + seed * (lg_sSFR_max - lg_sSFR_min)
  return 10**lg_sSFR_obs


def plot_SDSS_vs_TNG_hist():
  """
  plot color histograms for SDSS and TNG, 
  showing off the stregnth of the GV for both
  """
  data_SDSS = d1 = read_SDSS()
  data_TNG = d2 = read_TNG()
  fig,axs = plt.subplots(1,4,figsize=double_width,sharey=True)
  for ii,data in enumerate([d1,d2]):
    bands = [data[c] for c in data["cols"]]
    if ii==1: # TNG; simulate errors
      bands_err, bands = add_error(bands)
    c1 = np.diff(bands,axis=0)
    lbl = data['dset']
    for ii in range(4):
      mask_ii = np.abs((c1[ii] - np.mean(c1[ii])))/np.std(c1[ii]) < 3
      axs[ii].hist(c1[ii][mask_ii],100,histtype='step',label=lbl)
      lbl_ii = ["$u-g$", "$g-r$", "$r-i$", "$i-z$"][ii]
      axs[ii].set_xlabel(lbl_ii)
  axs[0].set_ylabel('Counts')
  plt.subplots_adjust(wspace=0)
  plt.legend()
  fname_out = 'hist_SDSS_vs_TNG.pdf'
  plt.savefig(fname_out)
  print("Saved",fname_out)
  plt.show()


def combine_TNG(zv=.1):
  " combine group cat data w/ SDSS observed photometry "
  # set up redshift-dependent local variables
  snap = TNG_snaps[zv]
  m_star = R14(zv)
  mu = 25 + 5 * np.log10(c*zv/H0) # distance modulus; ~42 for z=.5
  f_phot = f_dir_TNG + "StellarPhot/" + key_photo + f"_{snap}.hdf5"
  f_tmp = f_dir_TNG + f"groupcats/{snap}/fof_subhalo_tab_{snap}.%i.hdf5"
  # read in photometry catalog
  with h5py.File(f_phot,'r') as ds:
    # 'true' photometry, without errors
    ut,gt,rt,it,zt = bands_true = np.transpose(ds[key_photo][:])
  N_tot = len(ut)
  # add error to fields
  bands_err, bands_obs = add_error(bands_true)
  ue,ge,re,ie,ze = bands_err
  uo,go,ro,io,zo = bands_obs
  # set up fields to grab
  M_star,SFR = np.zeros((2,N_tot))
  # iterate through group catalogs
  tot_len = 0 # total galaxies processed so far
  for ii in range(N_gc):
    print(ii,end=', ')
    fname = f_tmp % ii
    with h5py.File(fname,'r') as ds:
      if 'SubhaloMassType' not in ds['Subhalo'].keys():
        print("NOTE: Empty file:",fname)
        continue
      masses = ds[key_Mass][()][:,4] # no wind
      N_ii = len(masses) # number of galaxies in current file
      M_star[tot_len:tot_len+N_ii] = masses
      SFR[tot_len:tot_len+N_ii] = ds[key_SFR][:]
      tot_len += N_ii # update new starting position
    if (ii+1) % 50 == 0:
      print() # to help flush out lines
  assert(tot_len == N_tot)
  # fix up outputs
  M_star *= Mf_TNG # now in Msol
  # vet down
  mask = (M_star > 0) * (it < m_star + 1.75)
  # save output
  fname_out = f_tmp_TNG_combined % (10*zv)
  with h5py.File(fname_out,'w') as ds:
    for tag,dat in zip(['u','g','r','i','z',
                        'err_u','err_g','err_r','err_i','err_z',
                        'u_true','g_true','r_true','i_true','z_true',
                        'M_star','SFR'],
                       [ uo, go, ro, io, zo, 
                         ue, ge, re, ie, ze,
                         ut, gt, rt, it, zt,
                         M_star,  SFR ]):
      ds.create_dataset(tag,data=dat[mask])
    # set up attributes 
    ds.attrs['N_tot'] = N_tot
    ds.attrs['snap'] = snap
    ds.attrs['Z'] = zv
    ds.attrs['mu'] = mu
    ds.attrs['m_star'] = m_star
    ds.attrs['dset'] = "TNG300-1"
    ds.create_dataset("ut",data=ut[mask])
  # wrap up 
  print(fname_out,"saved.")
  return fname_out


def read_TNG(zv=.1,observed=False,mask_mass=True):
  fname_combined = f_tmp_TNG_combined % (10*zv)
  if isfile(fname_combined):
    print("Reading in combined file")
    with h5py.File(fname_combined,'r') as ds:
      m_star = ds.attrs['m_star']
      N_tot = ds.attrs['N_tot']
      u,g,r,i,z,\
       ue,ge,re,ie,ze,\
        M_star,SFR = [ds[key][:] for key in ['u','g','r','i','z',
                         'err_u','err_g','err_r','err_i','err_z',
                         'M_star','SFR']]
    err_Z = np.ones(N_tot) * 1e-5
    sSFR = SFR/M_star
    if 0: # add errors to sSFR
      sSFR = add_sSFR_error(sSFR)
    logMass = np.log10(M_star)
    col_names = SDSS_col_names
  else:
    Mf = 10**10/h # Msol, factor all masses in TNG are divided by
    # set up snap enumeration to redshift link
    snap = TNG_snaps[zv]
    m_star = R14(zv)
    mu = 25 + 5 * np.log10(c*zv/H0) # distance modulus; ~42 for z=.5
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # read in base data
    f_dir = f_dir_TNG + f"/groupcats/{snap}/"
    try: # open HDF5 file
      f_tmp = "vetted.h5"
      fname = f_dir + f_tmp
      with h5py.File(fname,'r') as ds:
        m_i = ds['i'][:] + mu
        mask = m_i < m_star + 1.75
        bands = [ds[c][:][mask] for c in ['g','r','i','z']]
        g,r,i,z = bands + mu # shift into observed frame from rest
        M_star, sSFR = [ds[key][:][mask] for key in ['M_star','sSFR']]
      col_names = ['z','g','r','i'] # lazy, but keeps use of colors 1 & 2
      u = None
    except OSError:
      f_tmp = f"fof_subhalo_tab_{snap}.*.hdf5" # template fname
      fname = f_dir + f_tmp # file name to read in 
      print("ERROR: Not implemented yet! [see TNG_gamma.py]")
      assert(1==0)
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    # generate missing data
    SFR = sSFR * M_star
    logMass = np.log10(M_star)
    err_Z = np.ones(len(sSFR)) * 1e-5 # color errors set to small floor
    ue = ge = re = ie = ze = err_Z
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    if observed: # read in observed frame data (rather than rest frame)
      f_dir = f_dir_TNG + "StellarPhot/"
      fname = f_dir + key_photo + f"_{snap}.hdf5"
      # open HDF5 file
      with h5py.File(fname,'r') as ds:
        u,g,r,i,z = np.transpose(ds[key_photo][:])
      col_names = SDSS_col_names
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  # summarize output data vector
  data = {
    "dset":"TNG300-1", "Z_lim":[zv,zv], "Z_mid":zv, "m_star":m_star,
    "Z":zv, "err_Z":err_Z, "lbl_Z":f"$z={zv}$", "cols":col_names,
    "u":u, "g":g, "r":r, "i":i, "z":z,
    "err_u":ue,"err_g":ge,"err_r":re,"err_i":ie,"err_z":ze,
    "logMass":logMass,"minLogMass":logMass,"maxLogMass":logMass,
    "sSFR":sSFR,"sSFR_min":sSFR,"sSFR_max":sSFR,
    "SFR":SFR,"SFR_min":SFR,"SFR_max":SFR
  }
  return data


def make_masks(data,pivot=10**-11.5):
  " set up sSFR masks "
  sSFR_min,sSFR,sSFR_max = [data[key] for key in ["sSFR_min","sSFR","sSFR_max"]]
  mrr = mask_really_red = sSFR_max < pivot
  mrd = mask_red = (~mask_really_red) * (sSFR < pivot)
  msr = mask_sorta_red = (~mask_really_red) * (~mask_red) * (sSFR_min < pivot)
  mrb = mask_really_blue = (~mask_really_red) * (~mask_red) * (~mask_sorta_red)
  masks = [mrr,mrd,msr,mrb] # compilation of masks
  return masks


def plot_SDSS_TNG_combined(markers=['o','v']):
  " UGLY "
  # set up files to analyze
  fn_SDSS = ["parameters/nu_" + fn + ".h5" for fn in 
             ["CM_typical", "SDSS_CM_blind", "SDSS_CM_sSFR", 
              "CC_typical", "SDSS_CC_blind", "SDSS_CC_sSFR", 
              "SDSS_2GM", "SDSS_3GM" ]]
  fn_TNG = ["parameters/TNG_" + fn + ".h5" for fn in 
            [ "CM_typical", "CM_blind", "CM_sSFR",
               "CC_typical", "CC_blind", "CC_sSFR",
               "2GM", "3GM" ]]
  labels = [ "CM typical", "CM blind", "CM sSFR", 
             "CC typical", "CC blind", "CC sSFR", 
             "2GM blind", "3GM blind" ]
  kinds = [0,0,0, 1,1,1, 2,2]
  colors = np.array(mpl_colors)[kinds]
  # read in and plot
  for jj in range(len(markers)): 
    for ii in range(len(kinds)):
      with h5py.File([fn_SDSS,fn_TNG][jj][ii],'r') as ds:
        c = Classification(ds.attrs['classification'])
      vals = [c.get_BA(v) for v in [-1,0,+1]]
      ye = np.transpose([np.diff(vals)])
      plt.errorbar(ii+(jj-.5)/5,vals[1],ye,color=colors[ii],ms=10,
                   fmt=markers[jj],capsize=5,markerfacecolor=[colors[ii],'white'][jj])
  plt.gca().set_xticks(np.arange(len(kinds)))
  plt.gca().set_xticklabels(labels,rotation=45)
  plt.ylabel(lbl_BA)
  ylim = plt.ylim(np.clip(plt.ylim(),0,1))
  # make legend
  xlim = plt.xlim(plt.xlim())
  for mkr,lbl in zip(markers,['SDSS','TNG']):
    plt.scatter(-10,-10,100,color='k',marker=mkr,label=lbl,facecolor='white')
  plt.legend()
  # save output
  fname_out = "parameters/combined.pdf"
  plt.savefig(fname_out)
  print(fname_out,"saved!")
  plt.show()


if 0 and __name__ == '__main__':
  # SDSS fitting
  # fit function filenames
  fnames = [ "parameters/nu_CC_typical.h5", "parameters/nu_SDSS_CC_blind.h5", 
             "parameters/nu_SDSS_CC_sSFR.h5", "parameters/nu_CM_typical.h5",
             "parameters/nu_SDSS_CM_blind.h5", "parameters/nu_SDSS_CM_sSFR.h5",
             "parameters/nu_SDSS_2GM.h5", "parameters/nu_SDSS_2GM_sSFR.h5", 
             "parameters/nu_SDSS_3GM.h5", "parameters/nu_SDSS_3GM_sSFR.h5" ]
  # plot labels for each fname dataset
  labels = [ "CC typical", "CC blind", "CC sSFR", 
             "CM typical", "CM blind", "CM sSFR", 
             "2GM blind", "2GM sSFR", 
             "3GM blind", "3GM sSFR" ]
  kinds = [0,0,0, 1,1,1, 2,2,2,2]
  if 1: # import and analyze data
    data = read_SDSS()
    Z = data['Z_mid']
    pivot = 10**(-11 + Z) # previously -11.5
    bool_plot = True
    if 0: # CC fits
      fit_CC(data,fname_out=fnames[0],optimize=False,plotting=bool_plot)
      test_CC_fit(data,fnames[0],pivot)
      fit_CC(data,use_sSFR=None,fname_out=fnames[1],plotting=bool_plot)
      test_CC_fit(data,fnames[1],pivot)
      fit_CC(data,use_sSFR=pivot,fname_out=fnames[2],plotting=bool_plot)
      test_CC_fit(data,fnames[2],pivot)
    if 0: # CM fits
      fit_CM(data,fname_out=fnames[3],optimize=False,plotting=bool_plot)
      test_CM_fit(data,fnames[3],pivot)
      fit_CM(data,use_sSFR=None,fname_out=fnames[4],plotting=bool_plot)
      test_CM_fit(data,fnames[4],pivot)
      fit_CM(data,use_sSFR=pivot,fname_out=fnames[5],plotting=bool_plot)
      test_CM_fit(data,fnames[5],pivot)
    if 0: # GM fits
      fit_test_GM(data,2,pivot,fname_out=fnames[6],plotting=bool_plot,use_errors=0)
      fit_test_GM(data,3,pivot,fname_out=fnames[8],plotting=bool_plot,use_errors=0)
      # old methods: 
      # fit_test_GM(data,2,pivot,fname_out=fnames[7],plotting=bool_plot,optimize=True,use_errors=True)
      # fit_test_GM(data,3,pivot,fname_out=fnames[9],plotting=bool_plot,optimize=True,use_errors=True)
    if 0: # view data
      # plot_sSFR(data) # sSFR histograms
      plot_CC(data,sSFR_bins=[0,10**-11.5,1])
  if 1: # show combined plots
    lbl_SDSS = r"SDSS $z=.1\pm.005$"
    # tests_combined(fnames,labels,title=None)
    f_out = "parameters/combined_SDSS_TR.pdf"
    f_out = "parameters/combined_SDSS.pdf"
    mask = [3,4,5,0,1,2,6,8]
    arr = np.array
    tests_combined_v2(arr(fnames)[mask],arr(labels)[mask],kind=arr(kinds)[mask],
                      title=lbl_SDSS,fname_out=f_out,which="bACC",printing=False)
  print('~fin')


if 0 and __name__ == '__main__':
  # TNG routines
  if 1: # set up files to produce
    par = "parameters/"
    ext = ".h5"
    fnames = [ "TNG_CM_typical", "TNG_CM_blind", "TNG_CM_sSFR",
               "TNG_CC_typical", "TNG_CC_blind", "TNG_CC_sSFR",
               "TNG_2GM", "TNG_3GM" ]
    fnames = [ par + fn + ext for fn in fnames ]
    labels = [ "CM typical", "CM blind", "CM sSFR", 
               "CC typical", "CC blind", "CC sSFR", 
               "2GM blind", "3GM blind" ]
    kinds = [ 0,0,0, 1,1,1, 2,2 ]
    kinds = [ 1,1,1, 0,0,0, 2,2 ]
  if 0: # TNG fitting (routines requiring data) 
    data = read_TNG()
    if 0: # figure out optimal sSFR cut to use
      plot_sSFR(data)
    Z = data['Z_mid']
    pivot = 10**(-11 + Z) # previously -11.5
    bool_plot = True
    if 0: # CM fitting
      fit_CM(data,optimize=False,use_sSFR=None,plotting=bool_plot,fname_out=fnames[0],N_fit=2)
      test_CM_fit(data,fnames[0],pivot)
      fit_CM(data,optimize=True,use_sSFR=None,plotting=bool_plot,fname_out=fnames[1],N_fit=2)
      test_CM_fit(data,fnames[1],pivot)
      fit_CM(data,optimize=True,use_sSFR=pivot,plotting=bool_plot,fname_out=fnames[2],N_fit=2)
      test_CM_fit(data,fnames[2],pivot)
    if 0: # CC fitting
      fit_CC(data,optimize=False,use_sSFR=None,plotting=bool_plot,fname_out=fnames[3],N_fit=2)
      test_CC_fit(data,fnames[3],pivot)
      fit_CC(data,optimize=True,use_sSFR=None,plotting=bool_plot,fname_out=fnames[4],N_fit=2)
      test_CC_fit(data,fnames[4],pivot)
      fit_CC(data,optimize=True,use_sSFR=pivot,plotting=bool_plot,fname_out=fnames[5],N_fit=2)
      test_CC_fit(data,fnames[5],pivot)
    if 1: # GM fitting
      fit_test_GM(data,2,pivot,bool_plot,optimize=False,fname_out=fnames[6],use_errors=True)
      fit_test_GM(data,3,pivot,bool_plot,optimize=False,fname_out=fnames[7],use_errors=True)
  if 1: # show combined plots
    lbl_TNG = r"TNG $z=.1$"
    f_out = par + 'combined_TNG_TR.pdf'
    f_out = par + 'combined_TNG.pdf'
    tests_combined_v2(fnames,labels,kind=kinds,title=lbl_TNG,
                      fname_out=f_out,which="bACC",printing=True) 
  print('~fin')


if 1 and __name__ == '__main__':
  # cf TNG and SDSS
  # plot_SDSS_vs_TNG_hist()
  plot_SDSS_TNG_combined()
