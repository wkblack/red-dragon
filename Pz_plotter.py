# show how P(Z) for RS & BC behave as we slide along in redshift

import rd_gamma
rd = rd_gamma.dragon('output/fit_Bz_2K.h5')
cols = rd_gamma.mpl_colors_RF # re-cycled mpl_colors

exec(open("mpl_tools.py").read()) # import matplotlib tools and defaults


def plot_Pz(Z=[.1,.3],display=True,col_choice=[0,1],skip=2,fname_out=None):
  # grab mean colors of RS & BC at this redshift
  c_means = rd.get_means(Z)
  x_min,x_max = [f(c_means[:,:,col_choice[0]]) for f in [np.min,np.max]]
  y_min,y_max = [f(c_means[:,:,col_choice[1]]) for f in [np.min,np.max]]
  for ii,Z_ii in enumerate(Z):
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    ax1 = plt.subplot2grid((rd.N_fit,3),(0,0),rowspan=2)
    axs = [plt.subplot2grid((rd.N_fit,3),(jj,1),colspan=2) for jj in range(rd.N_fit)]
    for jj in range(rd.N_fit):
      # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
      # plot location in color--color space (at least 2D)
      x,y = [c_means[ii][jj][col_choice[kk]] for kk in [0,1]]
      ax1.scatter(x,y,color=cols[jj])
      ax1.set_xlim(x_min-.01,x_max+.01)
      ax1.set_ylim(y_min-.01,y_max+.01)
      # TODO: plot ellipses here for BC & RS
      # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
      # calculate and plot P(Z) for components
      if 1: # arbitrarily set up bands (only their difference matters)
        d = 0 # z-band arbitrarily set to null
        c = c_means[ii][jj][-1] + d # i-band = (i-z) + z
        b = c_means[ii][jj][-2] + c # r-band = (r-i) + i
        a = c_means[ii][jj][-3] + b # g-band = (g-r) + r
        bands = np.array([a,b,c,d])[np.newaxis]
      z,logL= rd.get_Pz(bands,interpolate=True)
      z = z[skip:]; logL = logL[skip:]
      Pz = np.exp(logL)/np.sum(np.exp(logL),axis=0)
      for kk in range(rd.N_fit):
        axs[jj].plot(z,Pz[:,kk,0]/np.max(Pz),cols[kk])
      ylim = axs[jj].set_ylim(0,1)
      axs[jj].plot(Z_ii*ones,ylim,cols[jj],ls=':')
      axs[-1].set_xlabel('Redshift')
      ax1.set_xlabel(r'$g-r$')
      ax1.set_ylabel(r'$r-i$')
      axs[jj].set_xlim(z[0],z[-1])
      axs[jj].set_ylabel(r'Relative $P(z)$')
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # tidy up and display
    if fname_out is not None:
      plt.savefig(fname_out % ii)
      print("Saved",fname_out % ii)
    if display:
      plt.show()
    else:
      plt.clf()


if __name__ == '__main__':
  dZ = .025
  plot_Pz(np.arange(.1,.7+dZ/2,dZ),fname_out='output/Pz_%03i.png',display=False)
