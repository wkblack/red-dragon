import red_dragon_base as rdb
import rd_alpha_analyze_h5 as rdaa
import rd_beta

np,plt,h5py = rdb.np,rdb.plt,rdb.h5py

lbls_cols = [r'$(g-r)$',r'$(g-i)$',r'$(g-z)$',
             r'$(r-i)$',r'$(r-z)$',r'$(i-z)$']
lbls_mags = [r'$m_g$',r'$m_r$',r'$m_i$',r'$m_z$']

def grab_lims(qtl,Z,bands,mask_alpha=None,mask_beta=None,
              plot_vals=None):
  """
  qtl = quantile to test
  Z = redshifts of the galaxies
  bands = magnitude bands [g,r,i,z]
  mask_alpha = None -> input rd_alpha mask (for second run of same cut)
  mask_beta = None -> same as above for rd_beta
  plot_vals = None -> (c1,c2,m) = colors and magnitudes for plotting
  """
  g,r,i,z = bands
  cols = [g-r,g-i,g-z,r-i,r-z,i-z]
  if mask_alpha is None:
    mask_alpha = rdaa.P_red(Z,np.transpose(cols))>.5
  if mask_beta is None:
    mask_beta = rd_beta.P_red(Z,bands)>.5
  report_alpha,report_beta = np.zeros((2,6,2)) # a&b + 6C + ulim & llim
  for ii,c in enumerate(cols):
    report_alpha[ii] = np.quantile(c[mask_alpha],[1-qtl,qtl])
    report_beta[ii] = np.quantile(c[mask_beta],[1-qtl,qtl])
  
  if plot_vals is not None: 
    c1,c2,m = plot_vals
    plt.subplot(121)
    plt.scatter(cols[c1],cols[c2],1,alpha=.05,linewidth=0,c=mask_beta)
    plt.xlabel(lbls_cols[c1])
    plt.ylabel(lbls_cols[c2])
    plt.subplot(122)
    plt.scatter(bands[m],cols[c2],1,alpha=.05,linewidth=0,c=mask_beta)
    plt.xlabel(lbls_mags[m])
    plt.ylabel(lbls_cols[c2])
    # wrap up
    plt.tight_layout()
    plt.show()
  
  return mask_alpha,mask_beta,report_alpha,report_beta


def test_lims(qtl,Z,bands,gl_out):
  mask_alpha,mask_beta,report_alpha,report_beta = gl_out
  g,r,i,z = bands
  cols = [g-r,g-i,g-z,r-i,r-z,i-z]
  mask_a,mask_b = np.ones((2,len(Z)))
  for ii,col in enumerate(cols):
    mask_a *= (report_alpha[ii][0]<=col)*(col<=report_alpha[ii][1])
    mask_b *= (report_beta[ii][0]<=col)*(col<=report_beta[ii][1])
  # compare output masks 
  s0 = rdb.score(mask_alpha,mask_beta)
  s1 = rdb.score(mask_alpha,mask_a)
  s2 = rdb.score(mask_alpha,mask_b)
  s3 = rdb.score(mask_beta,mask_a)
  s4 = rdb.score(mask_beta,mask_b)
  s5 = rdb.score(mask_a,mask_b)
  return [s0,s1,s2,s3,s4,s5]


def analyze(Z,dZ=.005,plotting=True,plot_sig=True):
  mask_Z = (Z<=rdb.Z)*(rdb.Z<Z+dZ)
  g,r,i,z,Z = [field[mask_Z] for field in rdb.fields]
  bands = [g,r,i,z]
  mn,mx = np.log10(1-rdb.one_sigma),np.log10(1-rdb.sigval(5))
  qtls = 1 - 10**np.linspace(mn,mx,500)
  # qtls = np.linspace(rdb.two_sigma,1,5000)
  report = np.zeros(np.shape(qtls))
  ma,mb = None,None
  for ii,qtl in enumerate(qtls):
    if len(qtls)<25:
      print(f"Processing quantile {qtl}")
    gl_out = grab_lims(qtl,Z,bands,ma,mb)
    ma,mb = gl_out[0],gl_out[1]
    scores = test_lims(qtl,Z,bands,gl_out)
    report[ii] = np.max(scores)
  
  qtl_min = np.mean(qtls[report==min(report)])
  score_min = np.min(report)
  if plotting:
    if len(qtls)<25:
      plt.scatter(qtls,report)
    else:
      plt.plot(qtls,report)
    plt.xlabel('Quantile')
    plt.ylabel('max error fraction')
    plt.title(f"Best quantile(s): {qtl_min:0.3g}")
    if plot_sig: # plot up sigma values
      xlim = plt.xlim()
      ylim = plt.ylim()
      for ii in np.arange(5+1):
        plt.plot(np.ones(2)*rdb.sigval(ii),ylim,'k',alpha=.5)
      plt.xlim(xlim)
      plt.ylim(ylim)
    plt.tight_layout()
    plt.show()
  
  return qtl_min,score_min


def analyze_all(z_vals=np.arange(.1,.7,.005),dZ=.005,
                plot_all=True,plot_each=False,plot_sig=True):
  report = np.zeros((len(z_vals),2))
  for ii,Z_ii in enumerate(z_vals):
    print(f"Analyzing z|[{Z_ii:0.3f},{Z_ii+dZ:0.3f})")
    report[ii] = analyze(Z_ii,dZ,plot_each)
  
  if plot_all:
    # plot best quantiles
    plt.subplot(2,1,1)
    plt.scatter(z_vals,report[:,0])
    plt.xlabel('Redshift')
    plt.ylabel('Best quantile')
    plt.ylim(None,1)
    plt.title('Quantile Analysis')
    # plot scores
    plt.subplot(2,1,2)
    plt.scatter(z_vals,report[:,1])
    plt.xlabel('Redshift')
    plt.ylabel('Lowest error fraction')
    plt.ylim(0,None)
    plt.title('Score Analysis')
    if plot_sig: # plot up sigma values
      xlim = plt.xlim()
      ylim = plt.ylim()
      for ii in np.arange(5+1):
        plt.plot(xlim,np.ones(2)*rdb.sigval(ii),'k',alpha=.5)
      plt.xlim(xlim)
      plt.ylim(ylim)
    # 
    plt.tight_layout()
    plt.show()
  return report



if __name__=='__main__':
  if 0:
    z_vals = [.1,.15]
    analyze_all(z_vals,plot_each=True)
  elif 0:
    z_vals = np.arange(.1,.7,.075)
    analyze_all(z_vals)
  else:
    analyze_all()
