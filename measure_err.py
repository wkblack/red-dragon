# program to estimate and model error in the Buzzard simulation

########################################################################
# import statements

exec(open("mpl_tools.py").read()) # import matplotlib tools and defaults
exec(open("kllr_tools.py").read()) # import KLLR and tools to aid it

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# import file
import h5py

Z_lim = [.499,.501]


if 0: # import full dataset
  mcat_200 = '/project/projectdirs/des/jderose/Chinchilla/Herd/Chinchilla-3/v2.0.0/sampleselection/Y3a/Buzzard-3_v2.0_Y3a_mastercat.h5'
  # item locations
  loc_Z = 'catalog/bpz/unsheared/redshift_cos' # cosmological redshift
  loc_col_true = 'catalog/gold/mag_%s_true'
  loc_col_obs = 'catalog/gold/mag_%s'
  loc_col_err = 'catalog/gold/mag_err_%s'
  bands = ['g','r','i','z']
  with h5py.File(mcat_200,'r') as ds:
    Z = ds[loc_Z][()] # ~46 sec
    mask_Z = (Z_lim[0] <= Z) * (Z < Z_lim[1]) # ~9 sec
    Z = Z[mask_Z] # ~3s
    # ds[...][mask_Z] takes >~2 times longer! (180s)
    bands_obs = [ds[loc_col_obs % c][:][mask_Z] for c in bands]
    bands_err = [ds[loc_col_err % c][:][mask_Z] for c in bands] # ~70s
    bands_tru = [ds[loc_col_true % c][:][mask_Z] for c in bands] # ~84s


if 1: # import luminosity-vetted sample: 
  fname = '/global/cscratch1/sd/wkblack/SkyMaps/buzzard/vet/subset_new_L.h5'
  with h5py.File(fname,'r') as ds:
    Z = ds['redshift_cos'][:]
    mask_Z = (Z_lim[0] <= Z) * (Z < Z_lim[1])
    Z = Z[mask_Z]
    bands_obs = [ds['mag_'+c][:][mask_Z] for c in ['g','r','i','z']]
    bands_err = [ds['mag_err_'+c][:][mask_Z] for c in ['g','r','i','z']]
    bands_tru = [ds['mag_'+c+'_true'][:][mask_Z] for c in ['g','r','i','z']]


# mask data
mask_bands = np.ones(len(Z))
for band in bands_obs:
  mask_bands *= band < 99

mask = mask_bands.astype(bool)
Z = Z[mask]


obs = go,ro,io,zo = [m[mask] for m in bands_obs]
err = ge,re,ie,ze = [m[mask] for m in bands_err]
tru = gt,rt,it,zt = [m[mask] for m in bands_tru]

ZP = 22.5 # "zero point" for magnitudes


########################################################################

def plot_err():
  plt.scatter(gt,ge,1,c=Z,lw=0)
  plt.xlabel('true apparent magnitude')
  plt.ylabel(r'error')
  plt.yscale('log')
  plt.show()


def model_err(kw=.25):
  " fit relationship between error and magnitude "
  for ii in range(4):
    t,e = tru[ii],err[ii]
    col = mpl_colors[ii]
    K_all, K_mn, K_err = KLLR_bootstrap(t-ZP, np.log10(e),
                                        nbins=100, kernel_width=kw)
    xr,yrm,icpt,sl,sig = K_all # KLLR 'truth', sampling all points
    xrm,yrmm,icptm,slm,sigm = K_mn # mean from bootstrap
    xre,yrme,icpte,sle,sige = K_err # st dev from bootstrap
    plt.scatter(xr,sl,10,color=col)
    plt.fill_between(xr,slm-sle,slm+sle,alpha=.125,lw=0,color=col)
    zf = np.polyfit(xr,slm,1,w=1/sle)
    plt.plot(xr,np.poly1d(zf)(xr),color=col,lw=.5)
    print(ii,zf)
    """
    output: 
    0 [0.04059498 0.28771868]
    1 [0.02921358 0.2873946 ]
    2 [0.02541113 0.29017707]
    3 [0.02747813 0.30691325]
    """
  plt.xlabel('true apparent magnitude')
  plt.ylabel(r'slope $d\epsilon/dm$')
  plt.show()


def model_err2(order=1,N_samples=50):
  zf_vals = np.zeros((N_samples,4,order+1))
  for jj in range(N_samples):
    for ii in range(4):
      choice = np.random.choice(len(tru[ii]),10**4)
      t,e = tru[ii][choice],err[ii][choice]
      zf = np.polyfit(t-ZP,np.log10(e),order)
      zf_vals[jj][ii] = zf
  return zf_vals


def analyze_chains(zf_vals):
  N_samples, N_bands, N_order = np.shape(zf_vals)
  # general fits
  means,stdvs = [fxn(zf_vals,axis=(0,1)) for fxn in [np.mean,np.std]]
  print("Together, we find:")
  for ii in range(N_order):
    N = int(np.round(1.5-np.log10(np.abs(stdvs[ii]))))
    print(f'par[{ii}] = {means[ii]:0.{N}f} \pm {stdvs[ii]:0.{N}f}')
  # plot particular fits
  fig, axes = plt.subplots(N_order, N_order, sharex='col',
                           gridspec_kw={'hspace':0,'wspace':0})
  for kk in range(N_bands):
    col = mpl_colors[kk]
    for ii in range(N_order):
      for jj in range(N_order):
        if ii < jj:
          _ = axes[ii,jj].set_visible(False)
        if jj < ii:
          _ = axes[ii,jj].scatter(zf_vals[:,kk,jj],zf_vals[:,kk,ii],2,color=col,alpha=.5)
        else:
          _ = axes[ii,jj].hist(zf_vals[:,kk,ii],histtype='step')
          axes[ii,jj].get_yaxis().set_visible(False)
  plt.show()



def produce_error(bands):
  " g,r,i,z = bands (true magnitude) "
  a = 0.017 # pm .004; quadratic slope of log error
  b = .295 # pm .01; linear slope of log error
  c = [-1.34898794, -1.27240629, -1.08144321, -0.86578751] # Z=0.5
  c_err = [0.00764304, 0.00389136, 0.00470332, 0.00580421] # ditto
  c = np.outer(c,np.ones(len(bands[0])))
  x = bands - ZP
  return 10**(a*x**2 + b*x + c)


if __name__ == '__main__':
  if 0: 
    zfv = model_err2(1,100)
    analyze_chains(zfv)
    zfv = model_err2(2,100)
    analyze_chains(zfv)
  else:
    pass
