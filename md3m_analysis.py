# compile and analyze results of multidim_3model.py
# plot up evolution and attempt fits

import h5py
import numpy as np
from glob import glob
from matplotlib import pyplot as plt
from string import ascii_lowercase as alphabet

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# set up golden aspect ratio
golden_ratio = (1+np.sqrt(5))/2.
golden_aspect = 5*np.array([golden_ratio,1])

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# set up default plotting paradigms
plt.rcParams['figure.figsize'] = golden_aspect
plt.rcParams['font.size'] = 15

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# jackknife routines

def jackknife_fit(x,y,order=1,w=None):
  " jackknife values and errors on np.polyfit parameters "
  if w is None:
    w = np.ones(len(x))
  pars = np.zeros((order+1,len(x)))
  for ii in range(len(x)):
    pars[:,ii] = np.polyfit(np.delete(x,ii),np.delete(y,ii),
                            order,w=np.delete(w,ii))
  return np.mean(pars,axis=1), np.std(pars,axis=1)


def print_jkf(fit,indent=0):
  [mus,errs] = fit
  N_par = len(mus) if hasattr(mus,'__len__') else 0
  for ii in range(N_par):
    N = int(np.round(1.5-np.log10(np.abs(errs[ii]))))
    print(' ' * indent, end='') # indent that many spaces
    print(f"{alphabet[ii]} = {mus[ii]:0.{N}f} +/- {errs[ii]:0.{N}f}")


########################################################################
# analysis routines

fnames = sorted(glob('output_md3m/md3m_Z*.h5'))
Z_len = len(fnames)

lbls = ['fR_a','fR_a_err','fR_b','fR_b_err',
        'gmr_RS_a','gmr_RS_a_err','gmr_RS_b','gmr_RS_b_err',
        'gmr_BC_a','gmr_BC_a_err','gmr_BC_b','gmr_BC_b_err',
        'lg_sig_RS_a','lg_sig_RS_a_err','lg_sig_RS_b','lg_sig_RS_b_err',
        'lg_sig_BC_a','lg_sig_BC_a_err','lg_sig_BC_b','lg_sig_BC_b_err']

plot_lbls = [r'$a(f_R)$', r'$b(f_R)$',
             r'$a(\langle g-r \rangle_{\rm RS})$',
             r'$b(\langle g-r \rangle_{\rm RS})$',
             r'$a(\langle g-r \rangle_{\rm BC})$',
             r'$b(\langle g-r \rangle_{\rm BC})$',
             r'$a(\log_{10} \sigma_{\rm RS})$',
             r'$b(\log_{10} \sigma_{\rm RS})$',
             r'$a(\log_{10} \sigma_{\rm BC})$',
             r'$b(\log_{10} \sigma_{\rm BC})$']

N_pars = len(plot_lbls)


def read_files():
  fs = [h5py.File(fnames[ii],'r') for ii in range(Z_len)]
  Z = np.zeros(Z_len)
  pars = np.zeros((Z_len,len(lbls)))
  for ii in range(Z_len):
    Z[ii] = fs[ii].attrs['Z_med']
    for jj in range(N_pars):
      w_jj = 1/fs[ii][lbls[2*jj+1]][:]**2 # weight
      pars[ii,2*jj] = np.sum(w_jj*fs[ii][lbls[2*jj]][:])/np.sum(w_jj) # mean
      pars[ii,2*jj + 1] = np.sqrt(np.sum(1/w_jj)) # error
    fs[ii].close()
  return Z,pars


def read_par(jj):
  fs = [h5py.File(fnames[ii],'r') for ii in range(Z_len)]
  Z_vals = []
  par_vals = []
  err_vals = []
  for ii in range(Z_len):
    # grab pars, errs, redshift
    pars = fs[ii][lbls[2*jj]][:]
    errs = fs[ii][lbls[2*jj+1]][:]
    Z_ii = fs[ii].attrs['Z_med']
    # append to existing arrays
    Z_vals = np.append(Z_vals,np.tile(Z_ii,len(pars)))
    par_vals = np.append(par_vals,pars)
    err_vals = np.append(err_vals,errs)
    # close file once done
    fs[ii].close()
  return Z_vals, par_vals, err_vals


def plot_pars(horizontal=True,plot_all=True,plot_verts=True):
  buzzard_breaks = [0, .32, .84, 2.35]
  Z, pars = read_files()
  a,b = [2,5] if horizontal else [5,2]
  c,d = [12,6] if horizontal else [6,12]
  fig, axs = plt.subplots(a, b, sharex=True, figsize=(c,d))
  for ii in range(N_pars):
    ax = axs[ii%2,ii//2] if horizontal else axs[ii//2,ii%2]
    ax.errorbar(Z,pars[:,2*ii],pars[:,2*ii+1],fmt='o',ms=2,capsize=2)
    ax.set_ylabel(plot_lbls[ii])
    if plot_verts: # for breaks in the Buzzard simulation
      xlim = ax.set_xlim(ax.get_xlim())
      ylim = ax.set_ylim(ax.get_ylim())
      for pos in [0,.32,.84,2.35]:
        ax.plot(pos*np.ones(2),ylim,'grey',lw=.5)
    if horizontal:
      if ii == 5: # central
        ax.set_xlabel('Redshift')
    else: # vertical
      if ii > 7: 
        ax.set_xlabel('Redshift')
    if plot_all: # grab all values
      Zv, pv, ev = read_par(ii)
      ax.errorbar(Zv,pv,ev,fmt='o',ms=2,capsize=2,color='k',alpha=.25)
  fig.tight_layout()
  plt.show()


def fit_each(dim=1):
  """
  run fitting for each variable
  good to see which variables need what dimensionality of fit / 
    where to break the fit & make it discrete
  """
  for ii in range(N_pars):
    # analyze each plot
    z, p, e = read_par(ii)
    plt.errorbar(z,p,e,fmt='o',ms=2,capsize=2,color='k',alpha=.25)
    # print fit constants
    print('#' * 72)
    print(plot_lbls[ii])
    # fit using errorbars as given
    fit = zf,zf_err = jackknife_fit(z,p,dim,1/e)
    plt.plot(z,np.poly1d(zf)(z),alpha=.5)
    print("Using errors:")
    print_jkf(fit,2)
    # fit without using errorbars
    fit = zf,zf_err = jackknife_fit(z,p,dim)
    plt.plot(z,np.poly1d(zf)(z),'--',alpha=.5)
    print("Ignoring errors:")
    print_jkf(fit,2)
    # tidy up and display
    plt.xlabel('Redshift')
    plt.ylabel(plot_lbls[ii])
    plt.tight_layout()
    plt.show()


def scatter_off_fit():
  """
  for seeing how much the parameters differ from your fit;
  useful for setting prior bounds / expectations
  """
  from rd6C1M import guess_pars
  rd6_order = np.roll(np.arange(10),2)
  for ii in range(N_pars):
    # analyze each plot
    z, p, e = read_par(ii)
    theta = guess_pars(z)
    p_fit = theta[rd6_order[ii]]
    plt.title(plot_lbls[ii])
    plt.plot(z,z*0,'k')
    plt.errorbar(z,p-p_fit,e,fmt='o',ms=2,capsize=2,color='k',alpha=.25)
    plt.ylabel('Residual')
    plt.xlabel('Redshift')
    plt.show()
  

if __name__=='__main__':
  from sys import argv
  N = int(argv[1]) if len(argv) > 1 else 0
  # plot_pars()
  # fit_each(N)
  scatter_off_fit()
  print('~fin')

