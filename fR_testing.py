import rd_beta
np,plt,h5py = rd_beta.np,rd_beta.plt,rd_beta.h5py

folder_analyzed = '../xtools/analyzed/'
fname_limit_L = folder_analyzed + 'subset_limitL.h5'
fname_limit_M = folder_analyzed + 'subset_muLimit.h5'
fname_limit_r = folder_analyzed + 'subset_rlimit.h5'

fnames_in = [fname_limit_L,fname_limit_M,fname_limit_r]
fnames_out = ['limL.h5','limM.h5','limR.h5']

labels = [r'$L > 0.2 L_*$',r'$\mu>13.5$',r'$r_{\rm halo} < r_{\rm vir}$']


def process_all():
  " create h5 outputs for fits for each subset "
  for in_name,out_name in zip(fnames_in,fnames_out):
    rd_beta.fit_h5(fname=in_name,Z_max=.75,observed_mags=True,
                   fname_out=out_name)
  print('~fin')


def process_rLim():
  " try to fix the r lim "
  rd_beta.fit_h5(fname=fnames_in[2],Z_max=.75,dZ=.02,
                 observed_mags=True,fname_out=fnames_out[2])


def grab_pred(ii=2,dZ=.02):
  " deprecated (ii=2,dZ=.02)"
  fR_grab(fname_in=fnames_in[ii],fname_out="out%i.npy" % ii,dZ=dZ)

def plot_pred(dZ=.02):
  for ii in range(3):
    Z,fR = np.load("out%i.npy" % ii)
    plt.scatter(Z+dZ/2.,fR,label=labels[ii])
  # set up axes
  plt.xlabel('Redshift')
  plt.ylabel('Red Fraction')
  # wrap up & display
  plt.legend()
  plt.tight_layout()
  plt.show()


def analyze():
  files = [h5py.File(fname) for fname in fnames_out]
  for ii in range(3): # for each of the above limits
    f = files[ii]
    Z = f['z_vals'][()]+.005
    plt.scatter(Z,f['weights'][()][:,2],label=labels[ii])
  # set up axes
  plt.xlabel('Redshift')
  plt.ylabel('Weight')
  plt.ylim(0,None)
  # wrap up & display
  plt.legend()
  plt.tight_layout()
  plt.show()


def fR_grab(fname_in,fname_out,dZ=.02,printing=True,band_tags='obs'):
  """
  fname_in : sky dataset, including redshift & magnitudes for galaxies
  fname_out : [redsfhit, red fraction] output file name
  """
  f = h5py.File(fname_in)
  Z = f['redshift'][()]
  if band_tags=='obs':
    band_tags = rd_beta.field_labels_obs[:-1]
  else:
    band_tags = rd_beta.field_labels_in[:-1]
  bands = np.array([f[tag][()] for tag in band_tags])
  z_vals = np.arange(.05,.75,dZ)
  fR = np.zeros(np.shape(z_vals))
  for jj in range(len(z_vals)):
    if printing:
      print(z_vals[jj])
    mask_Z = (z_vals[jj]<=Z) * (Z<z_vals[jj]+dZ)
    p_red = rd_beta.P_red(Z[mask_Z],bands[:,mask_Z])
    Nred,Ngal = sum(p_red),len(p_red)
    fR[jj] = Nred/Ngal
  np.save(fname_out,[z_vals,fR])
  if printing:
    print(f"Saved {fname_out}")


def cf_baselines(fnames=[rd_beta.ds_fname,'limL.h5','limM.h5'],
                 band_tags=['in','obs','obs']):
  """
  use the different input files as baselines for RD
  compare their red fraction estimates on the radius limited sample
  (eventually with different radius cuts)
  """
  for ii in range(len(fnames)):
    print(f"Analyzing {fnames[ii]}")
    rd_beta.analyze_fit(fnames[ii])
    fR_grab(fname_limit_r,f'method_{ii}.npy',band_tags=band_tags[ii])
  

def plot_baseline_cf():
  for ii in range(3):
    Z,fR = np.load(f'method_{ii}.npy')
    plt.plot(Z,fR,label=ii)
  plt.xlabel('Redshift')
  plt.ylabel('Red fraction')
  plt.legend()
  plt.tight_layout()
  plt.show()
