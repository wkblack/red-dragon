# test red dragon vs redmagic --- how well do their RS selections match?

########################################################################
# imports 

exec(open("mpl_tools.py").read()) # import matplotlib tools and defaults

from astropy.io import fits
import rd_gamma
import h5py


########################################################################
# importing data 
print('Reading in data')

fdir = '/global/cscratch1/sd/wkblack/'
fname = 'y3_gold_2.2.1_wide_sofcol+deep_mof_run_redmagic_highdens.fit'
fname = fdir + fname
hdul = fits.open(fname)

data = hdul[1].data

bands = data['mag']
bands_err = data['mag_err']
cols = np.transpose(bands)
g,r,i,z = cols
z_spec = data['zspec']
z_redm = data['zredmagic']
z_redm_err = data['zredmagic_e']

########################################################################
# calculate P_red

fname_Pred = 'redmagic_Pred.npy'
try:
  Pred_spec,Pred_redm = np.load(fname_Pred)
except FileNotFoundError:
  # set up dragon
  print("Forming dragon")
  fname_RD = 'output/fit_Bz_2K.h5'
  rd = rd_gamma.dragon(fname_RD)
  
  print("Calculating P_red for spec...")
  Pred_spec = rd.P_red(z_spec,bands,bands_err,dZ=.0025)
  print("Calculating P_red for redm...")
  Pred_redm = rd.P_red(z_redm,bands,bands_err,dZ=.0025)
  print("Finished P_red calculations")
  np.save(fname_Pred,[Pred_spec,Pred_redm])


########################################################################
# plotting commands 

def plot_gmr():
  " look at all at once "
  for ii in range(2):
    plt.subplot(211 + ii)
    if ii:
      plt.scatter(z_spec,g-r,1,c=Pred_spec,cmap='coolwarm',alpha=.5)
      plt.xlabel('Spectroscopic redshift')
    else:
      plt.scatter(z_redm,g-r,1,c=Pred_redm,cmap='coolwarm',alpha=.5)
      plt.xlabel('redMaGiC redshift')
    
    plt.xlim(0,.9)
    plt.ylabel(r'$(g-r)$')
    plt.ylim(.5,3)
  
  plt.tight_layout()
  plt.show()

def plot_gmr_spec(saving=True):
  " look at all at once "
  plt.scatter(z_redm,g-r,1,c=Pred_redm,cmap='coolwarm',alpha=.5)
  # set axes
  plt.xlabel('redMaGiC redshift')
  plt.xlim(.05,.75)
  plt.ylabel(r'$(g-r)$')
  plt.ylim(.5,3.5)
  # tidy up, save, & display 
  plt.colorbar(label=r'$P_{\rm red}$')
  plt.tight_layout()
  if saving:
    outname = 'gmr_spec.pdf'
    plt.savefig(outname,format='pdf',dpi=1000)
    print("Saved",outname)
  plt.show()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

fname_NNRR = 'NgalNredRMRD.h5'

def analyze_zbins(fname_out=fname_NNRR):
  " analyze z-bin by z-bin "
  z_bins = np.linspace(.05,.75,1+7*4)
  N_red_spec,N_gal_spec,N_red_redm,N_gal_redm = np.zeros((4,len(z_bins)-1))
  
  for ii in range(len(z_bins)-1):
    print(f'Analyzing z|[{z_bins[ii]:0.3g},{z_bins[ii+1]:0.3g})')
    mask_zs = (z_bins[ii] <= z_spec) * (z_spec < z_bins[ii+1])
    mask_zr = (z_bins[ii] <= z_redm) * (z_redm < z_bins[ii+1])
    # summed stats:
    N_gal_spec[ii] = len(Pred_spec[mask_zs])
    N_gal_redm[ii] = len(Pred_redm[mask_zr])
    N_red_spec[ii] = np.sum(Pred_spec[mask_zs])
    N_red_redm[ii] = np.sum(Pred_redm[mask_zr])
  
  with h5py.File(fname_out,'w') as f:
    f.create_dataset('N_red_spec',data=N_red_spec)
    f.create_dataset('N_gal_spec',data=N_gal_spec)
    f.create_dataset('N_red_redm',data=N_red_redm)
    f.create_dataset('N_gal_redm',data=N_gal_redm)
    f.create_dataset('z_bins',data=z_bins)
    f.attrs['notes'] = "Coded 10/10/2020. Thanks Fletcher!"
  print("~fin")


def plot_zbins(fname_in=fname_NNRR,saving=True,xlim=[.1,.7],ylim=[.9,1]):
  with h5py.File(fname_in,'r') as f:
    Nrs = f['N_red_spec'][()]
    Ngs = f['N_gal_spec'][()]
    Nrr = f['N_red_redm'][()]
    Ngr = f['N_gal_redm'][()]
    z_bins = f['z_bins'][()]
    print(f.attrs['notes'])
  z_med = (z_bins[1:] + z_bins[:-1])/2.
  if not saving:
    plt.title('Red Dragon cf. redMaGiC Y3A2 High Dens')
  plt.scatter(z_med,Nrs/Ngs,label=r'$z_{\rm spec}$')
  plt.scatter(z_med,Nrr/Ngr,label=r'$z_{\rm redM}$')
  plt.xlabel(r'Redshift')
  plt.xlim(xlim)
  plt.ylabel(r'$\sum P_{\rm red} \, / \, N_{\rm gal}$')
  plt.ylim(ylim)
  plt.legend(loc='lower right')
  plt.tight_layout()
  if saving:
    outname = 'zbins.pdf'
    plt.savefig(outname,format='pdf',dpi=1000)
    print("Saved",outname)
  plt.show()


def plot_counts(fname_in=fname_NNRR):
  with h5py.File(fname_in,'r') as f:
    Nrs = f['N_red_spec'][()]
    Ngs = f['N_gal_spec'][()]
    Nrr = f['N_red_redm'][()]
    Ngr = f['N_gal_redm'][()]
    z_bins = f['z_bins'][()]
    print(f.attrs['notes'])
  z_med = (z_bins[1:] + z_bins[:-1])/2.
  plt.plot(z_med,Nrs,label=r'$N_{\rm red}$ spec')
  plt.plot(z_med,Ngs,label=r'$N_{\rm gal}$ spec')
  plt.plot(z_med,Nrr,label=r'$N_{\rm red}$ redM')
  plt.plot(z_med,Ngr,label=r'$N_{\rm gal}$ redM')
  plt.xlabel(r'Redshift')
  plt.ylabel(r'$N$')
  # plt.ylim(None,1)
  plt.yscale('log')
  plt.legend()
  plt.tight_layout()
  plt.show()


if __name__=='__main__':
  # analyze_zbins()
  # plot_gmr()
  plot_zbins(ylim=[.75,1])
  # plot_counts()
  plot_gmr_spec(saving=False)
  print('~fin')
