# read in Buzzard default catalog, 
# output vetted version
# start with overall magnitude limited sample
# then use that as baseline to vet further down

########################################################################
# import statements

import h5py
import numpy as np


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# m* function

extrap_warn="WARNING: Extrapolating redshift! [m_star_model.m_star()]"

def m_star_Bz(z,warning=True):
  " Buzzard m* value as measured by WKB ~Feb 2021 "
  lims = [.0015,2.29] # redshift limits under which this measurment was taken
  extrapolating = np.any(z<lims[0]) or np.any(z>lims[-1]) # outside limits
  if warning and extrapolating: # warn of outliers
    print(extrap_warn)
  # fit parameters from xtools/m_star_plot.py
  Z1 = [ 2.85703803, 23.35451485]
  Z2 = [ -0.13290108,  1.11453393, -3.74351207, 6.96351527, 
        -10.04310863, 12.60755443, -7.85820367, 0.92175766]
  # model fit: offset + #*log z + polynomial correction
  m_star = np.poly1d(Z1)(np.log(z)) + np.poly1d(Z2)(z)
  return m_star


########################################################################
# set up Buzzard default catalogue

dir_Herd = '/project/projectdirs/des/jderose/Chinchilla/Herd/'
mcat_200 = dir_Herd + 'Chinchilla-3/v2.0.0/sampleselection/Y3a/Buzzard-3_v2.0_Y3a_mastercat.h5'

loc_Z = 'catalog/bpz/unsheared/redshift_cos' # cosmological redshift
loc_gold = 'catalog/gold/'
loc_band_true = loc_gold + 'mag_%s_true' # true magnitudes
loc_band_obs = loc_gold + 'mag_%s' # observed magnitudes
loc_band_err = loc_gold + 'mag_err_%s' # error on magnitudes

loc_Mvir = loc_gold + 'm200' # Msol/h; virial mass
loc_Rvir = loc_gold + 'r200' # ckpc/h; virial radius; NOTE: divide by 1000
loc_rhalo = loc_gold + 'rhalo' # cMpc/h; distance from nearest (sub)halo

lbl_bands = ['g','r','i','z']



# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# vetting functions

fdir_scratch = '/global/cscratch1/sd/wkblack/'
fdir_bz = fdir_scratch + 'SkyMaps/buzzard/vet/'
fname_Lvet = fdir_bz + 'Lvet.h5'

def Lvet(N=10**4,Z_lim=[.0015,2.29]):
  """
  create luminosity-vetted base file to work off of (m* + 1.75)
  Inputs
  ------
  N=10**4: number of galaxies to sample. defaults to small number for 
           testing purposes. use -1 for full dataset
  Z_lim=[.0015,2.29]: limits of redshift range of analysis. defaults to
                      bounds of m* function. 
  """
  f = h5py.File(mcat_200,'r')
  Z = f[loc_Z][:N]
  mask_Z = (Z_lim[0] < Z) * (Z < Z_lim[1]) # default Z_lim masks little
  m_star = m_star_Bz(Z[mask_Z])
  m_i = f[loc_band_true % 'i'][:N][mask_Z]
  mask_mag = m_i < m_star + 1.75
  mask = np.copy(mask_Z)
  mask[mask] = mask_mag
  for band in [f[loc_band_obs % b][:N] for b in lbl_bands]:
    mask = np.logical_and(mask,(band != 0) * (band < 99))
  for band in [f[loc_band_obs % b][:N] for b in lbl_bands]:
    assert(max(band[mask] < 99))
  # save masked output
  with h5py.File(fname_Lvet,'w') as ds:
    ds.create_dataset('redshift_cos',data=Z[mask])
    ds.create_dataset('m_star',data=m_star[mask[mask_Z]])
    for key in f[loc_gold].keys():
      ds.create_dataset(key,data=f[loc_gold+key][:N][mask])
    for key,val in zip(['L_lim','Z_lim','N'],
                       ['0.2L*', Z_lim , N ]):
      ds.attrs[key] = val
  print(fname_Lvet,'created.')


########################################################################
# vetter tool to select sub-samples of the data

def vetter(fname_out,mag_lim=None,mu_lim=None,r_lim=None,Z_lim=None,
           true_colors=False):
  """
  create subset of dataset based on a few key keys
  limiters: set to [low,high] to select data
            all limiters follow "[*,*)", i.e. low <= val < high
   * mag_lim=None: relative to m*(Z), 
                   e.g. to select L>0.2L*, use mag_lim=[-np.inf,1.75]
   * mu_lim=None: log10(Mvir/Msol) limits
   * r_lim=None: radial limits, relative to rhalo, 
                 e.g. to select r<.5Rvir, use r_lim=[0,.5]
   * Z_lim=None: redshift limits
  """
  f = h5py.File(fname_Lvet,'r') # input file from above
  Z = f['redshift_cos'][:] # cosmological redshift
  N = len(Z) # number of galaxies in this sample
  mask_tot = np.ones(N).astype(bool)
  
  if mag_lim is not None: # vet by Luminosity
    m_star = f['m_star'][:] # mag
    m_i = f['mag_i_true'][:] # mag
    x = m_i - m_star # mag
    mask_tot *= (mag_lim[0] <= x) * (x < mag_lim[1])
  
  if mu_lim is not None: # vet by mass
    mu = np.log10(f['m200'][:]/.7) # unitless (log Mvir/Msol)
    mask_tot *= (mu_lim[0] <= mu) * (mu < mu_lim[1])
  
  if r_lim is not None: # vet by distance to nearest halo
    rhalo = f['rhalo'][:] # Mpc/h
    rvir = f['r200'][:]/1000 # Mpc/h
    x = rhalo/rvir # unitless ratio
    mask_tot *= (r_lim[0] <= x) * (x < r_lim[1])
  
  if Z_lim is not None: # vet by redshift
    mask_tot *= (Z_lim[0] <= Z) * (Z < Z_lim[1])
  
  # set up variables to save
  Z = Z[mask_tot] # cosmological redshift
  Z_err = Z * 0 # cosmological redshift has no error
  bands = np.transpose([f['mag_'+b][mask_tot] for b in lbl_bands])
  bands_err = np.transpose([f['mag_err_'+b][mask_tot] for b in lbl_bands])
  
  f.close()
  
  with h5py.File(fname_out,'w') as ds:
    # save datasets (only RD gamma fields)
    for key,vals in zip(['Z','Z_err','bands','bands_err'],
                        [ Z , Z_err , bands , bands_err ]):
      ds.create_dataset(key,data=vals)
    # save attributes
    for key,val in zip(['mag_lim','mu_lim','r_lim','Z_lim'],
                       [ mag_lim , mu_lim , r_lim , Z_lim ]):
      ds.attrs[key] = val if val is not None else "None"
    for key,val in zip(['N','true_colors'],
                       [ N , true_colors ]):
      ds.attrs[key] = val
  print(f"Saved {fname_out}")



########################################################################
# grab subset

if 1:
  # vet to smaller subsets
  mag_lims = [[0,2],[-2,0]]
  mu_lims = [[13.75,14],[14,14.25]]
  r_lims = [[1,np.inf],[0,1]]
  vetter(fdir_bz+'rd_Lvet_000.h5',mag_lim=mag_lims[0],mu_lim=mu_lims[0],r_lim=r_lims[0])
  vetter(fdir_bz+'rd_Lvet_001.h5',mag_lim=mag_lims[0],mu_lim=mu_lims[0],r_lim=r_lims[1])
  vetter(fdir_bz+'rd_Lvet_010.h5',mag_lim=mag_lims[0],mu_lim=mu_lims[1],r_lim=r_lims[0])
  vetter(fdir_bz+'rd_Lvet_011.h5',mag_lim=mag_lims[0],mu_lim=mu_lims[1],r_lim=r_lims[1])
  vetter(fdir_bz+'rd_Lvet_100.h5',mag_lim=mag_lims[1],mu_lim=mu_lims[0],r_lim=r_lims[0])
  vetter(fdir_bz+'rd_Lvet_101.h5',mag_lim=mag_lims[1],mu_lim=mu_lims[0],r_lim=r_lims[1])
  vetter(fdir_bz+'rd_Lvet_110.h5',mag_lim=mag_lims[1],mu_lim=mu_lims[1],r_lim=r_lims[0])
  vetter(fdir_bz+'rd_Lvet_111.h5',mag_lim=mag_lims[1],mu_lim=mu_lims[1],r_lim=r_lims[1])


# Lvet(2*10**8)
Lvet(-1)


