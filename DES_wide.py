# read in DES wide fields & process

exec(open("mpl_tools.py").read()) # import matplotlib tools and defaults

import h5py

fname = '/global/project/projectdirs/des/www/y3_cats/Y3_mastercat_02_05_21.h5'
f = h5py.File(fname,'r')


########################################################################
# read in bands (TODO: figure out which tag is best... probably not this one)
print("Reading in DES bands")

key_band = 'catalog/gold/' + ['mag_auto_','sof_cm_mag_corrected_'][1]
key_berr = 'catalog/gold/' + ['magerr_auto_','sof_cm_mag_err_'][1]
DES_bands = ['g','r','i','z']

bands = np.transpose([f[key_band + c][:] for c in DES_bands])
g,r,i,z = np.transpose(bands)
cols = -np.diff(bands,axis=1)
bands_err = np.transpose([f[key_berr + c][:] for c in DES_bands])

########################################################################
# read in redshift information
print("Reading in redshift information") 

ZMC_BPZ = f['/catalog/bpz/unsheared/zmc_sof'][:]
ZMEAN_BPZ = f['/catalog/bpz/unsheared/zmean_sof'][:]
dZ_BPZ = ZMEAN_BPZ - ZMC_BPZ

# suggested by Aurelio Carnero to use DNF_ZMEAN_SOF
ZMC_DNF = f['/catalog/dnf/unsheared/zmc_sof'][:]
ZMEAN_DNF = f['/catalog/dnf/unsheared/zmean_sof'][:]
dZ_DNF = ZMEAN_DNF - ZMC_DNF


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# sort out sigma-Z values
fname_ZSIG = '/global/cscratch1/sd/wkblack/SkyMaps/DES/Z_SIGMA_DNF.npy'
if 0: 
  print("Reading in sigma Z values from fits :c") 
  fname = '/project/projectdirs/des/jvicente/y3/sof/Y3A2_SOF_DNF_V4_NVP.fits'
  # load file
  from astropy.table import Table
  dat = Table.read(fname, format='fits')
  df = dat.to_pandas()
  # sort by object ID
  ID_h5 = f['index/coadd_object_id'][:]
  import pandas as pd
  srs_ID = pd.Series(ID_h5,name='COADD_OBJECT_ID')
  df = pd.merge(srs_ID,df,how='left')
  Z_SIGMA = df['Z_SIGMA'][:].values
  np.save(fname_ZSIG,Z_SIGMA)
  print(fname_ZSIG,"saved!")
else:
  Z_SIGMA = np.load(fname_ZSIG)
  Z_SIGMA = Z_SIGMA[~np.isnan(Z_SIGMA)]

assert(len(Z_SIGMA)==len(ZMEAN_DNF))

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


Z = ZMEAN_DNF
# Z_err = np.abs(dZ_DNF)
Z_err = Z_SIGMA


# maybe cross-check with SOMPZ bins? 

dZ_max = .25
mask_dZ = (Z_SIGMA < dZ_max)


########################################################################
# grab all flags

print("Grabbing flags") 

fs,badregions,footprint,foreground,gold,phot = [f['/catalog/gold/flags'\
                                                    + k][:].astype(bool)
  for k in ['','_badregions','_footprint','_foreground','_gold','_phot']]

# gold photometry selection for photo-z use: 
# taken from cdcvs.fnal.gov/redmine/projects/des-sc/wiki/Y3kp_catalogs
mask_gold_photo = (foreground==0) * (badregions<2) * (gold<2) * (footprint==1)

# my own mask to throw out noisy data
# mask_Wm = (g+r < 99) * (i+z < 99) * (ZMC_DNF > 0) * (ZMEAN_DNF > 0) * (np.abs(dZ_DNF) < .05)
mx = 37.5 # hardcoded maximum magnitude across all bands
mask_bands = (g < mx) * (r < mx) * (i < mx) * (z < mx)

mask_Wm = mask_gold_photo * (~phot.astype(bool)) * mask_bands * (ZMEAN_DNF!=3.09) * mask_dZ * (Z > 0)

# mask_bright = z - 5 * np.log10(Z) < 22 # loosely chosen by me; good out to Z~1.3


########################################################################
# set up cosmology cut

print("Setting up cosmology cut")

from colossus.cosmology import cosmology
cosmo = cosmology.setCosmology('planck18', {'print_warnings': False})

dL_Wm = cosmo.luminosityDistance(Z[mask_Wm]) / cosmo.h # Mpc
Mg,Mr,Mi,Mz = [mag[mask_Wm] - 25 - 5 * np.log10(dL_Wm) for mag in [g,r,i,z]]

mask_bright = Mz < -21.8 # good to Z=1.25
# -22 good to Z>1.2

########################################################################
# overwrite data for saving

print("Prepping data for saving")

Z,Z_err,bands,bands_err = [v[mask_Wm][mask_bright] for v in \
                           [Z,Z_err,bands,bands_err]]
note = f"using gold photo mask, dZ < {dZ_max}, & Mz < -21.8"


########################################################################
# save RD-friendly output as hdf5

print("Saving output")

import h5py
fdir = '/global/cscratch1/sd/wkblack/SkyMaps/DES/'
fname_out = fdir + 'DES_wide_bright.h5'

with h5py.File(fname_out,'w') as ds:
  for label,dset in zip(['Z','Z_err','bands','bands_err'],
                        [ Z , Z_err , bands , bands_err ]):
    ds.create_dataset(label,data=dset)
  for label,attr in zip(['note','fname origin'],
                        [ note , fname ]):
    ds.attrs[label] = attr

print(fname_out,'saved!')

print('~fin')
