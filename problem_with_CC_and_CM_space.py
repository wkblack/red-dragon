# show off what's wrong with a flat CC or CM cut
import h5py
import numpy as np
from matplotlib import pyplot as plt
from sklearn.mixture import GaussianMixture

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# set up golden aspect ratio
golden_ratio = (1+np.sqrt(5))/2.
golden_aspect = 5*np.array([golden_ratio,1])

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# set up default plotting paradigms
plt.rcParams['figure.figsize'] = golden_aspect
plt.rcParams['font.size'] = 15

########################################################################
# set up defaults

fname = '/global/cscratch1/sd/wkblack/SkyMaps/buzzard/vet/subset_new_L.h5'
Z_lim = [.6,.601]
Z_lim = [.37,39] # at 4kA break
N_fit = 2 # number of Gaussians to fit the data with

# set up two colors + magnitude to use in fit
c0,c1 = 0,3 # color indices
m0 = 2 # magnitude index



########################################################################
# read in data

f = h5py.File(fname,'r')
# ['coadd_object_id', 'dec', 'flags_gold', 'flux_g', 'flux_i', 'flux_r', 'flux_z', 'haloid', 'hpix_16384', 'ivar_g', 'ivar_i', 'ivar_r', 'ivar_z', 'm200', 'mag_err_g', 'mag_err_i', 'mag_err_r', 'mag_err_z', 'mag_g', 'mag_g_lensed', 'mag_g_true', 'mag_i', 'mag_i_lensed', 'mag_i_true', 'mag_r', 'mag_r_lensed', 'mag_r_true', 'mag_z', 'mag_z_lensed', 'mag_z_true', 'mcal_flux_i', 'mcal_flux_r', 'mcal_flux_z', 'mcal_ivar_i', 'mcal_ivar_r', 'mcal_ivar_z', 'px', 'py', 'pz', 'r200', 'ra', 'redshift_cos', 'rhalo', 'sdss_sedid', 'vx', 'vy', 'vz', 'z', 'zmc_sof', 'zmean_sof']
Z = f['redshift_cos'][:]
mask_Z = (Z_lim[0] <= Z) * (Z < Z_lim[1])
bands = np.transpose([f['mag_'+col][mask_Z] for col in ['g','r','i','z']])

mask_c = np.any(bands==99,axis=1)
bands = bands[~mask_c]
Z = Z[mask_Z][~mask_c]

g,r,i,z = [bands[:,ii] for ii in range(4)]

########################################################################
# measure RS & BC w/ GM

c = np.transpose([g-r,g-i,g-z,r-i,r-z,i-z])
mask_RS = -np.ones(len(Z))
mi_bins = np.append(min(i),np.linspace(min(i)+1,max(i)+.001,16))
mag = bands[m0]

for ii in range(len(mi_bins)-1):
  mask_ii = (mi_bins[ii] <= i) * (i < mi_bins[ii+1])
  fit = GaussianMixture(N_fit).fit(c[mask_ii])
  x_pred = fit.predict(c[mask_ii])
  # fix ordering
  mask = x_pred.astype(bool)
  if np.mean(c[mask_ii,m0][mask]) < np.mean(c[mask_ii,m0][~mask]):
    x_pred = 1 - x_pred
  mask_RS[mask_ii] = x_pred

mask_RS = mask_RS.astype(bool)

########################################################################
# plot up problem

fig, axs = plt.subplots(1, 2, gridspec_kw={'width_ratios': [1, 2]}, sharey=True)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# set up CC space
axs[0].scatter(c[mask_RS,c0],c[mask_RS,c1],max(bands[m0])-bands[m0][mask_RS],
               c=i[mask_RS],cmap='autumn',lw=0,alpha=.25)
axs[0].scatter(c[~mask_RS,c0],c[~mask_RS,c1],max(bands[m0])-bands[m0][~mask_RS],
               c=i[~mask_RS],cmap='winter',lw=0,alpha=.25)
# set up axes
axs[0].set_xlabel(r'$(r-i)$')
axs[0].set_xlim(.1,1.2)

axs[0].set_ylabel(r'$(g-r)$')
# axs[0].set_ylim(.5,3.5) # max(0,ylim[0]),min(5,ylim[1]))
axs[0].set_ylim(.5,2.25) # max(0,ylim[0]),min(5,ylim[1]))

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# set up CM space
axs[1].scatter(i[mask_RS],(g-r)[mask_RS],max(i)-i[mask_RS],
            c=i[mask_RS],cmap='autumn',lw=0,alpha=.25)
axs[1].scatter(i[~mask_RS],(g-r)[~mask_RS],max(i)-i[~mask_RS],
            c=i[~mask_RS],cmap='winter',lw=0,alpha=.25)
if 1: # fit w/ KLLR
  from kllr import kllr_model
  lm = kllr_model(kernel_width=0.2)
  for mask in [mask_RS,~mask_RS]:
    xr,yrm,icpt,sl,sig = lm.fit(i[mask],(g-r)[mask],nbins=100)
    plt.plot(xr,yrm,'k')
    plt.fill_between(xr,yrm-sig,yrm+sig,alpha=.125,color='k',lw=0)
  
# set up axes
axs[1].set_xlabel(r'$m_i$')
axs[1].set_xlim(None,22.5)

plt.tight_layout()
fname_out = 'cf__CC_CM.pdf'
plt.savefig(fname_out,format='pdf',dpi=1000)
print(f"Saved {fname_out}")
plt.show()

