########################################################################
# Kernel localized balanced accuracy
# Idea: give classifications, but in a localized way, like KLLR

import numpy as np

from kllr.regression_model import calculate_weigth as weights
# w[ii] = np.sum(weights(x,kernel_type=kt,mu=xr[ii],width=kw))


def classify(T,P,w=None):
  """
  Returns confusion matrix elements:
  TP = true positives
  FP = type I error
  FN = type II error
  TN = true negatives
  """
  if w is None:
    w = np.ones_like(T)
  T = np.array(T).astype(bool)
  P = np.array(P).astype(bool)
  masks = [T*P, (~T)*P, T*(~P), (~T)*(~P)] # TP, FP, FN, TN
  return [len(T[msk]) * np.sum(w[msk]) / len(w[msk]) for msk in masks]


def KLC(x,T,P,kw=None,xlim=None,N_points=20,N_runs=1):
  N_vals = len(T)
  if xlim is None:
    xlim = np.quantile(x,[0,1]) # span full range
  x_vals = np.linspace(*xlim,N_points)
  if kw is None: 
    kw = (xlim[1] - xlim[0]) / 10 # tenth of range
  # set up return variables
  classifications = classify(T,P)
  report = np.zeros((N_points,N_runs,4)) # TP, FP, FN, TN
  for jj in range(N_runs):
    if N_runs > 1:
      choice = np.random.choice(N_vals,N_vals)
    else:
      choice = np.ones(N_vals).astype(bool)
    for ii in range(N_points):
      # set up weights using KLLR 
      w = weights(x[choice],kernel_type='gaussian',mu=x_vals[ii],width=kw)
      # measure classification at each point, weighted by kernel
      report[ii,jj] = classify(T[choice],P[choice],w)
  # return classifications as functions of x
  return x_vals,report



