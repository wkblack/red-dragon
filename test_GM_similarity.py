# test how close primary colors are to full color space
########################################################################
# import statements

# remove deprecation warnings
import warnings # to shut up sklearn
warnings.filterwarnings("ignore",category=DeprecationWarning)

# limit thread count
import os 
os.environ['OPENBLAS_NUM_THREADS'] = '1'

# set up main body of tools
exec(open("mpl_tools.py").read()) # import matplotlib tools and defaults
import fit_CCM
GM = fit_CCM.GM; plt = fit_CCM.plt; np = fit_CCM.np

# set up pygmmis (limit thread count)
# os.environ['OPENBLAS_NUM_THREADS'] = '12'
# os.environ['RLIMIT_NPROC'] = '47'
import pygmmis

# set up checks for singular matrices
import sys
epsilon = sys.float_info.epsilon
MAX = 1/epsilon


########################################################################
# import data

if 1:
  data = fit_CCM.read_SDSS()
  pivot = 10**-11.5
else:
  data = fit_CCM.read_TNG()
  pivot = 10**-11.105

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# unpack color data

N_col = len(data["cols"])
bands = [data[c] for c in data["cols"]]
bands_err = [data['err_'+c] for c in data["cols"]]

sig_off = (np.transpose(bands)-np.mean(bands,axis=1))/np.std(bands,axis=1)
sig_tot = np.sqrt(np.sum(sig_off**2,axis=1))

u,g,r,i,z = bands
ue,ge,re,ie,ze = bands_err
vu,vg,vr,vi,vz = np.array(bands_err)**2 # variances
ZR0 = np.zeros_like(u) # zeros


#  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  
# primary colors
X1 = np.transpose(-np.diff(bands,axis=0)) 
X1_err = np.transpose(np.sqrt([vu+vg,vg+vr,
                               vr+vi,vi+vz]))
X1_covar = np.transpose(
  [[vu+vg,   -vg,   ZR0,   ZR0],
   [  -vg, vg+vr,   -vr,   ZR0],
   [  ZR0,   -vr, vr+vi,   -vi],
   [  ZR0,   ZR0,   -vi, vi+vz]])

#  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  
# secondary colors
X2 = np.transpose([u-r,u-i,u-z,g-i,g-z,r-z]) 
X2_err = np.transpose(np.sqrt([vu+vr,vu+vi,vu+vz,
                               vg+vi,vg+vz,vr+vz]))
X2_covar = np.transpose([
    [vr+vu, vu, vu,ZR0,ZR0, -vr],
    [vu, vi+vu, vu, vi,ZR0,ZR0],
    [vu, vu, vu+vz,ZR0, vz, vz],
    [ZR0, vi,ZR0, vg+vi, vg,ZR0],
    [ZR0,ZR0, vz, vg, vg+vz, vz],
    [-vr,ZR0, vz,ZR0, vz, vr+vz]
  ])
#  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  
# all colors
X0 = np.concatenate([X1,X2],axis=1)
X0_err = np.append(X1_err,X2_err,axis=1)
X0_covar = np.transpose([
    [vu + vg, -vg,ZR0,ZR0, vu, vu, vu, -vg, -vg,ZR0],
    [-vg, vg + vr, -vr,ZR0, vr,ZR0,ZR0, vg, vg, -vr], 
    [ZR0, -vr, vi + vr, -vi, -vr, vi,ZR0, vi,ZR0, vr],
    [ZR0,ZR0, -vi, vi + vz,ZR0, -vi, vz, -vi, vz, vz],
    [vu, vr, -vr,ZR0, vr + vu, vu, vu,ZR0,ZR0, -vr],
    [vu,ZR0, vi, -vi, vu, vi + vu, vu, vi,ZR0,ZR0],
    [vu,ZR0,ZR0, vz, vu, vu, vu + vz,ZR0, vz, vz], 
    [-vg, vg, vi, -vi,ZR0, vi,ZR0, vg + vi, vg,ZR0],
    [-vg, vg,ZR0, vz,ZR0,ZR0, vz, vg, vg + vz, vz],
    [ZR0, -vr, vr, vz, -vr,ZR0, vz,ZR0, vz, vr + vz] 
  ])


def get_submatrix(X,indices):
  if not hasattr(indices,'__len__'):
    indices = [indices]
  N,M,M = np.shape(X)
  report = np.copy(X)
  nix = list(set(range(M)).difference(indices))
  for ii in [-1,-2]:
    report = np.delete(report,nix,ii)
  return report


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# read in sSFR dtaa and set up standard of truth

sSFR = data['sSFR']
T = truth = sSFR < 10**-11.5 # -11.105 for TNG
L = len(T) # total number of elements analyzed here
C = fit_CCM.Classification

lbls_bands = ['$u$', '$g$', '$r$', '$i$', '$z$']
lbls_col1 = ["$u-g$", "$g-r$", "$r-i$", "$i-z$"]
lbls_col2 = ["$u-r$", "$u-i$", "$u-z$", "$g-i$", "$g-z$", "$r-z$"]
lbls_col0 = np.array(lbls_col1 + lbls_col2)
band_jumps = [1, 1, 1, 1, 2, 3, 4, 2, 3, 2]
bands_incl = [\
 # u,g,r,i,z
  [1,1,0,0,0], # u-g
  [0,1,1,0,0], # g-r
  [0,0,1,1,0], # r-i
  [0,0,0,1,1], # i-z
  [1,0,1,0,0], # u-r
  [1,0,0,1,0], # u-i
  [1,0,0,0,1], # u-z
  [0,1,0,1,0], # g-i
  [0,1,0,0,1], # g-z
  [0,0,1,0,1]  # r-z
]
bands_matr = [\
 # u,g,r,i,z   # matrix form now for linear dependence
  [1,-1,0,0,0], # u-g
  [0,1,-1,0,0], # g-r
  [0,0,1,-1,0], # r-i
  [0,0,0,1,-1], # i-z
  [1,0,-1,0,0], # u-r
  [1,0,0,-1,0], # u-i
  [1,0,0,0,-1], # u-z
  [0,1,0,-1,0], # g-i
  [0,1,0,0,-1], # g-z
  [0,0,1,0,-1]  # r-z
]


truth = data['sSFR'] < pivot

from scipy.special import erf
one_sigma = s1 = erf(1/np.sqrt(2))
two_sigma = s2 = erf(2/np.sqrt(2))
thr_sigma = s3 = erf(3/np.sqrt(2))


########################################################################
# determine how close selection btw X0 and X1 are

# what I really care about is how close the two selections are, 
# not how close they are to the truth. 
# So for now, I'll ignore that. 


def fix_pred(pred,X):
  " resort pred by scatter in X "
  # determine number of components in fit
  N_fit = max(pred) + 1
  # calculate norm of median color for each fit
  means = [np.median(X[pred==ii,:4],axis=0) for ii in range(N_fit)]
  norms = np.linalg.norm(means,axis=1)
  # calculate scatter in color for each fit
  spreads = np.zeros(N_fit)
  for ii in range(N_fit):
    spreads[ii] = np.std(X[pred==ii,:4])
  # use my metric to sort things
  wm_metric = norms - 1.5 * spreads # redder and less spread out
  sort = np.flip(np.argsort(wm_metric)) # so 0 = RS hopefully
  # create output report
  report = np.ones(len(pred))
  for ii in range(N_fit):
    # report[pred==ii] = sort[ii]
    report[pred==sort[ii]] = ii
  return report


def test(Xa,Xb,N_fit=2,plotting=False):
  pred_a = GM(N_fit).fit_predict(Xa)
  pred_a = fix_pred(pred_a,Xa) # correct ordering of pred
  pred_b = GM(N_fit).fit_predict(Xb)
  pred_b = fix_pred(pred_b,Xb) # correct ordering of pred
  bACC = fit_CCM.Classification(pred_a==0,pred_b==0).get_BA()
  if plotting and bACC < .9:
    plt.subplot(121)
    plt.scatter(Xa[:,1],Xa[:,2],3,lw=0,alpha=.125,c=pred_a)
    plt.subplot(122)
    plt.scatter(Xb[:,1],Xb[:,2],3,lw=0,alpha=.125,c=pred_b)
    plt.show()
  return bACC #if (bACC > .5) else (1 - bACC) 


def bootstrap(N_runs=50,N_fit=2,rerun_only=False):
  bACC_vals = np.zeros(N_runs)
  L = len(data['sSFR']) # number of galaxies in total 
  for ii in range(N_runs):
    if rerun_only:
      mask = np.ones(L).astype(bool)
    else:
      mask = np.random.choice(L,L)
    bACC_vals[ii] = test(X0[mask],X1[mask],N_fit)
    print(ii+1,bACC_vals[ii])
  mn,st = [f(bACC_vals) for f in [np.mean,np.std]]
  print(f"bACC = {mn:0.3g} \pm {st:0.3g}")
  qtls = np.quantile(bACC_vals,[1-s2,1-s1,.5,s1,s2])
  print("quantiles [-2,-1,0,+1,+2]:",np.round(qtls,5))
  return bACC_vals


def print_median_info(vals):
  qtls = np.quantile(vals,[1-s1,.5,s1])
  mn,pl = np.diff(qtls)
  print(f"median value and spread: {qtls[1]:0.5g} ^+{pl:0.3g} _-{mn:0.3g}")


def test_fitting_accuracy(N_runs=50,N_fit=2,rerun_only=False,plotting=True):
  " test fitting accuracy between X0 and X1 "
  bv = bootstrap(N_runs,N_fit,rerun_only)
  print_median_info(bv)
  print("min,med,max:",np.quantile(bv,[0,.5,1]))
  if plotting:
    plt.hist(bv,N_runs//2)
    plt.show()


def plot_fitting_accuracy(N_runs=50):
  " hardcoded results from above quantiles "
  means = [96.54, 99.03 , 98.02, 87.586]
  mins =  [ 0.5 ,  0.12 ,  1.4 , 15.6  ]
  plus =  [ 2.7 ,  0.125,  0.5 , 4.46  ]
  N_comp = np.arange(4) + 2
  plt.errorbar(N_comp,means,(mins,plus),fmt='o',capsize=5)
  plt.xlabel(r'Number of components')
  plt.xticks(N_comp)
  plt.ylim(None,100)
  plt.ylabel(r'Balanced Accuracy (%)')
  plt.title(r'Similarity between primary and full color vectors')
  plt.show()


########################################################################
# bootstrap BIC values

def bootstrap_BIC(X,N_fit_max=5,N_runs=50,rerun_only=False,N_init=0,
                  use_pygmmis=False,covar=None):
  BIC_vals = np.zeros((N_runs,N_fit_max))
  # grab a few important values
  n,D = np.shape(X) # N_gal, N_col
  for ii in range(N_runs):
    if rerun_only:
      " just rerun sklearn, since it varies time to time "
      mask = np.ones(n).astype(bool)
    else:
      " do proper bootstrapping "
      mask = np.random.choice(n,n)
    for jj in range(N_init,N_fit_max):
      print(ii,jj) # start of round
      K = jj + 1 # number of fit components
      k = K * (D**2 + D + 1) - 1 # number of fit parameters
      if use_pygmmis:
        # calculate BIC from pygmmis
        """
        BIC = k ln n - 2 ln L
          k = number of parameters 
          n = number of galaxies
          L = likelihood
        
        from sklearn: bic = (-2 * self.score(X) * X.shape[0] +
                             self._n_parameters() * np.log(X.shape[0]))
        """
        try:
          gmm = pygmmis.GMM(K=K, D=D) # K components, D dimensions
          logL,U = pygmmis.fit(gmm, X[mask], covar[mask], 
                             init_method='kmeans') # logL = log likelihood
          # assert(not np.isnan(logL))
          if np.isnan(logL):
            print("nan logL without singular matrix.")
        except np.linalg.LinAlgError:
          print("Probably singular matrix problem. Setting logL = nan")
          logL = np.nan
        BIC_vals[ii,jj] = k * np.log(n) - 2 * logL # calculate BIC
      else:
        fit = GM(K).fit(X[mask])
        BIC_vals[ii,jj] = fit.bic(X[mask])
  return BIC_vals


fname_BIC_data = 'output/BIC_ext.npy'
def generate_BIC_data(N_fit_max=15,N_runs=50,fname_out=fname_BIC_data,
                      N_init=0,use_pygmmis=False):
  """
  generate BIC bootstrap data to find optimal number of components
  for Gaussian mixture, for either primary or full color vectors
  """
  # for primary color vector
  bv1 = bootstrap_BIC(X1,N_fit_max,N_runs,False,N_init,use_pygmmis,X1_covar)
  mn1,st1 = [f(bv1,axis=0) for f in [np.nanmean,np.nanstd]]
  if not use_pygmmis:
    # for full color vector
    bv0 = bootstrap_BIC(X0,N_fit_max,N_runs,False,N_init,use_pygmmis)
    mn0,st0 = [f(bv0,axis=0) for f in [np.nanmean,np.nanstd]]
  else:
    # fill w/ old values
    mn0,st0 = np.copy([mn1,st1])
  np.save(fname_out,[mn0,st0,mn1,st1])


def plot_BIC_data(fname=fname_BIC_data,N_sigma=3,printing=False,log=True):
  mn0,st0,mn1,st1 = np.load(fname)
  if printing: # initial analysis
    N_sig_min = (mn0-min(mn0)) / np.sqrt(st0**2 + st0[mn0==min(mn0)]**2)
    print("sigma from minimum:",np.round(N_sig_min,3))
    N_sig_nxt = np.diff(mn0) / np.sqrt(st0[:-1]**2 + st0[1:]**2)
    print("sigma from adjacent:",np.round(N_sig_nxt,3))
  Nfmx = len(mn0)
  fig,axs = plt.subplots(2, 1, sharex=True)
  xvals = np.arange(Nfmx)+1
  axs[0].errorbar(xvals,mn0-min(mn0),st0,fmt='o',capsize=5)
  axs[0].set_ylabel(r'$\Delta$BIC$(\vec c_0)$')
  axs[1].errorbar(xvals,mn1-min(mn1),st1,fmt='o',capsize=5)
  axs[1].set_ylabel(r'$\Delta$BIC$(\vec c_1)$')
  if log:
    for ii in range(2):
      # freeze ylim on log scale before adding fill_between
      axs[ii].set_yscale('log')
      mn = [mn0,mn1][ii] - min([mn0,mn1][ii])
      mn_min = min(mn[mn>0])
      st = mn - [st0,st1][ii]
      st_min = min(st[st>0])
      ylim_min = min(mn_min,st_min) * (max(mn)/mn_min)**(-1/10)
      ylim_ii = axs[ii].set_ylim(axs[ii].set_ylim(ylim_min,None)) 
  xlim = axs[1].set_xlim(axs[1].get_xlim())
  # axs[1].set_xscale('log')
  axs[1].set_xlabel('$K$') # r'$N_{\rm components}$')
  axs[1].set_xticks(xvals)
  # plot errorbars
  for ii in range(N_sigma):
    width = ones * (ii + 1) # number of sigma multiplier
    axs[0].fill_between(xlim,-st0[mn0==min(mn0)]*width,
                             +st0[mn0==min(mn0)]*width,
                        alpha=.125,lw=0,color='k')
    axs[1].fill_between(xlim,-st1[mn1==min(mn1)]*width,
                             +st1[mn1==min(mn1)]*width,
                        alpha=.125,lw=0,color='k')
  # save and display
  fname_out = "BIC_N_components.pdf"
  plt.savefig(fname_out)
  print("Saved",fname_out)
  plt.show()


def plot_BIC_comparative(fname=fname_BIC_data,log=True,N_max=12,which=0,N_sig=3):
  # load and analyze data
  data = mn0,st0,mn1,st1 = np.load(fname)
  if N_max is None:
    N_max = len(mn0) # total number of components tested
  else: 
    # vet down
    mn0,st0,mn1,st1 = [v[:N_max] for v in [mn0,st0,mn1,st1]]
  MN,ST = [mn0,mn1][which], [st0,st1][which]
  N_sig_min = (MN-min(MN)) / np.sqrt(ST**2 + ST[MN==min(MN)]**2) # sigma from minimum
  N_sig_nxt = - np.diff(MN) / np.sqrt(ST[:-1]**2 + ST[1:]**2) # sigma from adjacent
  if 1:
    print("Significance from minimum:",np.round(N_sig_min,2))
    print("Significance from adjacent:",np.around(N_sig_nxt,2))
  xvals = 1 + np.arange(N_max) # array of component count
  x_mid = (xvals[:-1] + xvals[1:])/2. # midpoints
  x_lim = xvals[0] - .5, xvals[-1] + .5 # x-limits on graph
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # plot BIC offset
  fig,axs = plt.subplots(3, 1, sharex=True) # TODO: consider taller plot
  axs[0].errorbar(xvals,MN-min(MN),ST,fmt='o',capsize=5)
  axs[0].set_ylabel(r'$\Delta {\rm BIC}$')
  if log:
    # freeze ylim on log scale before adding fill_between
    axs[0].set_yscale('log')
    mn = MN - min(MN)
    mn_min = min(mn[mn>0])
    st = mn - ST
    st_min = min(st[st>0])
    ylim_min = min(mn_min,st_min) * (max(mn)/mn_min)**(-1/10)
    ylim_0 = axs[0].set_ylim(ylim_min,None)
    # TODO: ensure >=1 ticks exist here!
    if 1:
      lgy = np.log10(ylim_0)
      mn,mx = 10**np.array([np.floor(lgy[0]),np.round(lgy[1],1)])
      ylim_0 = axs[0].set_ylim(mn,mx)
  # plot errorbars
  xlim = axs[0].set_xlim(.5,N_max+.5)
  for ii in range(N_sig):
    width = ones * (ii + 1) # number of sigma multiplier
    st_dev = ST[MN==min(MN)] * width # error on minimum point
    xlim = xlim[0]-1,xlim[1]+1
    axs[0].fill_between(xlim,-st_dev,+st_dev,alpha=.125*3/N_sig,lw=0,color='k')
  # sigma difference from minimum and adjacent 
  for ii,x,y,lbl,mkr in zip([1,2],[xvals,x_mid],
      [N_sig_min,N_sig_nxt],
      [r'$\sigma_{\rm to~min}$',r'$\sigma_\Delta$'],
      ['o',5]):
    axs[ii].scatter(x,y,marker=mkr)
    axs[ii].set_ylabel(lbl)
    ylim = axs[ii].set_ylim(axs[ii].get_ylim() + np.array([-.5,+.5]))
    # draw standard deviations
    for jj in range(N_sig):
      width = ones * (jj + 1) # number of sigma multiplier
      axs[ii].fill_between(xlim,-width,+width,alpha=.125*3/N_sig,lw=0,color='k')
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # tidy up lower axis
  # axs[1].set_xscale('log')
  axs[-1].set_xlabel('$K$') # r'$N_{\rm components}$')
  axs[-1].set_xticks(xvals)
  # save and display
  fname_out = "BIC_comparative.pdf"
  plt.savefig(fname_out)
  print("Saved",fname_out)
  plt.show()


########################################################################
# These are all flawed methods; one cannot compare BIC between datasets
# only between different models. 

fname_single = "output/BIC_single.npy"
def generate_color_BIC_single(N_runs=50):
  " test different colors & combinations out for 1 & 2 colors "
  # calculate BIC for each single band
  L,N_col = np.shape(X0)
  BIC_single_bands = np.zeros((N_runs,N_col))
  for jj in range(N_runs):
    mask = np.random.choice(L,L)
    for ii in range(N_col):
      color = np.transpose(X0[mask])[ii].reshape(-1,1)
      BIC_single_bands[jj,ii] = GM(2).fit(color).bic(color)
  mn,st = [f(BIC_single_bands,axis=0) for f in [np.mean,np.std]]
  np.save(fname_single,[mn,st])


def plot_color_BIC_single():
  " ERROR: BIC only works for identical data, different models "
  mn,st = np.load(fname_single)
  xv = np.arange(len(mn)) + 1
  # fxn = lambda x: np.log10(1 + x)
  plt.errorbar(xv,mn-min(mn),st,fmt='o',capsize=5)
  plt.ylabel(r"$\log_{10} (1 + \Delta{\rm BIC})$")
  plt.xticks(xv)
  plt.gca().set_xticklabels(lbls_col0,rotation=45)
  plt.show()


fname_double = "output/BIC_double.npy"
def generate_color_BIC_double():
  " calculate BIC for each shared band "
  L,N_col = np.shape(X0)
  BIC_double = np.zeros(N_col,N_col)
  # iterate over each possibility
  for ii in range(N_col):
    for jj in range(ii):
      X = X0[:,[ii,jj]]
      BIC_double[ii,jj] = GM(2).fit(X).bic(X)
  # save output
  pass


########################################################################
# color accuracy methods

def pygmmis_fit_predict(cols,covars,N_fit=2,mask_bad_sig=4,skl_init=0):
  """
  identical functionality to 
  `pred = GM(2).fit_predict(color)`
  but with errors included through pygmmis
  > to be used in `generate_color_acc`, `generate_3color_acc`, and
               in `generate_color_4_10`.
  """
  # read data of input colors
  N_gals,N_cols = np.shape(cols)
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # check condition number for singular matrices
  cond = np.linalg.cond(covars)
  assert(np.all(cond < MAX))
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # throw out outliers 
  if mask_bad_sig > 0:
    sig = (np.array(cols) - np.mean(cols,axis=0))/np.std(cols,axis=0)
    tot = np.sqrt(np.sum(sig**2,axis=1))
    mask = tot < mask_bad_sig
  else:
    mask = np.ones(N_gals).astype(bool) # use all 
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # set up GMM
  gmm = pygmmis.GMM(K=N_fit, D=N_cols) # set up Gaussian mixture model
  if skl_init:
    # get initial guess from sklearn
    gmm_skl = GM(N_fit)
    fit = gmm_skl.fit(cols[mask])
    # pull out sklearn fit parameters
    gmm.amp = fit.weights_
    gmm.mean = fit.means_
    gmm.covar = fit.covariances_
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # set up GMM
    logL,U = pygmmis.fit(gmm,cols[mask],covars[mask])
  else:
    logL,U = pygmmis.fit(gmm,cols[mask],covars[mask],init_method='kmeans')
  # grab probabilities
  Ps = [gmm.logL_k(ii,cols,covars) for ii in range(N_fit)]
  pred = np.argmax(Ps,axis=0)
  return pred


fname_color_acc = 'output/color_acc_runs.npy'
def generate_color_acc(X=X0,X_cov=X0_covar,N_runs=50,printing=True,plotting=[0,0]):
  " save data for 6x1 and 6x6 plot of accuracies "
  L,N_col = np.shape(X)
  bACC = np.zeros((N_runs,N_col,N_col))
  for kk in range(N_runs):
    mask = np.random.choice(L,L) # bootstrap sample
    for ii in range(N_col):
      # set up main diagonal
      if printing: print(kk,'>',ii)
      color = np.transpose(X[mask])[ii].reshape(-1,1)
      if X_cov is not None:
        covars = get_submatrix(X_cov[mask],ii)
        pred = pygmmis_fit_predict(color,covars)
      else:
        pred = GM(2).fit_predict(color)
      bacc_ii = C(T[mask],pred).get_BA()
      bACC[kk,ii,ii] = max(bacc_ii,1-bacc_ii)
      if printing: print('\t',np.round(bACC[kk,ii,ii],2))
      if plotting[0]:
        bins = np.linspace(np.min(color),np.max(color),100)
        plt.hist(color[pred==0],bins,histtype='step')
        plt.hist(color[pred==1],bins,histtype='step')
        plt.title(bACC[kk,ii,ii])
        plt.show()
      # fill in lower left triangle of matrix
      for jj in range(ii):
        if printing: print(kk,ii,jj)
        color = np.transpose(np.transpose(X[mask])[[ii,jj]])
        if X_cov is not None:
          covars = get_submatrix(X_cov[mask],[ii,jj])
          pred = pygmmis_fit_predict(color,covars)
        else:
          print(lin_indep)
          pred = GM(2).fit_predict(color)
        bacc_ii = C(T[mask],pred).get_BA()
        bACC[kk,ii,jj] = max(bacc_ii,1-bacc_ii)
        if printing: print('\t',np.round(bACC[kk,ii,jj],2))
        if plotting[1]:
          plt.subplot(121)
          plt.scatter(color[:,0],color[:,1],1,lw=0,c=pred)
          plt.title(bACC[kk,ii,jj])
          plt.subplot(122)
          plt.scatter(color[:,0],color[:,1],1,lw=0,c=T)
          plt.show()
  np.save(fname_color_acc,bACC)
  print(fname_color_acc,"saved!")


fname_3color_acc = 'output/color3_acc_runs.npy'
def generate_3color_acc(X=X0,X_cov=X0_covar,N_runs=50,printing=True):
  L,N_col = np.shape(X)
  bACC = np.zeros((N_runs,N_col,N_col,N_col))
  ba_max = 0
  for ll in range(N_runs):
    mask = np.random.choice(L,L) # bootstrap step
    for ii in range(N_col):
      for jj in range(ii):
        for kk in range(jj):
          if printing: print(ll,ii,jj,kk)
          color = np.transpose(np.transpose(X[mask])[[ii,jj,kk]]) 
          if X_cov is not None:
            lin_indep = check_lin_indep([ii,jj,kk])
            if not lin_indep: # only need to skip in pygmmis case.
              print("Not linearly independent. Skipping...")
              continue
            covars = get_submatrix(X_cov[mask],[ii,jj,kk])
            pred = pygmmis_fit_predict(color,covars)
          else:
            pred = GM(2).fit_predict(color)
          ba = C(T[mask],pred).get_BA()
          ba = max(ba,1-ba)
          bACC[ll,ii,jj,kk] = ba
          if ba > ba_max:
            print(np.round(100*ba,3))
            ba_max = ba
  np.save(fname_3color_acc,bACC)
  print(fname_3color_acc,"saved!")


def plot_color_acc():
  # read in data from generate_color_BIC_simple
  # to make a 6x1 and 6x6 plot
  bACC_runs = np.load(fname_color_acc)
  N_runs, N_col, _ = np.shape(bACC_runs)
  bACC_mean,bACC_std = [f(bACC_runs,axis=0) for f in [np.mean,np.std]]
  bACC = bACC_mean
  xv = np.arange(len(bACC))
  # set up axes
  col_min = min(bACC[bACC>0]) - .01
  # alpha = np.tril(np.ones((N_col,N_col)),-1) # would only work in future mpl
  plt.imshow(bACC,cmap='terrain_r',vmin=col_min)
  # set up axes labels with colors
  ax = plt.gca()
  ax.set_xticks(xv)
  ax.set_xticklabels(lbls_col0,rotation=45)
  ax.set_yticks(xv)
  ax.set_yticklabels(lbls_col0)
  # turn off grid edges
  for d in ["left", "top", "bottom", "right"]:
    ax.spines[d].set_visible(False)
  plt.colorbar(label=fit_CCM.lbl_BA) # ,vmin=col_min)
  fname_out = "color_acc_log.pdf"
  plt.savefig(fname_out)
  print("Saved",fname_out)
  plt.show()


def plot_color_acc2():
  # read in data
  bACC_runs = np.load(fname_color_acc)
  N_runs, N_col, _ = np.shape(bACC_runs)
  bACC_mean,bACC_std = [f(bACC_runs,axis=0) for f in [np.mean,np.std]]
  # plot main diagonal
  fig,axs = plt.subplots(2, 1, figsize=double_height)
  sort0 = np.flip(np.argsort(np.diag(bACC_mean)))
  xr = np.arange(N_col)
  axs[0].errorbar(xr,np.diag(bACC_mean)[sort0],np.diag(bACC_std)[sort0],fmt='o')
  if 0: # use median
    med = np.median(bACC_runs,axis=0)
    lo,hi = np.quantile(bACC_runs,[0.0027, 0.9973],axis=0) # 3 sig
    # 2 sig [.046,.954]
    # 1 sig [.317,.683]
    mdv = np.diag(med)[sort0] # sorted median values
    mdv_sig = (mdv-np.diag(lo)[sort0],np.diag(hi)[sort0]-mdv) # size of spread
    axs[0].errorbar(xr,mdv,mdv_sig,fmt='o')
  axs[0].set_xticks(xr)
  axs[0].set_xticklabels(lbls_col0[sort0])
  # display 2-color combinations
  indices = np.tril_indices(N_col,-1)
  N = len(indices[0])
  rx = np.arange(N)
  sort1 = np.flip(np.argsort(bACC_mean[indices]))
  axs[1].errorbar(rx,bACC_mean[indices][sort1],bACC_std[indices][sort1],fmt='o')
  lbls = [lbls_col0[indices[0][sort1]][ii] + r' $\oplus$ ' + lbls_col0[indices[1][sort1]][ii] for ii in range(N)]
  axs[1].set_xticks(range(N))
  axs[1].set_xticklabels(lbls,rotation=90)
  plt.show()


def plot_color_acc3():
  # read in data
  bACC_runs = np.load(fname_color_acc)
  N_runs, N_col, _ = np.shape(bACC_runs)
  # calculate median and errorbars
  sig_vals = .5,[.317,.683],[.046,.954],[0.0027, 0.9973]
  med = np.quantile(bACC_runs,sig_vals[0],axis=0)
  lo1,hi1 = np.quantile(bACC_runs,sig_vals[1],axis=0)
  lo2,hi2 = np.quantile(bACC_runs,sig_vals[2],axis=0)
  lo3,hi3 = np.quantile(bACC_runs,sig_vals[3],axis=0)
  # set up y-variable, main column, and sorting 
  yv = np.arange(N_col)
  mv = np.diag(med)
  sort = np.argsort(mv)
  # plot
  plt.errorbar(mv[sort],yv,xerr=((mv-np.diag(lo1))[sort],
                                 (np.diag(hi1)-mv)[sort]),
               capsize=16,fmt='o',color='k')
  plt.errorbar(mv[sort],yv,xerr=((mv-np.diag(lo2))[sort],
                                 (np.diag(hi2)-mv)[sort]),
               capsize=6.35,fmt='o',color='k')
  plt.errorbar(mv[sort],yv,xerr=((mv-np.diag(lo3))[sort],
                                 (np.diag(hi3)-mv)[sort]),
               capsize=1.01,fmt='o',color='k')
  # set up axes
  plt.xlabel(fit_CCM.lbl_BA)
  plt.yticks(yv)
  plt.gca().set_yticklabels(lbls_col0[sort])
  plt.show()


def plot_violin_single(sigma=True):
  " plot balanced accuracy of main colors in violin plot "
  # read in data
  bACC_runs = np.load(fname_color_acc)
  N_runs, N_col, _ = np.shape(bACC_runs)
  # run violin plot
  diag_runs = np.diagonal(bACC_runs,0,1,2) # 0 offset, axes = 1 & 2
  med = np.median(diag_runs,axis=0)
  sort = np.argsort(med)
  yv = np.arange(N_col)
  plt.violinplot(diag_runs[:,sort],yv,False,showmeans=True,showextrema=False)
  if sigma:
    lo,hi = np.quantile(diag_runs,[1-s1,s1],axis=0) # pm 1 sigma
    err = np.array([med - lo, hi - med]) # error off of median
    plt.errorbar(med[sort],yv,xerr=err[:,sort],fmt='.')
  # set up axes
  plt.xlabel(fit_CCM.lbl_BA)
  plt.yticks(yv)
  plt.gca().set_yticklabels(lbls_col0[sort])
  # save figure and display
  fname_out = 'violin_1.pdf'
  plt.savefig(fname_out)
  print("Saved",fname_out)
  plt.show()


def plot_violin_double(sigma=True):
  " violin plot of bACC for color combinations "
  # read in data
  bACC_runs = np.load(fname_color_acc)
  N_runs, N_col, _ = np.shape(bACC_runs)
  indices = np.tril_indices(N_col,-1)
  N = int(N_col * (N_col - 1) / 2)
  assert(len(indices[0])==N)
  yv = np.arange(N)
  # sort data
  tril_runs = bACC_runs[:,indices[0],indices[1]]
  med = np.median(tril_runs,axis=0)
  sort = np.argsort(med)
  lbls = [lbls_col0[indices[0][sort]][ii] + r' $\oplus$ ' + \
          lbls_col0[indices[1][sort]][ii] for ii in range(N)]
  # plot output
  plt.figure(figsize=double_height)
  plt.violinplot(tril_runs[:,sort],yv,False,showmeans=True,showextrema=False)
  if sigma:
    lo,hi = np.quantile(tril_runs,[1-s1,s1],axis=0) # pm 1 sigma
    err = np.array([med - lo, hi - med]) # error off of median
    plt.errorbar(med[sort],yv,xerr=err[:,sort],fmt='.')
  plt.xlabel(fit_CCM.lbl_BA)
  plt.xlim(.7,.95)
  plt.yticks(yv)
  plt.ylim(yv[0]-.5,yv[-1]+.5)
  plt.gca().set_yticklabels(lbls)
  # save figure and display
  fname_out = 'violin_2.pdf'
  plt.savefig(fname_out)
  print("Saved",fname_out)
  plt.show()


def plot_cf_ncomp():
  # read in data
  bACC_runs = np.load(fname_color_acc)
  N_runs, N_col, _ = np.shape(bACC_runs)
  c1 = bACC_runs[:,1,1] # g-r
  c2 = bACC_runs[:,4,2] # u-r, r-i (or r-z, -1)
  lbls = [lbls_col0[1],lbls_col0[4] + r' $\oplus$ ' + lbls_col0[2]]
  # print some data
  for ii,c in enumerate([c1,c2]):
    mn,st = [np.round(100*f(c),3) for f in [np.mean,np.std]]
    print(lbls[ii],mn,'+/-',st)
  # plot accuracies
  yv = np.arange(2)
  plt.violinplot([c1,c2],yv,False,showmeans=True,showextrema=False)
  plt.yticks(yv)
  plt.gca().set_yticklabels(lbls)
  # save figure and display
  fname_out = 'cf__nComp.pdf'
  plt.savefig(fname_out)
  print("Saved",fname_out)
  plt.show()


def plot_col_hist(idx=2,exclude=3):
  """
  double-check the color distribution and fitting for a given index
  default: idx=2 (r-i)
  """
  col = X0[:,idx]
  bins = np.linspace(sorted(col)[exclude+1],sorted(col)[-(exclude+1)],100)
  plt.hist(col[T],bins,log=True,color='r',alpha=.5)
  plt.hist(col[~T],bins,log=True,color='b',alpha=.5)
  plt.xlabel(lbls_col0[idx])
  plt.ylabel('Counts')
  plt.show()


def plot_violin_triple(sigma=True):
  bACC = np.load(fname_3color_acc)
  N_runs,N_col,_,_ = np.shape(bACC)
  # set up labels
  indexes,labels = [],[]
  for ii in range(N_col):
    for jj in range(ii):
      for kk in range(jj):
        labels += [lbls_col0[ii] + r' $\oplus$ ' + lbls_col0[jj] \
                                 + r' $\oplus$ ' + lbls_col0[kk]]
        indexes += [[ii,jj,kk]]
  labels = np.array(labels) # recast for easier sorting
  runs = np.array([bACC[:,idx[0],idx[1],idx[2]] for idx in indexes])
  N = len(runs)
  meds = np.median(runs,axis=1)
  sort = np.argsort(meds)
  # plot output
  plt.figure(figsize=double_height)
  xv = np.transpose(runs[sort])
  yv = np.arange(N)
  plt.violinplot(xv,yv,False,showmeans=True,showextrema=False)
  if sigma:
    lo,hi = np.quantile(runs,[1-s1,s1],axis=1) # pm 1 sigma
    err = np.array([meds - lo, hi - meds]) # error off of median
    plt.errorbar(meds[sort],yv,xerr=err[:,sort],fmt='.')
  plt.xlabel(fit_CCM.lbl_BA)
  plt.yticks(yv)
  plt.ylim(yv[0]-.5,yv[-1]+.5)
  plt.gca().set_yticklabels(labels[sort])
  # save figure and display
  fname_out = 'violin_3.pdf'
  plt.savefig(fname_out)
  print("Saved",fname_out)
  plt.show()


def plot_jumps(band_counts=False):
  bACC = np.load(fname_3color_acc)
  N_runs,N_col,_,_ = np.shape(bACC)
  # set up labels
  indexes,labels,jumps = [],[],[]
  band_counts = []
  for ii in range(N_col):
    for jj in range(ii):
      for kk in range(jj):
        labels += [lbls_col0[ii] + r' $\oplus$ ' + lbls_col0[jj] \
                                 + r' $\oplus$ ' + lbls_col0[kk]]
        indexes += [[ii,jj,kk]]
        jumps += [band_jumps[ii] + band_jumps[jj] + band_jumps[kk]]
        band_counts += [np.sum([bands_incl[idx] for idx in [ii,jj,kk]],axis=0)]
  runs = np.array([bACC[:,idx[0],idx[1],idx[2]] for idx in indexes])
  N = len(runs)
  meds = np.median(runs,axis=1)
  stds = np.std(runs,axis=1)
  # plot output
  if band_counts:
    # look at band counts
    for ii in range(5):
      x_ii = np.array(band_counts)[:,ii]+(ii-2)*.1
      # plt.errorbar(x_ii,meds,stds,label=ii,fmt='.')
      plt.scatter(x_ii,meds,10,label=lbls_bands[ii])
    plt.legend()
    plt.xlabel("band count")
    plt.ylabel(fit_CCM.lbl_BA)
    plt.show()
  if 1: 
    shift = (np.random.random(len(jumps)) - .5)/10.
    # plt.errorbar(jumps+shift,meds,stds,fmt='.')
    plt.scatter(jumps+shift,meds,10,alpha=.5)
    plt.xlabel("total magnitude jump length")
    plt.ylabel(fit_CCM.lbl_BA)
    plt.show()


def check_lin_indep(indices):
  " check various colors for linear independence (input color indices) "
  import sympy
  M = [bands_matr[idx] for idx in indices]
  _, indexes = sympy.Matrix(M).T.rref()
  return len(indexes) == len(indices) # true if independent


def plot_image2(plot_scores=True,color='sig_rel'):
  " use imshow to display which bands are used where "
  # read in data
  bACC = np.load(fname_color_acc)
  N_runs, N_col, _ = np.shape(bACC)
  N_dim = 2 # dimensionality of color space
  indices = np.tril_indices(N_col,-1)
  N = int(N_col * (N_col - 1) / 2)
  # set up labels
  indexes,indexes_used,lin_indep = [],[],[]
  for ii in range(N_col):
    for jj in range(ii):
      indexes += [[ii,jj]]
      # count which bands were used
      curr = np.zeros(10)
      for idx in [ii,jj]:
        curr[idx] = 1
      indexes_used += [curr]
      # check whether bands are linearly independent
      lin_indep += [check_lin_indep([ii,jj])]
  # convert to numpy arrays
  indexes_used = np.array(indexes_used)
  lin_indep = np.array(lin_indep)
  # sort out each of the runs
  runs = np.array([bACC[:,idx[0],idx[1]] for idx in indexes])
  N = len(runs)
  meds = np.median(runs,axis=1)
  sv = [1-one_sigma,one_sigma]
  sigs = np.diff(np.quantile(runs,sv,axis=1),axis=0).squeeze()
  sort = np.flip(np.argsort(meds)) # sort best scoring to worst
  # plot image of used bands, sorted 
  img = np.transpose(indexes_used[sort])
  if plot_scores:
    # score each color by how often it's used with the best models
    running_score = np.cumsum(img,axis=1)
    score_eff = np.sum(running_score,axis=1) # effective score of each color
    N_avg = np.sum(np.cumsum(N_dim/N_col*np.ones(N)))
    total_score = score_eff/N_avg # where 1 is random / average
    sort_N = np.flip(np.argsort(total_score)) # highest score first
    # plot results
    _=plt.errorbar(range(10),score_eff[sort_N]/N_avg,np.sqrt(score_eff[sort_N])/N_avg,fmt='o')
    plt.xticks(range(10))
    plt.gca().set_xticklabels(lbls_col0[sort_N])
    xlim = plt.xlim(plt.xlim())
    plt.plot(xlim,ones,'k')
    plt.ylabel('Arbitrary Score')
    plt.show()
  else:
    sort_N = range(N_col)
  if color=='lin_indep':
    img[:,~lin_indep[sort]] *= -1
    plt.imshow(img,cmap='bwr_r',vmin=-1)
    plt.yticks(range(N_col))
    plt.gca().set_yticklabels(lbls_col0)
  elif color=='sig_rel':
    plt.figure(figsize=double_width*.75)
    rel = (meds[sort][0] - meds[sort]) / sigs[sort][0]
    nanimg = np.copy(img)
    nanimg[img==0] = None
    img = nanimg * rel # np.log10(1 + rel)
    cmap = 'tab20b' # 'nipy_spectral'
    # masked_array = np.ma.array(img, mask=np.isnan(img))
    im = plt.imshow(img[sort_N],cmap=cmap,vmin=-1) # ,norm=matplotlib.colors.LogNorm())
    plt.yticks(range(N_col))
    ax = plt.gca()
    ax.set_yticklabels(lbls_col0[sort_N])
    # set up colorbar
    divider = make_axes_locatable(ax)
    cax = divider.append_axes('right', size='2.25%', pad=0.05)
    lbl = r'$\Delta\,$bACC / $\sigma_0$'
    plt.colorbar(im, cax=cax, orientation='vertical', label=lbl)
  fname_out = 'tGM_img2.pdf'
  plt.savefig(fname_out)
  print("Saved",fname_out)
  plt.show()


def plot_image3(plot_score=True,color='sig_rel',vert_break=True):
  " use imshow to display which bands are used where "
  # sorted by accuracy
  bACC = np.load(fname_3color_acc)
  N_runs,N_col,_,_ = np.shape(bACC)
  # set up labels
  indexes,indexes_used,lin_indep = [],[],[]
  for ii in range(N_col):
    for jj in range(ii):
      for kk in range(jj):
        indexes += [[ii,jj,kk]]
        # count which bands were used
        curr = np.zeros(10)
        for idx in [ii,jj,kk]:
          curr[idx] = 1
        indexes_used += [curr]
        # check whether bands are linearly independent
        lin_indep += [check_lin_indep([ii,jj,kk])]
  # convert to numpy arrays
  indexes_used = np.array(indexes_used)
  lin_indep = np.array(lin_indep)
  # sort out each of the runs
  runs = np.array([bACC[:,idx[0],idx[1],idx[2]] for idx in indexes])
  N = len(runs)
  meds = np.median(runs,axis=1)
  sv = [1-one_sigma,one_sigma]
  sigs = np.diff(np.quantile(runs,sv,axis=1),axis=0).squeeze()
  sort = np.flip(np.argsort(meds)) # sort best scoring to worst
  # plot image of used bands, sorted 
  img = np.transpose(indexes_used[sort])
  if plot_score:
    # score each color by how often it's used with the best models
    running_score = np.cumsum(img,axis=1)
    N = np.sum(running_score,axis=1) # effective score of each color
    N_avg = np.sum(np.cumsum(3/10*np.ones(120)))
    total_score = N/N_avg # where 1 is random / average
    sort_N = np.flip(np.argsort(total_score)) # highest score first
    # plot results
    _=plt.errorbar(range(10),N[sort_N]/N_avg,np.sqrt(N[sort_N])/N_avg,fmt='o')
    plt.xticks(range(10))
    plt.gca().set_xticklabels(lbls_col0[sort_N])
    plt.ylabel('Arbitrary Score')
    plt.show()
  else:
    sort_N = range(N_col)
    sort_N = [0,4,5,6,1,7,8,2,9,3]
  if color=='lin_indep':
    img[:,~lin_indep[sort]] *= -1
    cmap = 'bwr_r'
  elif color=='sig_rel':
    rel = (meds[sort][0] - meds[sort]) / sigs[sort][0]
    nanimg = np.copy(img)
    nanimg[img==0] = None
    img = nanimg * rel # np.log10(1 + rel)
    cmap = ['tab10','tab20b'] # 'nipy_spectral'] # 'Spectral_r'] # 'tab20b']
    cmap = ['Paired','tab20b']
  if vert_break:
    size = double_width * np.array([.7,1])
    fig,axs = plt.subplots(2,1,sharey=True,figsize=size)
    for ii in [0,1]:
      ax = axs[ii]
      N_per_row = 60
      N_per_row = 55
      # TODO: figure out more robust coloring. 
      vlim = [[0,5],[0,50]][ii]
      vlim = [[0,12],[15,35]][ii]
      # sort_N = np.argsort(np.nanmedian(img,axis=1))
      im = ax.imshow(img[sort_N],cmap=cmap[ii],vmin=vlim[0],vmax=vlim[1])
      xlim = np.array([-.5,-.5+N_per_row]) + N_per_row * ii
      ax.set_xlim(xlim)
      ax.set_yticks(range(N_col))
      ax.set_yticklabels(lbls_col0[sort_N])
      # set up colorbar
      divider = make_axes_locatable(ax)
      cax = divider.append_axes('right', size='1.5%', pad=0.05)
      lbl = r'$\Delta\,$bACC / $\sigma_0$'
      fig.colorbar(im, cax=cax, orientation='vertical', label=lbl, extend='max')
  else:
    plt.imshow(img,cmap=cmap)
    plt.yticks(range(N_col))
    plt.gca().set_yticklabels(lbls_col0)
  fname_out = 'tGM_img3.pdf'
  plt.savefig(fname_out)
  print("Saved",fname_out)
  plt.show()


fname_4_10 = 'output/color_4_10.npy'
def generate_color_4_10(N_runs=50,printing=True,use_errors=True):
  " get estimates on primary vs full color accuracy "
  bACC = np.zeros((N_runs,2))
  for ii in range(N_runs):
    if printing: print(ii+1)
    mask = np.random.choice(L,L) # bootstrap sample
    for jj in range(2):
      X = [X1,X0][jj]
      if use_errors and jj==0: # covariances singular for c0
        S = [X1_covar,X0_covar][jj]
        pred = pygmmis_fit_predict(X[mask],S[mask])
      else:
        pred = GM(2).fit_predict(X[mask])
      ba = C(T[mask],pred).get_BA()
      bACC[ii,jj] = max(ba,1-ba)
  np.save(fname_4_10,bACC)
  print("Saved",fname_4_10)


def plot_NC_bACC(lbl_Ncol=False,violin=False,sigma=s2):
  # plot best fits for 1,2,3,4,10 colors
  xv = [1,2,3,4,5]
  if lbl_Ncol:
    x_lbls = [1,2,3,4,10]
  else: # label w/ relevant colors
    # WARNING: these hardcoded labels may not be the real ones!
    # Make sure to update after running their respective sources
    lbl3 = r'$u-r$' '\n' r'$g-r$' '\n' r'$r-z$'
    lbl4 = r'$u-g$' '\n' r'$g-r$' '\n' r'$r-i$' '\n' r'$i-z$'
    x_lbls = ['$g-r$',r'$u-i$' '\n' r'$u-r$', lbl3, lbl4, r'sklearn']
  yv = np.zeros(len(xv))
  lo,hi = np.zeros((2,len(xv)))
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # load in data for single and double colors
  bACC_runs = np.load(fname_color_acc)
  N_runs, N_col, _ = np.shape(bACC_runs)
  lo_,med_,hi_ = np.quantile(bACC_runs,[1-sigma,.5,sigma],axis=0) # full matrix
  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  
  # handle single colors
  ii = idx_max_one = np.argsort(np.diag(med_))[-1]
  if violin:
    vp = plt.violinplot([bACC_runs[:,ii,ii]],[xv[0]],showmeans=True,showextrema=False)
    for pc in vp['bodies']: # fix coloring
      pc.set_facecolor(mpl_colors[0])
  lo[0],yv[0],hi[0] = [v[ii,ii] for v in [lo_,med_,hi_]]
  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  
  # handle double colors
  indices = np.tril_indices(N_col,-1)
  tril_runs = bACC_runs[:,indices[0],indices[1]]
  med = np.median(tril_runs,axis=0)
  ii = idx_max_two = np.argsort(med)[-1]
  if violin:
    vp = plt.violinplot([tril_runs[:,ii]],[xv[1]],showmeans=True,showextrema=False)
    for pc in vp['bodies']: # fix coloring
      pc.set_facecolor(mpl_colors[0])
    vp['cmeans'].set_color(mpl_colors[0]) # fix coloring of lines
  lo[1],yv[1],hi[1] = [v[indices[0][ii],indices[1][ii]] for v in [lo_,med_,hi_]]
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # load in data for three colors
  bACC = np.load(fname_3color_acc)
  N_runs,N_col,_,_ = np.shape(bACC)
  lo_,med_,hi_ = np.quantile(bACC,[1-sigma,.5,sigma],axis=0) # full matrix
  flat = med_.reshape(N_col**3)
  ii = idx_max_three = np.argsort(flat)[-1]
  lo[2],yv[2],hi[2] = [v.reshape(N_col**3)[ii] for v in [lo_,med_,hi_]]
  i,j,k = map(int,str(ii))
  if violin:
    vp = plt.violinplot([bACC[:,i,j,k]],[xv[2]],showmeans=True,showextrema=False)
    for pc in vp['bodies']: # fix coloring
      pc.set_facecolor(mpl_colors[0])
    vp['cmeans'].set_color(mpl_colors[0]) # fix coloring of lines
  if 0: # freeze lower bound for vertical scale
    ylim = plt.ylim()
    plt.ylim(ylim[0],None)
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # load in data for four and ten colors
  four_ten = np.load(fname_4_10)
  if violin:
    vp = plt.violinplot(four_ten,xv[-2:],showmeans=True,showextrema=False)
    for pc in vp['bodies']: # fix coloring of transparencies
      pc.set_facecolor(mpl_colors[0])
    vp['cmeans'].set_color(mpl_colors[0]) # fix coloring of lines
  yv[-2:] = np.median(four_ten,axis=0)
  lo[-2:],hi[-2:] = np.quantile(four_ten,[1-sigma,sigma],axis=0) # pm 1 sigma
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # plot total errorbar
  plt.errorbar(xv,yv,(yv-lo,hi-yv),fmt='o',color=mpl_colors[1],capsize=5)
  if 1: 
    # print diagnostics: how much bACC gained per jump?
    std = np.mean([yv-lo,hi-yv],axis=0)
    std_diff = np.sqrt(std[:-1]**2 + std[1:]**2)
    print("%bACC diff:",np.round(100*np.diff(yv),3))
    print("sigma diff:",np.round(np.diff(yv)/std_diff,3))
  if lbl_Ncol:
    plt.xlabel(r'$N_{\rm colors}$')
  plt.xticks(xv)
  plt.gca().set_xticklabels(x_lbls)
  plt.ylabel(fit_CCM.lbl_BA)
  if 1: # hardcode bounds
    plt.xlim(.8,4.2)
    plt.ylim(.86,.95)
  # save and display
  fname_out = "NC_bACC.pdf"
  plt.savefig(fname_out)
  print("Saved",fname_out)
  plt.show()


def plot_LT_bACC():
  """
  plot lower triangle (LT) of bACC for 2D color combinations 
  """
  data = np.load(fname_color_acc)
  data[data<.5] = np.nan
  v = np.arange(data.shape[-1])
  plt.imshow(np.mean(data,axis=0),vmin=.6,cmap='nipy_spectral_r')
  ax = plt.gca()
  [spine.set_visible(False) for spine in ax.spines.values()]
  # set x-tick labels
  plt.xticks(v)
  ax.set_xticklabels(lbls_col0,rotation=45)
  # set y-tick labels
  plt.yticks(v)
  ax.set_yticklabels(lbls_col0)
  # tidy up, save, and display
  plt.colorbar(label='Balanced Accuracy')
  fname_out = 'LT_bACC.pdf'
  plt.savefig(fname_out)
  print("Saved",fname_out)
  plt.show()


########################################################################

if __name__ == '__main__':
  import warnings
  warnings.filterwarnings("ignore")
  if 0:
    # test fitting accuracy between X0 and X1
    test_fitting_accuracy()
  if 0: 
    # plot results of above
    plot_fitting_accuracy()
  if 0: # test BIC
    if 0: # generate data
      if 1: # proper run
        Nfmx = 15 # maximum number of components
        Nr = 50 # number of bootstrap runs
      else: # testing
        Nfmx = 10 # maximum number of components
        Nr = 3 # number of bootstrap runs
      generate_BIC_data(Nfmx,Nr,fname_BIC_data,use_pygmmis=True)
    # plot_BIC_data(fname_BIC_data)
    # test out color BIC
    # plot_color_BIC_single()
    plot_BIC_comparative(N_max=7,which=1,N_sig=3)
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # 1 & 2d color selections
  # generate_color_acc(X0,100)
  # plot_color_acc()
  # plot_color_acc2()
  # plot_color_acc3()
  # # # # # # # # # # # # 
  # plot_violin_single()
  # plot_violin_double()
  # plot_image2()
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # triad color selections
  if 0:
    # generate_3color_acc(X0,N_runs=50)
    # plot_violin_triple()
    # plot_jumps()
    plot_image3()
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # joint plots
  # generate_color_4_10(100)
  plot_NC_bACC()
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # other
  # plot_col_hist()
  # plot_BIC_comparative() # log=False)
  # plot_LT_bACC()
  print('~fin')
