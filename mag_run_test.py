# quick test of magnitude running

import sklearn
import warnings # to shut up sklearn
warnings.filterwarnings("ignore",category=DeprecationWarning)

exec(open("mpl_tools.py").read()) # import matplotlib tools and defaults
exec(open("kllr_tools.py").read()) # import KLLR and tools to aid it

import rd_gamma
import pygmmis
import h5py

from test_GM_similarity import pygmmis_fit_predict # cols,covars,N_fit
from m_star_model import m_star as R14

import os
os.environ['OPENBLAS_NUM_THREADS'] = '1'
os.environ['RLIMIT_NPROC'] = '6'
"""
OpenBLAS blas_thread_init: pthread_create failed for thread 1 of 64: Resource temporarily unavailable
OpenBLAS blas_thread_init: RLIMIT_NPROC 2048 current, 8192 max
"""

########################################################################
# fitting functions

def fit_kllr(bands,bands_err,xlim=None,N_fit=2,N_bins=100,factor=0):
  " split w/ Gaussian mixture, then fit with KLLR "
  # fit with pygmmis for components
  print("selecting via pygmmis")
  g,r,i,z = np.transpose(bands)
  cols = -np.diff(bands,axis=1)
  covars = rd_gamma._band_err_to_covar(bands_err)
  pred = pygmmis_fit_predict(cols,covars,N_fit)
  # re-sort
  medians = [np.median(cols[pred==jj],axis=0) for jj in range(N_fit)]
  M = np.linalg.norm(medians,axis=1)
  S = np.sqrt(np.sum([np.std(cols[pred==jj],axis=0)**2 for jj in range(N_fit)],axis=1))
  sort = np.flip(np.argsort(M + factor * S))
  # for each component, measure the KLLR fit
  kw = (max(i) - min(i)) / 10.
  if xlim is None:
    xlim = np.quantile(i,[0,1]) # min and max
  lm = kllr_model(kernel_width=kw)
  report = np.zeros((N_fit,5,N_bins)) # xr, yr, sig, N, w
  print(f"running KLLR fits")
  for ii in range(N_fit):
    mask = (pred == sort[ii])
    # TODO? run on all colors, not just gmr
    x,y = bands[mask,2],cols[mask,0]
    xr,yr,_,_,sig = lm.fit(x,y,xlim,nbins=N_bins,kernel_width=kw)
    N = get_counts(x,xr,kw)
    report[ii][0:-1] = [xr,yr,sig,N]
  N_tot = np.sum(report[:,-2],axis=0) # total pseudo-count per point
  report[:,-1] = report[:,-2] / N_tot
  return report


def fit_bins(bands,bands_err,bins,N_fit=2,factor=0):
  " use Delta m_i bins to separate data "
  # read in data
  bins = np.array(bins)
  g,r,i,z = np.transpose(bands)
  cols = -np.diff(bands)
  _,N_cols = np.shape(cols)
  covars = rd_gamma._band_err_to_covar(bands_err)
  # set up return variable
  bin_mids = (bins[1:] + bins[:-1])/2.
  N_bins = len(bins) - 1
  gmm = pygmmis.GMM(N_fit,N_cols)
  report = np.zeros((N_fit,5,N_bins))
  for ii in range(N_bins):
    mask_ii = (bins[ii] <= i) * (i < bins[ii+1])
    # fit with pygmmis
    x,y = cols[mask_ii],covars[mask_ii]
    assert(len(x)>0)
    logL,U = pygmmis.fit(gmm,x,y,init_method='kmeans')
    # read out gmm parameters
    w = gmm.amp
    mu = gmm.mean
    Sigma = gmm.covar
    var = np.diagonal(Sigma,axis1=-2,axis2=-1) # variances
    # get prediction for each point
    logLs = [gmm.logL_k(jj,x,y) for jj in range(N_fit)]
    pred = np.argmax(logLs,axis=0)
    # sort and count
    medians = [np.median(x[pred==jj],axis=0) for jj in range(N_fit)]
    M = np.linalg.norm(medians,axis=1)
    S = np.sqrt(np.sum(var,axis=-1))
    sort = np.flip(np.argsort(M + factor * S))
    a,b = [np.round(v[sort],3) for v in [M,S]]
    print(f"{ii+1}/{N_bins} : {a}, {b}")
    # save outputs
    for jj in range(N_fit):
      report[jj][0][ii] = bin_mids[ii]
      report[jj][1][ii] = mu[sort][jj][0] # return (g-r) values only at the moment
      report[jj][2][ii] = np.sqrt(var)[sort][jj][0] # "
      report[jj][3][ii] = len(pred[pred==sort[jj]])
      report[jj][4][ii] = w[sort][jj]
  return report


fname_out = 'output/par_mag_run.h5'
def fit_slice(bands,bands_err,N_fit=2,N_bootstrap=50,
              m_star=19,N_KLLR=100,factor=0,fname_save=fname_out):
  """
  fit a single redshift slice for magnitude running
  fit_slice(bands,bands_err,N_fit=2,N_bootstrap=50,
            m_star=19,N_KLLR=100,factor=0)
  """
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # set up return variables
  report_KLLR = np.zeros((N_bootstrap,N_fit,5,N_KLLR))
  report_bivar = np.zeros((N_bootstrap,N_fit,5,2)) # bimodal split
  report_deci = np.zeros((N_bootstrap,N_fit,5,10)) # decimated split
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # set up bounds and bins for data
  N_gal,N_bands = np.shape(bands)
  g,r,i,z = np.transpose(bands)
  xlim = np.quantile(i,[0,1])
  bins_bivar = xlim[0],m_star,xlim[-1] # split at m*
  if 1:
    n,bins_deci = np.histogram(i,np.quantile(i,np.linspace(0,1,11))) # bins, counts
  else:
    n,bins_deci = np.histogram(i,10) # bins, counts; even-space in magnitude
    assert(np.all(n>0)) # FIXME: softcode solution
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # run fits
  for ii in range(N_bootstrap):
    print("#"*72)
    print(f"Analyzing {ii+1}/{N_bootstrap}...")
    if ii == 0: # don't do bootstrap for first run
      x,y = bands,bands_err # standard selection
    else: # do bootstrap runs for the rest
      mask_ii = np.random.choice(N_gal,N_gal) # bootstrap selection
      x,y = bands[mask_ii],bands_err[mask_ii]
    print("# KLLR fit")
    report_KLLR[ii] = fit_kllr(x,y,xlim,N_fit,N_KLLR)
    print("# bivariate fit")
    report_bivar[ii] = fit_bins(x,y,bins_bivar,N_fit,factor)
    print("# decimated fit")
    report_deci[ii] = fit_bins(x,y,bins_deci,N_fit,factor)
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # save output
  print("Saving output...")
  with h5py.File(fname_out,'w') as ds:
    for tag,dat in zip(['KLLR','bivar','deci'],
                       [report_KLLR,report_bivar,report_deci]):
      ds.create_dataset(tag,data=dat)
    for tag,dat in zip(['N_fit','N_bootstrap','m_star','N_KLLR','factor'],
                       [ N_fit , N_bootstrap , m_star , N_KLLR , factor ]):
      ds.attrs[tag] = dat
    ds.attrs['N_bivar'] = len(np.array(bins_bivar)) - 1
    ds.attrs['N_deci'] = len(bins_deci) - 1


def plot_fit(fname=fname_out):
  # read in data
  with h5py.File(fname,'r') as ds:
    reports = [ds[tag][()] for tag in ['KLLR','bivar','deci']]
    N_fit,N_bootstrap,m_star,N_KLLR,factor = [ds.attrs[key] for key in \
                     ['N_fit','N_bootstrap','m_star','N_KLLR','factor']]
  # make comparative plots
  lbls = [r'$m_i$',r'$\langle g-r \rangle$',r'$\sigma_{(g-r)}$',r'$N$',r'$w$']
  fig,axs = plt.subplots(4,1,figsize=double_height,sharex=True)
  axs[-1].set_xlabel(lbls[0]) # set x-label of last subplot
  xlim = np.quantile(reports[0][:,:,0,:],[0,1])
  axs[-1].set_xlim(xlim)
  # iterate through all variables
  for hh,ii in enumerate([3,4,1,2]): # for each variable
    ax = axs[hh] # to be zero-indexed
    ax.set_ylabel(lbls[ii]) # set current y-axis
    if ii == 3: # set number counts to log
      ax.set_yscale('log')
    for jj in range(len(reports)): # for each dataset
      # reports has structure (N_methods,N_bootstrap,N_fit,N_par,N_x)
      x = reports[jj][0,0,0,:] # should be identical for all
      s = 100 / len(x) # set marker size
      for kk in range(N_fit): # for each GM component
        y = reports[jj][:,kk,ii,:]
        # plot bootstrap mean and error
        mn,st = [f(y,axis=0) for f in [np.mean,np.std]]
        if jj == 0: # KLLR analysis
          # plot default fit (no bootstrapping)
          ax.plot(x,y[0],s,color=mpl_colors[kk])
          # plot errorbars from bootstrapping
          ax.fill_between(x,mn-st,mn+st,alpha=.125,color=mpl_colors[kk],lw=0)
        else:
          # plot default fits (no bootstrapping)
          ax.scatter(x,y[0],s,color=mpl_colors[kk])
          # plot errorbars from bootstrapping
          ax.errorbar(x,mn,st,color=mpl_colors[kk],alpha=.5,fmt='o',markersize=0)
  plt.show()

fname_sig = 'output/sig.npy'
def calc_sig(fname=fname_out,fname_save=fname_sig):
  " plot significance of slope for each parameter, by each method "
  # read in data
  with h5py.File(fname,'r') as ds:
    # `reports` has structure (N_methods)(N_bootstrap,N_fit,N_par,N_x)
    reports = [ds[tag][()] for tag in ['KLLR','bivar','deci']]
    N_fit,N_bootstrap,m_star,N_KLLR,factor = [ds.attrs[key] for key in \
                     ['N_fit','N_bootstrap','m_star','N_KLLR','factor']]
  # set up report variable
  N_methods = len(reports)
  pars = [4,1,2] # weight, mean, scatter
  N_pars = len(pars)
  report = np.zeros((N_pars,N_methods,N_fit,N_bootstrap))
  # analyze fits
  for hh,ii in enumerate(pars): # weight, mean, scatter
    for jj in range(len(reports)): # for each calculation method
      x = reports[jj][0,0,0,:] - m_star # should be identical for all
      for kk in range(N_fit): # for each GM component
        for ll in range(N_bootstrap):
          y = reports[jj][ll,kk,ii,:]
          if len(x) > 2: 
            p,V = np.polyfit(x,y,1,cov=True)
            err = np.sqrt(np.diag(V)) # apx err on each parameter
            report[hh,jj,kk,ll] = (p/err)[0]
          else:
            report[hh,jj,kk,ll] = np.polyfit(x,y,1)[0]
  np.save(fname_save,report)
  print(f"Saved {fname_save}")


def plot_sig(fname=fname_sig):
  report = np.load(fname)
  N_pars,N_methods,N_fit,N_bootstrap = np.shape(report)
  for ii in range(N_pars):
    for jj in range(N_methods):
      for kk in range(N_fit):
        x_kk = ii + (jj + kk / (N_fit + 1)) / (N_methods + 1)
        col = mpl_colors[[3,0,1,4,5,6,7,8,9][kk]]
        # analyze bootstrap sample
        mn,st = [f(report[ii,jj,kk]) for f in [np.mean,np.std]]
        mkr = '^' if (mn > 0) else 'v'
        if jj == 1: # single thingme
          plt.scatter(x_kk,np.abs(mn/st),color=col,marker=mkr)
        else:
          plt.errorbar(x_kk,np.abs(mn),st,fmt='o',color=col,marker=mkr)
  plt.xticks(np.arange(N_pars))
  plt.gca().set_xticklabels([r'$w$',r'$\mu$',r'$\sigma$'])
  plt.ylim(.9,None)
  plt.yscale('log')
  plt.ylabel(r'Significance ($N_{\sigma}$)')
  if 1: # plot sigmas
    xlim = plt.xlim(plt.xlim())
    for ii in range(5): # plot 5 sigma gradients
      y_ii = ones * (ii + 1)
      plt.fill_between(xlim,-y_ii,y_ii,color='k',lw=0,alpha=.0625)
  plt.show()


########################################################################
# run test

if __name__ == '__main__':
  if 1: 
    # read in file
    fname = rd_gamma.fname_Bz # Buzzard simulation, vetted at 0.2L*
    with h5py.File(fname,'r') as ds:
      Z,Z_err,bands,bands_err = [ds[key][()] for key in \
                                 ['Z','Z_err','bands','bands_err']]
    # Zv,dZ = .3,.001 # mask = (.299 < Z) * (Z < .301)
    # Zv,dZ = .1,.01
    Zv,dZ = .6,.001
    mask = (Zv-dZ < Z) * (Z < Zv+dZ)
    bands,bands_err = [v[mask] for v in [bands,bands_err]]
    # analyze file
    f1 = 'output/par_mag_run_Zp6.h5'
    f2 = 'output/sig_Zp6.npy'
    fit_slice(bands,bands_err,2,50,m_star=R14(Zv),fname_save=f1)
    print("~fin")
    # plot analysis
    plot_fit(f1)
    calc_sig(f1,f2)
    plot_sig(f2)
  # 
  if 0: # analyze SDSS
    # set up filenames
    f0 = rd_gamma.fname_SDSS_ext
    f1 = 'output/par_mag_run_SDSS.h5'
    f2 = 'output/sig_SDSS.h5'
    # read in data
    with h5py.File(f0,'r') as ds:
      Z = ds['Z'][:]
      Zv,dZ = .4,.1 # full dataset
      mask = (Zv-dZ < Z) * (Z < Zv+dZ)
      bands,bands_err = [ds[key][()][mask,1:] for key in ['bands','bands_err']]
      # toss u-band for now
    # analyze file
    N_fit = 2
    N_boot = 3 # 50 # for full run
    fit_slice(bands,bands_err,N_fit,N_boot,m_star=R14(Zv),fname_save=f1)
    # plot analysis
    plot_fit(f1)
    calc_sig(f1,f2)
    plot_sig(f2)
