# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# set up KLLR and tools to aid it

from kllr import kllr_model
from kllr.regression_model import calculate_weigth as weights

# helper libraries
import numpy as np
from sys import stdout


def get_counts(x,xr,kw=.2,kt='gaussian'):
  """ return pseudo counts for each xr bin (sum of all weights at point)
  
  Parameters
  ----------
  x : data input x-values
  xr : KLLR sampled output x-points
  kw : kernel width used in KLLR analysis (default: .2)
  kt : kernel type used in KLLR analysis (default: 'gaussian')
  
  Returns
  -------
  pseudocount N, size=len(xr)
  """
  w,N = np.zeros((2,len(xr)))
  for ii in range(len(xr)):
      w[ii] = np.sum(weights(x,kernel_type=kt,mu=xr[ii],width=kw))
  return w # number of points in analysis bin


def KLLR_bootstrap(x, y, xrange=None, nbins=25, 
                   kernel_type='gaussian', kernel_width=0.2,
                   N_resample=50, printing=False):
  """
  x, y, xrange, 
  nbins=25, kernel_type=None, kernel_width=None,
  N_resample=50 : number of times to resample bootstrap. 
  """
  # set up default x-range
  if xrange is None:
    xrange = np.quantile(x,[0,1])
  
  # 'truth' from using *all* datapoints
  lm = kllr_model(kernel_type,kernel_width)
  KLLR_all = lm.fit(x,y,xrange,nbins)
  
  # set up reports
  KLLR_pars, KLLR_par_err = np.zeros((2,5,N_resample,nbins))
  _xr,_yrm,_icpt,_sl,_sig = KLLR_pars
  
  if printing:
    print("Running bootstrap, step ",end='')
    stdout.flush()
  
  for jj in range(N_resample):
    mask = np.random.choice(len(x),len(x))
    xr,yrm,icpt,sl,sig = lm.fit(x[mask],y[mask],xrange,nbins)
    _xr[jj] = xr
    _yrm[jj] = yrm
    _icpt[jj] = icpt
    _sl[jj] = sl
    _sig[jj] = sig
    
    if printing:
      print(jj+1,end=', ')
      stdout.flush()
  
  if printing:
    print('fin~')

  KLLR_par_mn = np.mean(KLLR_pars,axis=1)
  KLLR_par_err = np.std(KLLR_pars,axis=1)
  
  return KLLR_all, KLLR_par_mn, KLLR_par_err


def jackknife_fit(x,y,deg=1,w=None,verbose=False):
  """
  Jackknife values and errors on np.polyfit parameters
  (Alternateive to np.polyfit with cov=True)
  Parameters: (x,y,deg=1,w=None)
  Returns: coeff,sigma
    coeff = fit coefficients
    sigma = jackknife error estimates
  """
  assert(len(x) == len(y))
  if w is None:
    w = np.ones(len(x))
  pars = np.zeros((deg+1,len(x)))
  for ii in range(len(x)):
    pars[:,ii] = np.polyfit(np.delete(x,ii),np.delete(y,ii),
                            deg,w=np.delete(w,ii))
  if verbose:
    return np.transpose(pars) # return list of fits
  else: # return mean, std
    return np.mean(pars,axis=1), np.std(pars,axis=1)



