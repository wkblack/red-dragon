r"""
Red Dragon evolved: 
six color + 1 magnitude (i-band, as 0.2L_{*,i} is current lower bound)
emcee fitting
"""
########################################################################
# imports
import h5py
import emcee
import numpy as np
from sys import stdout
from scipy.optimize import minimize
from matplotlib import pyplot as plt
from scipy.stats import multivariate_normal as mvn

import m_star_model

########################################################################
# parameter functions

def guess_pars(Z):
  """
  make a quick guess at the parameters for a given redshift
  _a : slope with magnitude terms (d/d m_i)
  _b : zero point terms (value at m_i == m_*)
  """
  if hasattr(Z,'__len__'):
    # ensure we can do calculations over multiple redshifts
    Z = np.array(Z)
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  # offset terms
  gmr_RS_a = -.06062 * Z - .01752 # ~ -.02
  gmr_RS_b = -3.55 * Z**2 + 4.285 * Z + .497 # drifts up sharp until z~.375
  gmr_BC_a = .695 * Z**2 - .5262 * Z - .0234
  gmr_BC_b = -4.358 * Z**2 + 4.116 * Z + .3075 # drifts up sharp until z~.375
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  # scatter terms
  lg_sig_RS_a = .3555 * Z - .0097
  lg_sig_RS_b = np.poly1d([4.969,-5.28,2.846,-1.61])(Z)
  lg_sig_BC_a = 0 * Z # expect zero evolution with magnitude of BC scatter
  lg_sig_BC_b = np.poly1d([4.867,-6.777,3.061,-1.1392])(Z)
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  # red fraction
  fR_a = -.0602 * Z - .0683 # +/- .02
  fR_b = -.140 * Z + .5494 # at m_*: .506 +/- .006
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  # compile and return
  theta = [gmr_RS_a, gmr_RS_b, gmr_BC_a, gmr_BC_b, \
           lg_sig_RS_a, lg_sig_RS_b, lg_sig_BC_a, lg_sig_BC_b, \
           fR_a, fR_b]
  return np.array(theta)


def theta_to_fit_pars(Z,i,theta=None,warning=True):
  if theta is None:
    theta = guess_pars(Z)
  if (hasattr(Z,'__len__') and hasattr(i,'__len__')) \
      and (len(Z) != len(i)):
    if warning:
      print("Warning: len(Z) != len(i); taking median redshift.") 
    Z = np.median(Z)
  # unpack theta
  gmr_RS_a, gmr_RS_b, gmr_BC_a, gmr_BC_b, \
   lg_sig_RS_a, lg_sig_RS_b, lg_sig_BC_a, lg_sig_BC_b, \
    fR_a, fR_b = theta
  # create fit pars
  x = i - m_star_model.m_star(Z,warnings=False)
  mu_RS = gmr_RS_a * x + gmr_RS_b
  mu_BC = gmr_BC_a * x + gmr_BC_b
  sig_RS = 10**(lg_sig_RS_a * x + lg_sig_RS_b) 
  sig_BC = 10**(lg_sig_BC_a * x + lg_sig_BC_b)
  fR = np.clip(fR_a * x + fR_b,0,1) # enforce non-negative components
  return mu_RS,mu_BC,sig_RS,sig_BC,fR


########################################################################
# likelihood model


def log_prior(theta):
  """
  give prior on parameter set
  _a : slope with magnitude terms (d/d m_i)
  _b : zero point terms (value at m_i == m_*)
  """
  gmr_RS_a, gmr_RS_b, gmr_BC_a, gmr_BC_b, \
   lg_sig_RS_a, lg_sig_RS_b, lg_sig_BC_a, lg_sig_BC_b, \
    fR_a, fR_b = theta
  if (0 < fR_b) * (fR_b < 1) * (fR_a <= 0) * (fR_a > -.5) \
   * (gmr_RS_b < 2.5) * (gmr_RS_b > gmr_BC_b + .2) * (gmr_BC_b > 0) \
   * (-.5 < gmr_RS_a) * (gmr_RS_a < 0) \
   * (-.5 < gmr_BC_a) * (gmr_BC_a < 0) \
   * (np.abs(lg_sig_RS_b + 1) < 1) * (np.abs(lg_sig_BC_b + 1) < .5) \
   * (lg_sig_RS_a > -.1) * (lg_sig_RS_a < .6) \
   * (np.abs(lg_sig_BC_a) < .2):
    return 0.0
  else:
    return -np.inf


def log_likelihood(theta, Z, m, m_err):
  # unpack inputs
  g,r,i,z = m
  ge,re,ie,ze = m_err
  gmr_RS_a, gmr_RS_b, gmr_BC_a, gmr_BC_b, \
   lg_sig_RS_a, lg_sig_RS_b, lg_sig_BC_a, lg_sig_BC_b, \
    fR_a, fR_b = theta
  # 
  x = i - m_star_model.m_star(Z,warnings=False)
  mu_RS = gmr_RS_a * x + gmr_RS_b
  mu_BC = gmr_BC_a * x + gmr_BC_b
  sig_RS = 10**(lg_sig_RS_a * x + lg_sig_RS_b) 
  sig_RS_err = np.sqrt(sig_RS**2 + (ge+re)**2)
  sig_BC = 10**(lg_sig_BC_a * x + lg_sig_BC_b)
  sig_BC_err = np.sqrt(sig_BC**2 + (ge+re)**2)
  fR = fR_a * x + fR_b
  fR = np.clip(fR,0,1) # enforce non-negative components
  # 
  if 1: # seems to work faster, despite not being vectorized..... 
    if 1: # track progress
      # print('here 0') # evaluates every two seconds or so
      print(',',end='')
      stdout.flush()
    return np.sum([fR[ii] * mvn.pdf((g-r)[ii],mu_RS[ii],sig_RS[ii]) \
             + (1-fR[ii]) * mvn.pdf((g-r)[ii],mu_BC[ii],sig_BC[ii]) \
                   for ii in range(len(fR))])
  else:
    # print('here 1')
    return np.sum(fR * mvn.pdf((g-r),mu_RS,sig_RS) \
            + (1-fR) * mvn.pdf((g-r),mu_BC,sig_BC)) 
            


def log_probability(theta, Z, m, m_err):
  " m = magnitude list [griz]; m_err = errors on those magnitudes "
  lp = log_prior(theta)
  if not np.isfinite(lp): # Don't bother calculating the likelihood.
    return -np.inf
  else:
    return lp + log_likelihood(theta, Z, m, m_err)


########################################################################
# maximum likelihood estimate

nll = lambda *args: -log_probability(*args)

def estimate_max_likelihood(Z, m, m_err):
  " use minimize (from SciPy optimize) to estimate maximum likelihood "
  # https://emcee.readthedocs.io/en/stable/tutorials/line/#maximum-likelihood-estimation
  # initial = np.array([m_true, b_true, np.log(f_true)]) + 0.1 * np.random.randn(3)
  initial = guess_pars(Z)
  initial += initial/100 * np.random.randn(len(initial))
  soln = minimize(nll, initial, args=(Z, m, m_err))
  return soln.x


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# try maximum likelihood method

def read_data(f=0): 
  " read in test data "
  f_dir_vet = '/global/cscratch1/sd/wkblack/SkyMaps/buzzard/vet/'
  fname = f_dir_vet + ['subset_low_Z.h5','subset_new_L.h5'][f]
  # pull m, m_err
  bands = ['g','r','i','z'] # DES photometric bands
  fname = '/global/cscratch1/sd/wkblack/SkyMaps/buzzard/vet/subset_low_Z.h5'
  print("Reading in data...")
  with h5py.File(fname,'r') as ds:
    m = [ds['mag_'+c][:] for c in bands]
    m_err = [ds['mag_err_'+c][:] for c in bands]
    Z = ds['redshift_cos'][:]
  return np.array(m), np.array(m_err), Z


def test_max_llh(Z_curr=.3,dZ=.0001,plotting=True):
  m, m_err, Z = read_data(0)
  # select small sample
  Z_lim = [Z_curr, Z_curr + dZ]
  mask_Z = (Z_lim[0] <= Z) * (Z < Z_lim[1])
  print(f"Using $z|[%0.4f,%0.4f)$" % (Z_lim[0],Z_lim[1]))
  print(f"{len(mask_Z[mask_Z])} data points selected.")
  Z_med = np.median(Z[mask_Z])
  print("Fitting...")
  max_llh = estimate_max_likelihood(Z_med, m[:,mask_Z], m_err[:,mask_Z])
  print()
  print("Maximum likelihood estimate:", max_llh)
  if plotting:
    i_vals = np.linspace(min(m[2,mask_Z]),max(m[2,mask_Z]))
    mu_RS,mu_BC,sig_RS,sig_BC,fR = theta_to_fit_pars(Z_med,i_vals,max_llh)
    # plot baseline points
    plt.scatter(m[2,mask_Z],m[0,mask_Z]-m[1,mask_Z],1,lw=0)
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    # plot red relation
    plt.plot(i_vals,mu_RS,'r')
    plt.fill_between(i_vals,mu_RS-sig_RS,mu_RS+sig_RS,color='r',alpha=.125,lw=0)
    for lim in [.5,.68,.95]:
      mask = fR > lim
      if len(mask[mask]) > 1:
        plt.fill_between(i_vals[mask],(mu_RS-sig_RS)[mask],(mu_RS+sig_RS)[mask],
                         color='r',alpha=.125,lw=0)
    # plot blue relation
    plt.plot(i_vals,mu_BC,'b')
    plt.fill_between(i_vals,mu_BC-sig_BC,mu_BC+sig_BC,color='b',alpha=.125,lw=0)
    for lim in [.5,.32,.05]:
      mask = fR < lim
      if len(mask[mask]) > 1:
        plt.fill_between(i_vals[mask],(mu_BC-sig_BC)[mask],(mu_BC+sig_BC)[mask],
                         color='b',alpha=.125,lw=0)
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    # plot guesses faintly
    mu_RS,mu_BC,sig_RS,sig_BC,fR = theta_to_fit_pars(Z_med,i_vals)
    plt.plot(i_vals,mu_RS,'k',alpha=.25)
    plt.fill_between(i_vals,mu_RS-sig_RS,mu_RS+sig_RS,color='k',alpha=.0625,lw=0)
    plt.plot(i_vals,mu_BC,'k',alpha=.25)
    plt.fill_between(i_vals,mu_BC-sig_BC,mu_BC+sig_BC,color='k',alpha=.0625,lw=0)
    # set up axes
    plt.xlabel(r'$m_i$')
    plt.ylabel(r'$g-r$')
    # tidy up and display
    plt.tight_layout()
    plt.show()
  return max_llh


if 0 and __name__=='__main__':
  max_llh = test_max_llh(.20,.005)
  print('~fin')


########################################################################
# run emcee

if 0 and __name__=='__main__':
  m, m_err, Z = read_data(0) # low redshift data
  # select small sample
  dZ = .0001
  Z_curr = .20
  Z_lim = [Z_curr, Z_curr + dZ]
  mask_Z = (Z_lim[0] <= Z) * (Z < Z_lim[1])
  # set up walkers scattered about guess
  pos = guess_pars(np.median(Z))
  shape = nwalkers, ndim = 32, len(pos)
  pos = np.tile(pos,(nwalkers,1)) * (1 + 1e-2 * np.random.randn(*shape))
  pos += 1e-4 * np.random.randn(*shape)
  # run mcmc
  print("Running MCMC") # takes forever...
  sampler = emcee.EnsembleSampler(nwalkers, ndim, log_probability, args=(Z, m, m_err))
  N_steps = 100 # 5000
  sampler.run_mcmc(pos, N_steps, progress=True);


########################################################################
# random code snips

if 0: 
  pos = soln.x + 1e-4 * np.random.randn(32, 3)
  nwalkers, ndim = pos.shape
  #
  sampler = emcee.EnsembleSampler(nwalkers, ndim, log_probability, args=(x, y, yerr))
  sampler.run_mcmc(pos, 5000, progress=True);

if 0: 
  ndim, nwalkers = 5, 100
  ivar = 1. / np.random.rand(ndim)
  p0 = np.random.randn(nwalkers, ndim)
  
  sampler = emcee.EnsembleSampler(nwalkers, ndim, log_prob, args=[ivar])
  sampler.run_mcmc(p0, 10000)
