# Red Dragon

Red Dragon is a python tool for analyzing the red sequence given spectroscopic data. 


## Inputs

Red Dragon takes as input an HDF5 file, already vetted down for use. 
For my runs, I used galaxy photometry from 
[the Buzzard Flock](https://arxiv.org/abs/1901.02401), 
vetted to have luminosities L<0.2L* and 
demanding they be within halos of mass > 10**13.5 solar masses. 

By default, it uses filename `vetted.h5` with fields 
`m_g`, `m_r , `m_i`, `m_z`, `redshift`.

## Use

```python
import rd_beta

# create baseline GaussianMixture components for interpolation (long step)
rd_beta.fit_h5(fname='input.h5')

# probability a galaxy at redshift Z & photometry to be part of red sequence
bands = [m_g,m_r,m_i,m_z]
prob_red = rd_beta.P_red(Z,bands) 
richness = rd_beta.np.sum(prob_red)
red_sequence_mask = prob_red > .5

# alternatively, get relative likelihoods for being part of each component
L_BC,L_GV,L_RS = rd_beta.likelihoods(Z,bands)
prob_green = L_GV/(L_BC+L_GV+L_RS) # be careful not to divide by zero!
green_valley_mask = prob_green > .5
```

## Contact
[wkblack](wkblack@umich.edu)
