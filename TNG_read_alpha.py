# read in full TNG300-1 LGalaxies catalog from
# https://www.tng-project.org/data/docs/specifications/#sec5n
# handle it, and vet it down to something useable. 

########################################################################
# imports

import h5py
import numpy as np

from matplotlib import pyplot as plt
from m_star_model import m_star as R14

# cosmology bits
h = .7 # unitless Hubble constant
H0 = 100 * h # (km/s)/Mpc
c = 299792.458 # km/s

Mf = 10**10/h # Msol, factor all masses in TNG are divided by

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# KLLR imports
from kllr import kllr_model
from kllr.regression_model import calculate_weigth as weights
def get_counts(x,xr,kw=.2,kt='gaussian'):
  w = np.zeros(len(xr))
  for ii in range(len(xr)):
      w[ii] = np.sum(weights(x,kernel_type=kt,mu=xr[ii],width=kw))
  return w # number of points in analysis bin


########################################################################
# load file & fields 

Z = 0.1

TNG_snaps = {
  0.5 : "067",
  0.4 : "072", # DNE
  0.3 : "078",
  0.2 : "084", # DNE
  0.1 : "091",
  0.0 : "099" # DNE
}
  
fname_base = '/global/cscratch1/sd/wkblack/TNG300-1/lgalaxies/LGalaxies_%s.hdf5'
fname = fname_base % TNG_snaps[Z]

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

m_star = R14(Z) # apparent magnitude; apx 20.257
mu = 25 + 5 * np.log10(c*Z/H0) # distance modulus; apx 41.653
emstar = m_star - mu # absolute mag; characteristic luminosity m_*

# read in i-band and make mask
f = h5py.File(fname,'r')
i = f['Galaxy/Mag'][:,18] # i-band, SDSS AB
U,B,V = np.transpose(f['Galaxy/Mag'][:,:3]) # Johnson-Bessel filters

# other useful fields
SFR = f['Galaxy/StarFormationRate'][:] # Msol / yr
M_star = f['Galaxy/StellarMass'][:] * Mf # Msol

if 0: 
  # other fields, not needed at the moment;
  # Don't know if I really want or need these: 
  cmc = f['Galaxy/Central_M_Crit200'][mask] * Mf # Msol
  mc = f['Galaxy/M_Crit200'][mask] * Mf # Msol
  rmc = f['Galaxy/Central_R_Crit200'][mask] / h # ckpc
  rc = f['Galaxy/R_Crit200'][mask] / h # ckpc


########################################################################
# create vetted catalogue

def vet():
  factor = -2.5 * np.log10(.2) # apx + 1.75
  mask = i < emstar + factor # apx 0.2 L_* limited
  l1,l2 = len(mask[mask]), len(mask)
  print(f"Vetting down to {l1} of {l2} galaxies. ({l1/l2*100:0.3g}%)")
  fname_out = '/global/cscratch1/sd/wkblack/TNG300-1/lgalaxies/LGalaxies_067_vetted.hdf5'
  ds_tags = f['Galaxy'].keys()
  ds_attrs = ['Z','m_star (absolute)','m_star (apparent)','mu']
  
  with h5py.File(fname_out,'w') as ds:
    for ii,tag in enumerate(ds_tags):
      print(f"Processing tag {ii+1}/{len(ds_tags)}: {tag}")
      try:
        ds.create_dataset(tag,data=f['Galaxy/'+tag][mask])
      except TypeError:
        x= f['Galaxy/'+tag][:]
        ds.create_dataset(tag,data=x[mask])
    print("Setting attributes and saving.")
    # for tag in ds_attrs: # DNE
    #   exec(f"ds.attrs['{tag}'] = {tag}")
    ds.attrs['Z'] = Z # redshift
    ds.attrs['m_star (absolute)'] = emstar # char. lum. from LF
    ds.attrs['m_star (apparent)'] = m_star # char. lum. from LF
    ds.attrs['mu'] = mu # distance modulus
  # complete
  print(fname_out,"created.")



########################################################################
# analyze sSFR distribution

lg_sSFR_label = r'$\log_{10} {\rm sSFR \cdot yr}$'


def plot_sSFR_hist(vert=-11):
  # import relevant fields
  mask_hist = (SFR > 0) * (M_star > 0) * (U < -18)
  sSFR = SFR[mask_hist] / M_star[mask_hist] # 1/yr; specific star formation rate
  x = np.log10(sSFR)
  i_cutoffs = np.linspace(emstar-2,max(i[mask_hist]),10)
  bins = np.linspace(min(x),max(x),50)
  for i_val in i_cutoffs:
    mask_i = i[mask_hist] < i_val
    plt.hist(x[mask_i],bins,alpha=.5,histtype='step',log=True)
  # tidy up and display
  plt.xlabel(lg_sSFR_label)
  if vert is not None:
    ylim = plt.ylim(plt.ylim())
    plt.plot(vert*np.ones(2),ylim,'r',lw=.5)
  plt.tight_layout()
  plt.show()


def plot_col_hist(cutoff=-11,mag_lim=-18,use_BaBar=True):
  # set up mask and sSFR
  mask = U < mag_lim
  sSFR = SFR[mask] / M_star[mask] # 1/yr; specific star formation rate
  mask_sfr = np.array(sSFR<10**cutoff).astype(bool)
  # set up color, its binning, for loop variables
  col = (U - V)[mask]
  bins = np.linspace(-.5,2.5,75)
  bin_mids = (bins[1:]+bins[:-1])/2.
  vals = [col,col[mask_sfr],col[~mask_sfr]]
  cols = ['k','r','b']
  sizes = [10,5,5]
  for v,c,s in zip(vals,cols,sizes):
    # n,b,p = plt.hist(v,bins,histtype='step',color=c,log=True)
    n,b = np.histogram(v,bins)
    plt.scatter(bin_mids,n,s,color=c)
    if use_BaBar:
      err = np.sqrt(n+.25)
      lo,hi = n + .5 - err, n + .5 + err
    else:
      err = np.sqrt(n)
      lo,hi = n-err, n+err
    plt.fill_between(bin_mids,lo,hi,alpha=.125,lw=0,color=c)
  # tidy up and display
  plt.title(f"sSFR < $10^{{{cutoff}}}$; $M_U < {mag_lim}$")
  plt.xlabel(r'$U-V$')
  plt.xlim(min(bins),max(bins))
  plt.ylabel('counts')
  plt.yscale('log')
  plt.ylim(.9,None)
  plt.tight_layout()
  plt.show()


def plot_sSFR_CM(max_count=10**4):
  # import relevant fields
  mask_CM = (SFR > 0) * (M_star > 0) * (i < emstar + 1.75)
  L = len(mask_CM[mask_CM])
  if L > max_count:
    mask_CM[mask_CM] = np.random.rand(L) < max_count / L
  bands = gv,rv,iv,zv = np.transpose(f['Galaxy/Mag'][mask_CM,16:20])
  sSFR = SFR[mask_CM] / M_star[mask_CM] # 1/yr; specific star formation rate
  x = np.log10(sSFR)
  plt.scatter(iv,gv-rv,2,lw=0,alpha=.25,c=x,cmap='coolwarm_r')
  plt.colorbar(label=lg_sSFR_label)
  plt.xlabel(r'$m_i$')
  plt.ylabel(r'$g-r$')
  plt.tight_layout()
  plt.show()


def plot_SFR_Mstar(N_iter=1):
  mask = (M_star > 1e8) * (SFR > 0)
  mask = (M_star > 1e10) * (SFR > 0) # FIXME: use for testing
  plt.scatter(M_star[mask],SFR[mask],1,alpha=.125,lw=0,c=U[mask])
  # do KLLR analysis
  kw = 0.1 # kernel width
  lm = kllr_model(kernel_width=kw)
  for ii in range(N_iter):
    print("ii:",ii)
    x,y = np.log10(M_star[mask]),np.log10(SFR[mask])
    xr,yrm,icpt,sl,sig = lm.fit(x,y,nbins=100)
    N = get_counts(x,xr,kw)
    err = sig / np.sqrt(N)
    plt.plot(10**xr,10**yrm,'k')
    plt.fill_between(10**xr,10**(yrm-sig),10**(yrm+sig),
                     color='k',alpha=.125,lw=0)
    plt.fill_between(10**xr,10**(yrm-err),10**(yrm+err),
                     color='r',alpha=.125,lw=0)
    lower_bound = np.interp(x,xr,yrm-err) - 1 # for which to keep
    mask[mask] = y > lower_bound
  plt.xlabel(r'$M_\star$')
  plt.xscale('log')
  plt.xlim(1e8,1e12)
  plt.ylabel(r'SFR')
  plt.yscale('log')
  plt.ylim(1e-4,1e2)
  plt.tight_layout()
  plt.show()


########################################################################

if __name__ == '__main__':
  print("In main.")
  # plot_sSFR_hist()
  plot_sSFR_CM()
  # plot_SFR_Mstar()
  # plot_col_hist()
  print('~fin')
