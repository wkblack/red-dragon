# including photometric errors

########################################################################
# import statements 

from time import time
import os
os.environ['OPENBLAS_NUM_THREADS'] = '12'

from os import sys
epsilon = sys.float_info.epsilon
MAX = 1/epsilon # maximum permissible value for condition number

import h5py
import sklearn

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# remove deprecation warnings
import warnings # to shut up sklearn
warnings.filterwarnings("ignore",category=DeprecationWarning)
import matplotlib.cbook # to shut up mpl
warnings.filterwarnings("ignore",category=matplotlib.cbook.mplDeprecation)

from sklearn.mixture import GaussianMixture as GM

# from matplotlib import pyplot as plt
exec(open("mpl_tools.py").read()) # import matplotlib tools and defaults
from matplotlib.colors import LinearSegmentedColormap as LSC
cmap_rgb = LSC.from_list("rgb", ['r','g','b']) # simple rgb
cmap_spec = LSC.from_list("redshift", ['r','#632E86','g','xkcd:aqua','b']) # prettier rgb, with better middle values
vals = [0,1/6.,1/3.,.5,.75,1] # Red to Purple rainbow
cols = ['#EC4141','#EF7135','#FAF5AB','#5FBB4E','#1B98D1','#632E86']
cmap_RD = LSC.from_list("dash",list(zip(vals,cols)))

# create re-sorted color cycle, preserving most of original MPL sorting
ir = 3 # index of red in mpl_colors
mpl_colors_RF = np.array(mpl_colors)[[ir,*range(ir),*range(ir+1,len(mpl_colors))]]

import pygmmis

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# set up sigmas 
from scipy.special import erf
s1,s2,s3,s4,s5 = [erf(ii/np.sqrt(2)) for ii in range(1,6)]

def _Gaussian_interval(mu,sig,a,b):
  """
  determine whether point with Gaussian error falls in [a,b]
  """
  sqrt2sig = np.sqrt(2) * np.array(sig) # for prettier calculation
  a,b,mu = [np.array(v) for v in [a,b,mu]] # ensure vector inputs
  return .5 * np.abs(erf((b-mu)/sqrt2sig) - erf((a-mu)/sqrt2sig))


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# set up interpolation tools
import scipy
spline = scipy.interpolate.UnivariateSpline
interp1d = scipy.interpolate.interp1d


########################################################################
# set up defaults & universals 

fname_default = 'output/fit.h5'
lbl_W = r'$w$'
lbl_M = r'$\left | \vec \mu \right |$'
lbl_S = r'$\left \langle \vec \sigma^2 \right \rangle^{1/2}$'
lbl_c = r'$c_{%i}$'


########################################################################
# general color functions 

def _band_err_to_col_err(err):
  " convert photometric errors (N,N_bands) to color errors (N,N_col) "
  N_bands = np.shape(err)[-1]
  return np.transpose([np.sqrt(err[:,ii+1]**2 + err[:,ii]**2) \
                       for ii in range(N_bands-1)])


def _band_err_to_covar(err):
  " go from photometric band errors to covariance matrix "
  col_err = _band_err_to_col_err(err)
  N_cols = np.shape(col_err)[-1]
  """
  diagonal = np.eye(N_cols) * col_err
  off_diag = np.eye(N_cols,k=-1) + np.eye(N_cols,k=-1) # off-diagonals
  off_diag *= err[1:-1] # perfectly correlated errors 
  """
  diagonal = np.transpose([[col_err[:,ii] * (ii==jj)
                            for ii in range(N_cols)]
                           for jj in range(N_cols)])
  off_diag = np.transpose([[-err[:,max(ii,jj)] * (np.abs(ii-jj)==1) \
                            for ii in range(N_cols)] \
                           for jj in range(N_cols)])
  return diagonal + off_diag


########################################################################
# RD class, including interpolation functions

class dragon:
  """
  rd = dragon('input.h5')
  rd.P_red(Z,bands)
  
  # back-end tools:
  rd.covar(Z)
  """
  def __init__(self,fname,factor=.5):
    with h5py.File(fname,'r') as ds:
      # load data (dragon DNA)
      Zb = self._Z_bins = ds['Z_bins'][:] # redshift bins
      self._Z_mids = (Zb[1:] + Zb[:-1])/2. # redshift midpoints
      self._w = ds['w'][:] # measured weights across redshift
      self._mu = ds['mu'][:] # measured means "
      self._Sigma = ds['Sigma'][:] # measured covariances "
      # load attributes
      self.N_cols = ds.attrs['N_cols'] # number of colors used in fit
      self.N_fit = ds.attrs['N_fit'] # number of Gaussian components
      self._exclusions = ds.attrs['exclusions'] # points to exclude
      self.fname = ds.attrs['fname']
      # secondary attributes (unneeded?)
      self._N_select = ds.attrs['N_select']
      self._use_initial = ds.attrs['use_initial']
      self._use_errors = ds.attrs['use_errors']
      self._lim_weight = ds.attrs['lim_weight']

    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # set up defaults for fitting
    # this allows for the user to change values easily on the front-end,
    # while the fit functions check for changes and update accordingly 
    self._reset = False
    
    # component weight parameters
    self.tol_w = 1e-3 # tolerance for f_R (exclude zeros in fit)
    self.method_w = 'spline'
    self.softening_w_spline = .0075 # best in ~(.001,.0075)
    self._softening_w_spline = self.softening_w_spline # to check against
    # mean color parameters
    self.method_mu = 'spline'
    self.softening_mu_spline = .005 # best in ~(.003,.0075)
    self._softening_mu_spline = self.softening_mu_spline # ditto
    # correlation matrix parameters
    self.method_Sigma = 'interp1d'
    self.softening_Sigma_spline = 1e-4 # .750 previously
    self._softening_Sigma_spline = self.softening_Sigma_spline # ditto
    
    # only allow spline default if there are sufficient points
    N_min = 4 # minimum needed for cubic spline 
    if len(self._Z_mids) < N_min: # linear interpolation only
      self.method_w = 'interp1d'
      self.method_mu = 'interp1d'
      self.method_Sigma = 'interp1d'
    
    # TODO: figure out minimum needed for abs(fp-s)/s < tol = 0.001
    pass # TODO: alter softening to larger value to better fit this
    
    self.set_fits()
    
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # tame the dragon 
    
    # re-sort
    self.set_sorting(factor)
    
    # test which redshifts to exclude
    self.test_exclusions() # test old exclusions, make new exclusions
    
    # set up interpolations, splines, polyfits (relatively inexpensive)
    self.set_fits()
    
    pass # complete! 
  
  
  def __str__(self):
    return f"{self.N_fit}K {self.N_cols}D Red Dragon, trained from {self.fname}"
  
  
  def set_fits(self):
    " set up spline fits "
    mask = ~np.array(self._exclusions).astype(bool)
    Z = self._Z_mids[mask]
    w,mu,Sigma = [v[mask] for v in [self._w,self._mu,self._Sigma]]
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # linear interpolation between points (interp1d)
    self._w_interp, self._mu_interp, self._Sigma_interp = [interp1d( \
      Z,v,axis=0,assume_sorted=True,fill_value='extrapolate') \
        for v in [w,mu,Sigma]]
    self._mu_interp = interp1d(Z,mu,axis=0,assume_sorted=True,
                               fill_value='extrapolate')
    self._Sigma_interp = interp1d(Z,Sigma,axis=0,assume_sorted=True,
                                  fill_value='extrapolate')
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # spline interpolation
    # weights
    self._w_spline = [spline(Z,w[:,ii],s=self.softening_w_spline) \
                      for ii in range(self.N_fit)]
    self._softening_w_spline = self.softening_w_spline # update val
    # means
    self._mu_spline = [[spline(Z,mu[:,ii,jj],s=self.softening_mu_spline) \
                        for jj in range(self.N_cols)] \
                       for ii in range(self.N_fit)]
    self._softening_mu_spline = self.softening_mu_spline # ditto
    # covariances
    self._Sigma_spline = [[[spline(Z,Sigma[:,ii,jj,kk], \
                             s=self.softening_Sigma_spline) \
                            for kk in range(self.N_cols)] \
                           for jj in range(self.N_cols)] \
                          for ii in range(self.N_fit)]
    self._softening_Sigma_spline = self.softening_Sigma_spline # ditto
    return
  
  
  def get_WMS(self):
    " get {weights, colors, scatters}, summed across colors "
    # return weights
    W = self._w
    # do vector length in multi-color space 
    # FIXME: won't work well w/ negative colors
    M = np.linalg.norm(self._mu,axis=-1)
    # return total scatter across all colors
    var = np.diagonal(self._Sigma,axis1=-2,axis2=-1) # variances
    S = np.sqrt(np.sum(var,axis=-1))
    # S = np.sqrt(np.sum(var**2,axis=-1))
    if 0: # normalize
      norm = lambda V: (V-np.mean(V))/np.std(V)
      W_norm,M_norm,S_norm = [norm(V) for V in [W,M,S]]
    return W,M,S
  
  
  def test_exclusions(self):
    """
    look at weights, means, &c. and make additional exclusions,
    updating self._exclusions
    """
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # TODO: be more rigorous here! 
    # just in general, this will only find /single/ errant points---
    # these methods won't find /groups/ of errant points. 
    pass
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # jackknife error -> sigmas from line when excluded from fit?
    pass
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # check second derivatives in mean color
    mu = self._mu
    second = np.zeros_like(mu)
    ii = np.arange(np.shape(mu)[0] - 2) # excluding endpoints
    second[1:-1] = mu[ii] - 2 * mu[ii+1] + mu[ii+2]
    second[0] = second[1] # start = second
    second[-1] = second[-2] # endpoint = penultimate
    mn,st = [f(second,axis=0) for f in [np.mean,np.std]]
    sigs = (second - mn)/st
    sig_tot = np.sqrt(np.sum(sigs**2,axis=(-2,-1)))
    factor = np.sqrt(self.N_cols)
    mask_odd = sig_tot/factor > 3
    if len(mask_odd[mask_odd]) > 0:
      print("Possible problems (large local 2nd deriv in colors):")
      print(f"Odd redshifts:",np.round(self._Z_mids[sig_tot/factor>3],5))
      print(f"Errant redshifts:",np.round(self._Z_mids[sig_tot/factor>5],5))
    self._exclusions[sig_tot/factor>5] = 1
  
  
  def set_sorting(self,factor=.5):
    # test WMS relations and correct if needed
    W,M,S = self.get_WMS()
    sort = np.flip(np.argsort(M + factor * S),axis=-1)
    # sort w, mu, Sigma accordingly 
    self._w = np.array([v[s] for v,s in zip(self._w,sort)])
    self._mu = np.array([v[s] for v,s in zip(self._mu,sort)])
    self._Sigma = np.array([v[s] for v,s in zip(self._Sigma,sort)])
    self.set_fits()
    return
  
  
  def get_weights(self,Z):
    " returns weights for given redshift(s) "
    # calculate fit
    if self.method_w == 'interp1d':
      # linearly interpolate between input points
      report = self._w_interp(Z)
    elif self.method_w == 'spline':
      # use softened spline to estimate components
      if self.softening_w_spline != self._softening_w_spline:
        self.set_fits() # update values
      report = np.transpose([spl(Z) for spl in self._w_spline])
    else:
      print(f"ERROR: unset method '{self.method_w}'")
      return
    # clip to fit within (0,1); literal zero yields error
    report = np.clip(report,0+self.tol_w,1-self.tol_w)
    if hasattr(Z,'__len__'):
      return report/np.sum(report,axis=-1)[:,np.newaxis]
    else:
      return report/np.sum(report,axis=-1)
  
  
  def get_means(self,Z): # ,method=None,softening=.035):
    " returns means for given redshift(s) "
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    if self.method_mu == 'interp1d':
      # linearly interpolate between input points
      report = self._mu_interp(Z)
    elif self.method_mu == 'spline':
      # use softened spline to estimate components
      if self.softening_mu_spline != self._softening_mu_spline:
        self.set_fits() # update values
      report = np.transpose([[self._mu_spline[ii][jj](Z) \
                              for ii in range(self.N_fit)] \
                             for jj in range(self.N_cols)])
    else:
      print(f"ERROR: unset method '{self.method_mu}'")
      return
    return report
  
  
  def get_covars(self,Z):
    " returns covariance matrix for given redshift(s) "
    if self.method_Sigma == 'interp1d':
      # linearly interpolate between input points
      report = self._Sigma_interp(Z)
    elif self.method_Sigma == 'spline':
      # use softened spline to estimate components
      if self.softening_Sigma_spline != self._softening_Sigma_spline:
        self.set_fits() # update values
      report = np.transpose([[[self._Sigma_spline[ii][jj][kk](Z) \
                               for ii in range(self.N_fit)] \
                              for jj in range(self.N_cols)] \
                             for kk in range(self.N_cols)])
    else:
      print(f"ERROR: unset method '{self.method_Sigma}'")
      return
    return report
  
  
  def get_corr(self,Z):
    " return correlation matrix for given redshift(s) "
    # weight by each component
    w = self.get_weights(Z) # shape: (len(Z),N_fit)
    Sigma = self.get_covars(Z) # shape: (len(Z),N_fit,N_col,N_col)
    diag = np.sqrt(np.diagonal(Sigma,axis1=-2,axis2=-1)) # nix last axis
    if hasattr(Z,'__len__'):
      for ii in range(np.shape(Sigma)[0]):
        for jj in range(np.shape(Sigma)[1]):
          for kk in range(np.shape(Sigma)[2]):
            for ll in range(np.shape(Sigma)[3]):
              Sigma[ii,jj,kk,ll] *= w[ii,jj]
              Sigma[ii,jj,kk,ll] /= diag[ii,jj,kk]
              Sigma[ii,jj,kk,ll] /= diag[ii,jj,ll]
    else:
      for jj in range(np.shape(Sigma)[0]):
        for kk in range(np.shape(Sigma)[1]):
          for ll in range(np.shape(Sigma)[2]):
            Sigma[jj,kk,ll] *= w[jj]
            Sigma[jj,kk,ll] /= diag[jj,kk]
            Sigma[jj,kk,ll] /= diag[jj,ll]
    return np.sum(Sigma,axis=-3) # sum across components
  
  
  def get_gmm(self,Z):
    gmm = pygmmis.GMM(self.N_fit,self.N_cols)
    gmm.amp = self.get_weights(Z)
    gmm.mean = self.get_means(Z)
    gmm.covar = self.get_covars(Z)
    return gmm
  
  
  def _get_logL_slice(self,Z_med,bands,bands_err=None):
    " helper method to handle slice of data for get_logL "
    # grab Gaussian mixture for this redshift
    X = -np.diff(bands,axis=1) # color measurements
    gmm = self.get_gmm(Z_med)
    # if errors available, set up covariance in errors
    if bands_err is not None:
      covar = _band_err_to_covar(bands_err)
    else: 
      covar = None
    # calculate log likelihoods for each component
    logLs = np.array([gmm.logL_k(ii,X,covar) for ii in range(gmm.K)])
    return logLs
  
  
  def get_logL(self,Z,bands,bands_err=None,dZ=.01):
    """
    returns likelihoods for each component given redshifts, bands, err
    
    get_logL(Z,bands,bands_err=None,dZ=.01):
    Z : median redshift or redshift for each galaxy
    bands : photometry for each galaxy
    bands_err : photometric error for each galaxy
    dZ=.01 : maximum width permissable for binned color analysis
    
    For samples with redshifts for each galaxy (i.e. redshift-evolving
    lightcone, rather than a think redshift slice), RD uses bins of
    width dZ to analyze chunks of galaxies at a time. 
    """
    # set up redshift bins
    Z_lim = np.quantile(Z,[0,1])
    if np.diff(Z_lim) < dZ:
      Z = np.median(Z) # condense to single redshift bin
    if hasattr(Z,'__len__'): # handle a series of redshifts
      # break up data into series of small redshift bins, width dZ
      # (Don't actually use N GMMs!)
      N_bins = int(np.ceil(np.diff(Z_lim)/dZ))
      Z_bins = np.linspace(*Z_lim,N_bins+1)
      # set up report variable for outputs
      logLs = np.zeros((self.N_fit,len(Z))) # for each data point
      for ii in range(N_bins):
        mask_ii = (Z_bins[ii] <= Z) * (Z < Z_bins[ii+1])
        Z_ii = np.median(Z[mask_ii])
        err_ii = None if bands_err is None else bands_err[mask_ii]
        Ls_ii = self._get_logL_slice(Z_ii,bands[mask_ii],err_ii)
        logLs[:,mask_ii] = Ls_ii
      return logLs
    else: # use single redshift bin
      return self._get_logL_slice(Z,bands,bands_err)
  
  
  def get_pred(self,Z,bands,bands_err=None,dZ=.01):
    " returns index of maximum likelihood component "
    logLs = self.get_logL(Z,bands,bands_err,dZ)
    return np.argmax(logLs,axis=0)
  
  
  def get_P(self,Z,bands,bands_err=None,dZ=.01):
    logLs = self.get_logL(Z,bands,bands_err,dZ)
    denominator = np.sum(np.exp(logLs),axis=0)
    denominator[denominator==0] = 1
    return np.array([np.exp(logLs[ii])/denominator \
                    for ii in range(self.N_fit)])
  
  
  def P_red(self,Z,bands,bands_err=None,dZ=.01):
    return self.get_P(Z,bands,bands_err,dZ)[0]
  
  
  def wyvren_bounds(self,Z):
    """
    hard cut values from RD DNA to select RS
    returns (RS lower bounds, RS upper bounds), shape (2,len(Z),N_col)
    """
    # reformat redshift
    if not hasattr(Z,'__len__'):
      Z = np.array([Z])
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # grab each part of the Gaussian mixture
    w = self.get_weights(Z)
    mu = self.get_means(Z)
    Sigma = self.get_covars(Z)
    stds = np.sqrt(np.diagonal(Sigma,0,-1,-2)) # grab scatters of each
    # select RS and BC nominal components
    w0,w1 = w[:,0], w[:,1]
    mu_RS,mu_BC = mu[:,0], mu[:,1]
    sig_RS,sig_BC = stds[:,0], stds[:,1]
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # set up report (it's long)
    # 1) square root terms:
    A = (w0/w1)[:,np.newaxis] * sig_BC/sig_RS
    A = np.clip(2 * (sig_BC**2 - sig_RS**2) * np.log(A),0,1)
    A = sig_BC * sig_RS * np.sqrt((mu_BC-mu_RS)**2 + A)
    # 2) in square brackets terms
    B = 1/(sig_BC**2 - sig_RS**2)
    C = mu_RS*sig_BC**2 - mu_BC*sig_RS**2
    m,p = np.array([B * (C + mp * A) for mp in [-1,+1]])
    p[p<m] = np.inf
    return m,p
  
  
  def wyvren_pred1D(self,Z,bands,bands_err=None):
    m,p = self.wyvren_bounds(Z) # lone color RS lower & upper bounds
    cols = -np.diff(bands)
    if bands_err is None:
      # boolean selection
      return (m < cols) * (cols < p)
    else:
      col_err = _band_err_to_col_err(bands_err)
      return _Gaussian_interval(cols,col_err,m,p)
  
  
  pass # end of `dragon` class


########################################################################
# fitting functions 

def fit_slices(ds,Z_bins,N_fit=2,use_errors=False,use_initial=None,
               fname_out='output/fit.h5',printing=True,plotting=False,
               N_select=-1,lim_weight=.001,sig_max=4,timing=True,
               N_Z=10**4):
  """
  lim_weight = minimum weight allowed to be included
  sig_max = maximum std dev away from mean color allowed per slice
  """
  if timing:
    old_time = curr_time = start = time()
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  # read in data from dataset ds
  Z_mids = (Z_bins[1:] + Z_bins[:-1])/2.
  bands = ds['bands'][:]
  N_gals,N_bands = np.shape(bands)
  N_cols = N_bands - 1
  X = -np.diff(bands,axis=1)
  Z = ds['Z'][:]
  fname = ds.filename
  # read in errors, if using
  if use_errors:
    Z_err = ds['Z_err'][:]
    bands_err = ds['bands_err'][:]
    col_err = _band_err_to_col_err(bands_err)
    covars = _band_err_to_covar(bands_err)
  else:
    Z_err = np.zeros_like(Z)
    bands_err = np.zeros_like(bands)
    col_err = np.zeros_like(X)
  # read in baseline, if using
  if use_initial is not None:
    if use_initial.__class__ is str:
      rd = dragon(use_initial) # set up dragon for initial guess 
      if 1:
        # soften input, as initial fits are generally noisy
        rd.softening_w_spline = .05
        rd.softening_mu_spline = .1 # should be larger
        rd.softening_Sigma_spline = .005 # should be larger
    elif use_initial.__class__ is dragon:
      rd = use_initial # use input dragon
    else:
      print(f"ERROR: Invalid input class ({use_initial.__class__})")
      return
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  # set up report variables
  N_bins = len(Z_bins) - 1
  exclusions = np.zeros(N_bins)
  w = np.zeros((N_bins,N_fit))
  mu = np.zeros((N_bins,N_fit,N_cols))
  Sigma = np.zeros((N_bins,N_fit,N_cols,N_cols))
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  # iterate through redshift bins
  for ii in range(N_bins):
    ####################################################################
    # calculate timing information
    if timing:
      curr_time = time()
      elapsed = time() - start
      if elapsed > 3:
        # set up time since last
        dt = curr_time - old_time
        if dt < 90:
          a = f"{dt:.1f} s"
        else:
          a = f"{dt/60.:.1f} min"
        # set up total elapsed time
        if elapsed < 90:
          b = f" ({elapsed:.0f} s total)"
        else:
          b = f" ({elapsed/60.:.0f} min total)"
        print("dt: " + a + b)
      old_time = curr_time
    
    ####################################################################
    # Redshift cuts
    
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # hard cut redshift slice for starters
    mask_ii = (Z_bins[ii] <= Z) * (Z < Z_bins[ii+1])
    N_ii = len(mask_ii[mask_ii])
    warn = "" if N_ii > N_fit * 10 else " (low number count!)"
    print(f"{ii+1}/{N_bins}: Z|[{Z_bins[ii]:0.4g},{Z_bins[ii+1]:0.4g})" \
          f" -> {N_ii} galaxies" + warn)
    
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # select via probabilistic cut
    if use_errors and np.all(Z_err>0):
      # use probabilistic redshift cut
      weight = _Gaussian_interval(Z,Z_err,Z_bins[ii],Z_bins[ii+1])
    else: # hard redshift cut
      weight = np.copy(mask_ii)
    P = weight/np.sum(weight)
    mask_ii = np.random.choice(len(Z),size=N_Z,replace=True,p=P)
    
    ####################################################################
    # remove spurious points in color space
    
    if sig_max > 0:
      # if summed sigma > sig_max, exclude
      mn,st = [f(X[mask_ii],axis=0) for f in [np.mean,np.std]]
      sig = (X[mask_ii] - mn)/(st + col_err[mask_ii])
      sig_tot = np.sqrt(np.sum(sig**2,axis=1))
      mask_ii = mask_ii[sig_tot < sig_max]
    
    ####################################################################
    # set up initial weights, if using
    
    if use_initial is not None:
      weights_ii = rd.get_weights(Z_mids[ii])
      means_ii = rd.get_means(Z_mids[ii])
      covars_ii = rd.get_covars(Z_mids[ii])
      precs_ii = np.linalg.inv(covars_ii)
    elif False and (ii > 0) and (exclusions[ii-1] == 0):
      # DEACTIVATED for now
      # use previous values if they fit well
      weights_ii = w[ii-1]
      means_ii = mu[ii-1]
      covars_ii = Sigma[ii-1]
      precs_ii = np.linalg.inv(covars_ii)
      pass
    else: # reset to None if no good previous values exist
      weights_ii,means_ii,covars_ii,precs_ii = None, None, None, None
    
    ####################################################################
    # measure fit
    if use_errors: # use pygmmis
      covar = covars[mask_ii]
      gmm = pygmmis.GMM(K=N_fit, D=N_cols)
      if use_initial is not None:
        gmm.amp = weights_ii
        gmm.mean = means_ii
        gmm.covar = covars_ii
        init = 'none'
      else:
        init = 'kmeans'
      logL,U = pygmmis.fit(gmm,X[mask_ii],covar,init_method=init)
      weights_ii = gmm.amp
      means_ii = gmm.mean
      Sigma_ii = gmm.covar
      Ps = [gmm.logL_k(jj,X[mask_ii],covar) for jj in range(N_fit)]
      pred = np.argmax(Ps,axis=0)
    else: # use sklearn
      gmm = GM(N_fit, weights_init=weights_ii, means_init=means_ii, precisions_init=precs_ii)
      fit = gmm.fit(X[mask_ii]) 
      weights_ii = fit.weights_
      means_ii = fit.means_
      Sigma_ii = fit.covariances_
      precs_ii = fit.precisions_
      pred = fit.predict(X[mask_ii])
    
    ####################################################################
    # fix sorting
    # NOTE: This likely needs tweaking, depending on N_fit and dataset
    medians = [np.median(X[mask_ii][pred==jj],axis=0) for jj in range(N_fit)]
    M = np.linalg.norm(medians,axis=1) # distance to color vector median
    if 0: # use quantiles
      qtls = [np.quantile(X[mask_ii][pred==jj],[1-s1,s1],axis=0) for jj in range(N_fit)]
      sig_range = np.squeeze(np.diff(qtls,axis=1)) / 2. # difference between quantiles
      S = np.sqrt(np.sum(sig_range**2,axis=-1)) # combine colors
    else: # use standard deviation
      # deactivate to eschew outlier influence
      S = np.sqrt(np.sum(np.diagonal(Sigma_ii,axis1=-2,axis2=-1),axis=-1))
    # set up factor of spread to use in detangling GV & BC
    factor = .5 # seems to generally work well, though sometimes need -1
    sort = np.flip(np.argsort(M + factor * S))
    
    ####################################################################
    # check whether it was a bad fit; mark if so
    if np.any(weights_ii*(1-weights_ii) < lim_weight*(1-lim_weight)):
      exclusions[ii] = 1
    
    ####################################################################
    # save redshift slice
    w[ii] = [weights_ii[sort][jj] for jj in range(N_fit)]
    mu[ii] = [means_ii[sort][jj] for jj in range(N_fit)]
    Sigma[ii] = [Sigma_ii[sort][jj] for jj in range(N_fit)]
    
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # plot fit
    if plotting: 
      re_pred = np.copy(pred)
      for jj in range(N_fit):
        re_pred[pred==jj] = sort[jj]
      plt.subplot(121)
      plt.scatter(X[mask_ii,0],X[mask_ii,1],3,alpha=.25,lw=0,c=re_pred)
      plt.subplot(122)
      if N_cols >=4:
        plt.scatter(X[mask_ii,2],X[mask_ii,3],3,alpha=.25,lw=0,c=re_pred)
      else:
        for jj in range(N_fit):
          _ = plt.hist(X[mask_ii,2][re_pred==jj],100,histtype='step')
      plt.show()
  
  ######################################################################
  # save output to file
  with h5py.File(fname_out,'w') as ds:
    for key,data in zip(['w','mu','Sigma','Z_bins'],
                        [ w , mu , Sigma , Z_bins ]):
      ds.create_dataset(key,data=data)
    for key,data in zip(['exclusions','N_fit','N_cols','N_select','fname',
                         'use_initial','use_errors','lim_weight'],
                        [ exclusions , N_fit , N_cols , N_select , fname ,
                          use_initial , use_errors , lim_weight ]):
      ds.attrs[key] = data if data is not None else 'None'
  return fname_out


def fit_file(fname,N_fit=2,fname_out=fname_default,N_max=2*10**4,
             use_initial=None,Z_min=None,Z_max=None,lim_weight=.01,
             printing=True,use_errors=None,dZ_0=.025,dZ_1=.01):
  """
  N_fit=2 : number of Gaussian components to model galaxy photometrics with
  N_max=2e4 : apx maximum number of points to use in first run model
  lim_weight=.01 : minimum component weight to be included in interpolation model
  Z_min,Z_max=None,None : range of redshifts to evaluate (default spans)
  """
  assert(N_fit > 1) # otherwise you can't select RS
  # multi-step file analyzer
  ds = h5py.File(fname,'r')
  Z = ds['Z'][:]
  has_errs = 'bands_err' in ds.keys() # else no errors available
  if use_errors is not None:
    has_errs = use_errors
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # set up bins
  if Z_min is None:
    Z_min = min(Z)
  if Z_max is None:
    Z_max = max(Z)
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # 1) simple wide-bin sklearn fit as base model
  if printing:
    print("Sparse fitting...")
  # set up bins
  N_bins = np.round((Z_max - Z_min)/dZ_0).astype(int) # bin count
  Z_bins = np.linspace(Z_min,Z_max,N_bins) # analysis bins
  # calculate temporary output filename
  split = fname_out.split('/')
  split[-1] = 'tmp_' + split[-1]
  fname_tmp = "/".join(split)
  # run model
  out = fit_slices(ds,Z_bins,N_fit,use_errors=False,fname_out=fname_tmp,
                   use_initial=use_initial,printing=printing,
                   N_select=N_max,lim_weight=lim_weight)
  if printing:
    print("Saved",out)
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # 2) finer-binned fit, incl errors if available
  if printing:
    print("Finer fitting...")
  # set up bins
  N_bins = np.round((Z_max - Z_min)/dZ_1).astype(int) # bin count
  Z_bins = np.linspace(Z_min,Z_max,N_bins) # analysis bins
  out = fit_slices(ds,Z_bins,N_fit,use_errors=has_errs,
                   use_initial=fname_tmp,fname_out=fname_out,
                   printing=printing,N_select=N_max,lim_weight=lim_weight)
  if printing:
    print("Saved",out)
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # 3) check for exclusions or misplaced values
  rd = dragon(fname_out)
  return rd


########################################################################
# analysis functions

def plot_fit(rd=fname_default,fit_lines=True):
  " plot fit points and optionally curve fit "
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  if rd.__class__ == dragon: # plot exant dragon
    # grab attributes
    Z_bins = rd._Z_bins
    Z_mids = rd._Z_mids
    N_fit = rd.N_fit
    N_cols = rd.N_cols
    # grab points
    w_v = rd._w
    mu_v = rd._mu
    Sigma_v = rd._Sigma
  else: # read in from file
    # grab attributes
    ds = h5py.File(rd,'r')
    Z_bins = ds['Z_bins'][:]
    Z_mids = (Z_bins[1:] + Z_bins[:-1])/2.
    N_fit = ds.attrs['N_fit']
    N_cols = ds.attrs['N_cols']
    # read in points
    w_v,mu_v,Sigma_v = [ds[key][:] for key in ['w','mu','Sigma']]
    if fit_lines:
      rd = dragon(rd)
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  if fit_lines:
    zv = np.linspace(Z_bins[0],Z_bins[-1],1000)
    w = rd.get_weights(zv)
    mu = rd.get_means(zv)
    Sigma = rd.get_covars(zv)
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  if 1: # plot weights
    plt.figure(1)
    for ii in range(N_fit):
      plt.scatter(Z_mids,w_v[:,ii],10)
      if fit_lines:
        plt.plot(zv,w[:,ii])
    plt.xlabel('Redshift')
    plt.ylabel(r'Weight')
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  if 1: # plot means
    fig,axs = plt.subplots(1,N_cols,sharex=True)
    for ii in range(N_cols):
      axs[ii].set_xlabel('Redshift')
      axs[ii].set_ylabel(f'$c_{ii}$')
      for jj in range(N_fit):
        axs[ii].scatter(Z_mids,mu_v[:,jj,ii],10)
        if fit_lines:
          axs[ii].plot(zv,mu[:,jj,ii])
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  if 1: # plot covariances
    fig,axs = plt.subplots(N_cols,N_cols,sharex=True,sharey='row')
    if np.all(Sigma_v > 0):
      axs[0,0].set_yscale('log')
    for ii in range(N_cols):
      for jj in range(N_cols):
        if jj > ii: # upper right triangle
          axs[ii,jj].set_visible(False)
          continue
        if jj == 0: # first column (?)
          axs[ii,jj].set_ylabel(f'$c_{ii}$')
        if ii == N_cols - 1: # last row (?)
          # axs[ii,jj].set_xlabel('Redshift')
          axs[ii,jj].set_xlabel(f'$c_{jj}$')
        for kk in range(N_fit):
          axs[ii,jj].scatter(Z_mids,Sigma_v[:,kk,jj,ii],10)
          if fit_lines:
            axs[ii,jj].plot(zv,Sigma[:,kk,jj,ii])
  plt.show()


def plot_gif(data,rd,Z_bins=None,dZ=None,coloring=None,display=False,
             colormap=cmap_spec,labels=None,sizing=None,
             xlim=None,ylim=None,factor=.5,
             fnames_out='output/gif%04i.png'):
  """
  factor = amount by which to decrease the size of all points 
           (magnitude dependent at the moment, by i-band.)
  """
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # read in inputs and convert to proper form
  if rd.__class__ is str:
    # read in as filename if not already dragon
    rd = dragon(rd)
  with h5py.File(data,'r') as ds:
    Z,bands = [ds[key][:] for key in ['Z','bands']]
    N = len(Z)
    Z_err = ds['Z_err'][:] if 'Z_err' in ds.keys() else np.zeros(N)
    bands_err = ds['bands_err'][:] if 'bands_err' in ds.keys() else np.zeros(N)
  cols = -np.diff(bands,axis=1)
  # set up redshift limits if not defined
  if Z_bins is None:
    Z_bins = np.quantile(Z,[0,1]) # default = all
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  if not hasattr(Z_bins,'__len__'):
    # allow for integer input, i.e. single redshift slice
    if dZ is None:
      dZ = .01 # as default
    Z_bins = [Z_bins - dZ, Z_bins + dZ]
    dZ = None # so it's treated as the bin it is now
  if dZ is None:
    N = len(Z_bins) - 1
  else:
    N = len(Z_bins)
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  for ii in range(N):
    if dZ is None:
      Z_lim = Z_bins[ii], Z_bins[ii+1]
    else: # treat Z_bins as Z_mids +/- dZ/.2
      Z_lim = Z_bins[ii] - dZ/2., Z_bins[ii] + dZ/2.
    mask_ii = (Z_lim[0] <= Z) * (Z < Z_lim[1])
    label_ii = f"Z|[{Z_lim[0]:0.3f},{Z_lim[1]:0.3f})"
    Z_med = np.median(Z[mask_ii])
    P_red = rd.P_red(Z_med,bands[mask_ii],bands_err[mask_ii])
    if (coloring is None) or (coloring == 'P_red'):
      # coloring = P_red > .5
      color = 1 - P_red # color by redness (0=red)
    elif coloring == 'pred':
      color = rd.get_pred(Z_med,bands[mask_ii],bands_err[mask_ii])
    # set up sizing of points
    if sizing is None:
      # enlarge bright galaxies; shrink dim ones
      mi_star_pseudo = 21.25 + 2.128 * np.log(Z_med) + 8.30 * Z_med
      mi_star_pseudo = 18.0 # this makes high redshifts cleaner, as there are more galaxies
      mi_star_pseudo = 22.9 + 2.54 * np.log(Z_med) # Bz median i-band
      sizing = mi_star_pseudo - factor - bands[mask_ii,2]
      sizing = np.clip(sizing,0,None) # no negative sizes
    # plot points
    plt.scatter(cols[mask_ii,0],cols[mask_ii,1],sizing,c=color,
                cmap=colormap,alpha=.5)
    plt.title(label_ii)
    if labels is None:
      plt.xlabel(lbl_c % 0)
      plt.ylabel(lbl_c % 1)
    else:
      plt.xlabel(labels[0])
      plt.ylabel(labels[1])
    plt.xlim(xlim)
    plt.ylim(ylim)
    if fnames_out is not None:
      plt.savefig(fnames_out % ii)
      print("Saved",fnames_out % ii)
    if display:
      plt.show()
    else:
      plt.clf()
  return


def plot_WMS(rd):
  if rd.__class__ is str:
    # read in dragon from file
    rd = dragon(rd)
  Z = rd._Z_mids
  W,M,S = rd.get_WMS()
  good = ~(rd._exclusions).astype(bool)
  ######################################################################
  # plot MS plane
  for ii in range(rd.N_fit):
    s = 1 + 36 * W[:,ii]
    col = mpl_colors_RF[ii]
    plt.plot(M[good,ii],S[good,ii],alpha=.5,color=col)
    plt.scatter(M[:,ii],S[:,ii],s,color=col)
  plt.xlabel(lbl_M)
  plt.ylabel(lbl_S)
  plt.yscale('log')
  plt.show()
  ######################################################################
  # plot deciding value
  for ii in range(rd.N_fit):
    col = mpl_colors_RF[ii]
    plt.plot(Z,M[:,ii],'-',color=col)
    plt.fill_between(Z,(M-.5*S)[:,ii],(M+.5*S)[:,ii],
                     color=col,lw=0,alpha=.125)
  plt.xlabel('Redshift')
  plt.ylabel(lbl_M + ' $\pm$ ' + r'$\frac{1}{2}$' + lbl_S)
  plt.show()


def plot_cZ(fname,c=1,rd=None,xlim=None):
  " plot_cZ(fname,c=1,rd=None,xlim=None) "
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # read in data
  with h5py.File(fname,'r') as ds:
    Z,bands = [ds[key][:] for key in ['Z','bands']]
    Z_err = ds['Z_err'][:] if 'Z_err' in ds.keys() \
                           else np.zeros_like(Z)
    bands_err = ds['bands_err'][:] if 'bands_err' in ds.keys() \
                                   else None
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # set up red dragon, if input
  if rd is None:
    pred = np.zeros_like(Z)
    cmap = 'bone'
  else:
    c = -1 # set coloring to pred
    if rd.__class__ is str:
      # read in dragon from file
      rd = dragon(rd)
    pred = rd.get_pred(Z,bands,bands_err)
    cmap = cmap_spec
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # augment data for analysis
  cols = -np.diff(bands,axis=1)
  N_gal,N_cols = np.shape(cols)
  col_err = _band_err_to_col_err(bands_err)
  for ii in range(N_cols):
    z = [np.log10(Z_err), np.log10(col_err[:,ii]), np.sum(bands,axis=1), pred][c]
    mn_z,mx_z = np.quantile(z,[.05,.95])
    plt.scatter(Z,cols[:,ii],1,lw=0,alpha=.5,
                c=z,cmap=cmap,vmin=mn_z,vmax=mx_z)
    plt.xlabel('Redshift')
    plt.xlim(xlim)
    ylim = plt.ylim()
    plt.ylim(max(ylim[0],-2),min(ylim[1],5))
    plt.show()
  pass


########################################################################
# main function for default fitting

# Files for my personal use; delete later. 
fdir_wkb = '/global/cscratch1/sd/wkblack/'
fname_Bz = fdir_wkb + 'SkyMaps/buzzard/vet/vetted_gamma.h5'
fname_SDSS = fdir_wkb + 'SDSS/SDSS_Zp3__p4.h5'
fname_SDSS_ext = fdir_wkb + 'SDSS/SDSS_Zp3__5.h5'
fname_DES = fdir_wkb + 'SkyMaps/DES/DES_DEEP.h5'
fname_DES_all = fdir_wkb + 'SkyMaps/DES/DES_DEEP_ALL.h5'

if __name__ == '__main__':
  print("In main...")
  
  if 0: # run Buzzard fitting
    for N_fit in [4]: # range(2,4):
      fit_file(fname_Bz,N_fit,fname_out=f'output/fit_Bz_{N_fit}K.h5')
  
  if 0: # run SDSS fitting
    for N_fit in range(2,5):
      fit_file(fname_SDSS_ext,N_fit,fname_out=f'output/fit_SDSS_{N_fit}K.h5')
  
  if 1: # run DES fitting
    N_fit = 3
    fit_file(fname_DES,N_fit,fname_out=f'output/fit_DES_{N_fit}K.h5',
             Z_max=1,dZ_0=.2,dZ_1=.1)
  
  print('\n~fin\n')
