# show off what's wrong with a flat CC or CM cut
import h5py
import numpy as np
import m_star_model
from matplotlib import pyplot as plt
from sklearn.mixture import GaussianMixture

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# KLLR imports
from kllr import kllr_model
from kllr.regression_model import calculate_weigth as weights
def get_counts(x,xr,kw=.2,kt='gaussian'):
  w = np.zeros(len(xr))
  for ii in range(len(xr)):
      w[ii] = np.sum(weights(x,kernel_type=kt,mu=xr[ii],width=kw))
  return w # number of points in analysis bin

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# set up golden aspect ratio
golden_ratio = (1+np.sqrt(5))/2.
golden_aspect = 5*np.array([golden_ratio,1])

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# set up default plotting paradigms
plt.rcParams['figure.figsize'] = golden_aspect
plt.rcParams['font.size'] = 15

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# jackknife routine

def jackknife_fit(x,y,order=1,w=None):
  " jackknife values and errors on np.polyfit parameters "
  if w is None:
    w = np.ones(len(x))
  pars = np.zeros((order+1,len(x)))
  for ii in range(len(x)):
    pars[:,ii] = np.polyfit(np.delete(x,ii),np.delete(y,ii),
                            order,w=np.delete(w,ii))
  return np.mean(pars,axis=1), np.std(pars,axis=1)


########################################################################
# set up defaults

fname = '/global/cscratch1/sd/wkblack/SkyMaps/buzzard/vet/subset_new_L.h5'
N_fit = 2 # number of Gaussians to fit the data with
frac_N = 1/(1+N_fit)
qtls = np.flip(np.arange(frac_N,.99999,frac_N)) # for guessing means
cmaps = ['autumn','winter','spring','cool'] # RS first


########################################################################
# read in data

f = h5py.File(fname,'r')
# ['coadd_object_id', 'dec', 'flags_gold', 'flux_g', 'flux_i', 'flux_r', 'flux_z', 'haloid', 'hpix_16384', 'ivar_g', 'ivar_i', 'ivar_r', 'ivar_z', 'm200', 'mag_err_g', 'mag_err_i', 'mag_err_r', 'mag_err_z', 'mag_g', 'mag_g_lensed', 'mag_g_true', 'mag_i', 'mag_i_lensed', 'mag_i_true', 'mag_r', 'mag_r_lensed', 'mag_r_true', 'mag_z', 'mag_z_lensed', 'mag_z_true', 'mcal_flux_i', 'mcal_flux_r', 'mcal_flux_z', 'mcal_ivar_i', 'mcal_ivar_r', 'mcal_ivar_z', 'px', 'py', 'pz', 'r200', 'ra', 'redshift_cos', 'rhalo', 'sdss_sedid', 'vx', 'vy', 'vz', 'z', 'zmc_sof', 'zmean_sof']
Z = f['redshift_cos'][:]

from sys import argv
if len(argv)>1:
  Z_curr = float(argv[1])
else: # hardcode
  Z_curr = .25
dZ = .001
Z_lim = [Z_curr - dZ/2., Z_curr + dZ/2.] # center around Z_curr
print(f"z|[{Z_lim[0]:0.4f},{Z_lim[1]:0.4f})")
mask_Z = (Z_lim[0] <= Z) * (Z < Z_lim[1])
Z_med = np.median(Z[mask_Z])
m_star = m_star_model.m_star(Z_med,warnings=False) # use as pivot

c = np.transpose([f['mag_'+col][mask_Z] for col in ['g','r','i','z']])

mask_c = np.any(c==99,axis=1)
c = c[~mask_c]
Z = Z[mask_Z][~mask_c]

g,r,i,z = [c[:,ii] for ii in range(4)]


########################################################################
# measure RS & BC w/ GM

c = np.transpose([g-r,g-i,g-z,r-i,r-z,i-z])
mask_RS = -np.ones(len(Z))
mi_bins = np.concatenate([[min(i)],
                          np.linspace(min(i)+1.4,max(i)-.5,9),
                          [max(i)]])

means_global = GaussianMixture(N_fit).fit(c).means_
means_guess_global = means_global[np.flip(np.argsort(np.sum(means_global,axis=1)))]

mi_vals,fR_vals,gmr_RS,gmr_BC,sig_RS,sig_BC = np.zeros((6,len(mi_bins)-1))

for ii in range(len(mi_bins)-1):
  mask_ii = (mi_bins[ii] <= i) * (i < mi_bins[ii+1])
  mi_vals[ii] = np.median(i[mask_ii])
  if 0: # guess using quantile
    means_guess = np.quantile(c[mask_ii],qtls,axis=0)
  else: # set up using blank slate fitting
    means_guess = means_guess_global
  # fit
  fit = GaussianMixture(N_fit,means_init=means_guess).fit(c[mask_ii])
  x_pred = fit.predict(c[mask_ii])
  mask_RS[mask_ii] = x_pred
  # save fit values
  fR_vals[ii] = fit.weights_[0]
  gmr_RS[ii] = fit.means_[0,0]
  gmr_BC[ii] = fit.means_[1,0]
  sig_RS[ii] = np.sqrt(fit.covariances_[0,0,0])
  sig_BC[ii] = np.sqrt(fit.covariances_[1,0,0])

if 0: # plot vals
  plt.scatter(mi_vals,fR_vals,color='k')
  plt.scatter(mi_vals,gmr_RS,color='r')
  plt.scatter(mi_vals,gmr_BC,color='b')
  plt.scatter(mi_vals,sig_RS,color='r')
  plt.scatter(mi_vals,sig_BC,color='b')
  plt.show()

mask_RS = mask_RS.astype(bool)


########################################################################
# plot up problem

fig, axs = plt.subplots(1, 2, gridspec_kw={'width_ratios': [1, 2]}, sharey=True)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# set up CC space
for ii in range(N_fit):
  axs[0].scatter((r-i)[mask_RS==ii],(g-r)[mask_RS==ii],
                 max(i)-i[mask_RS==ii],c=i[mask_RS==ii],
                 cmap=cmaps[ii],lw=0,alpha=.25)
# set up axes
axs[0].set_xlabel(r'$(r-i)$')
axs[0].set_xlim(.1,1.2)

axs[0].set_ylabel(r'$(g-r)$')
axs[0].set_ylim(.5,3.5) # max(0,ylim[0]),min(5,ylim[1]))

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# set up CM space
for ii in range(N_fit):
  axs[1].scatter(i[mask_RS==ii],(g-r)[mask_RS==ii],max(i)-i[mask_RS==ii],
                 c=i[mask_RS==ii],cmap=cmaps[ii],lw=0,alpha=.25)


#  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  # 
# fit with KLLR

kw = 0.25 # kernel width
lm = kllr_model(kernel_width=kw)
nb = 100 # number of bins
mns,sls,sigs,Ns,errs = np.zeros((5,N_fit,nb))
for ii in range(N_fit): 
  mask = (mask_RS==ii)
  # bounds = np.array([min(i),max(i)]) - m_star
  bounds = np.quantile(i - m_star,[.001,.999])
  xr,yrm,icpt,sl,sig = lm.fit(i[mask]-m_star,(g-r)[mask],bounds,nbins=nb)
  N = get_counts(i[mask] - m_star, xr, kw) # pseudocount
  err = sig/np.sqrt(N) # pseudo error on the mean
  mask = N > 2 # ensure somewhat valid scatter
  x = xr[mask] + m_star
  # save outputs
  mns[ii] = yrm
  sls[ii] = sl
  sigs[ii] = sig
  Ns[ii] = N # save
  errs[ii] = err
  if 1: # plot onto CM space
    axs[1].plot(x,yrm[mask],'k')
    axs[1].fill_between(x,(yrm-sig)[mask],(yrm+sig)[mask],
                        alpha=.125,color='k',lw=0)
    axs[1].fill_between(x,(yrm-err)[mask],(yrm+err)[mask],
                        alpha=.125,color='r',lw=0)
    axs[1].fill_between(x,(yrm-2*err)[mask],(yrm+2*err)[mask],
                        alpha=.125,color='r',lw=0)

# calculate red fraction
fR_fit = Ns[0]/np.sum(Ns,axis=0)

# set up axes
print("Setting up axes")
axs[0].set_xlim(np.quantile(r-i,.001)-.1,np.quantile(r-i,.99)+.1)
axs[0].set_ylim(np.quantile(g-r,.001)-.1,np.quantile(g-r,.99)+.1)
axs[1].set_xlabel(r'$m_i$')
axs[1].set_xlim(None,max(i))

fig.tight_layout()
if 0:
  fname_out = 'cf__CC_CM.pdf'
  plt.savefig(fname_out,format='pdf',dpi=1000)
  print(f"Saved {fname_out}")
plt.show()

########################################################################
# plot fit constants

fig, axs = plt.subplots(1, 3, sharex=True)

axs[0].scatter(mi_vals,fR_vals,color='r')
axs[0].plot(xr+m_star,fR_fit,color='r')
axs[0].set_ylim(0,1)
axs[0].set_ylabel(r'$f_R$')

# still could plot slopes... 
# xr,mns,sls,sigs,Ns,errs

for ii in range(2):
  col = ['r','b'][ii]
  # <g-r>
  axs[1].set_ylabel(r'$\langle g-r \rangle$')
  gmr = [gmr_RS,gmr_BC][ii]
  axs[1].scatter(mi_vals,gmr,color=col)
  axs[1].plot(xr+m_star,mns[ii],color=col)
  axs[1].fill_between(xr+m_star,mns[ii]-errs[ii],mns[ii]+errs[ii],
                      color=col,alpha=.125,lw=0)
  # scatter
  axs[2].set_ylabel(r'$\sigma_{(g-r)}$')
  sig = [sig_RS,sig_BC][ii]
  axs[2].scatter(mi_vals,sig,color=col)
  axs[2].plot(xr+m_star,sigs[ii],color=col)
  axs[2].fill_between(xr+m_star,sigs[ii]-errs[ii],sigs[ii]+errs[ii],
                      color=col,alpha=.125,lw=0)

for ii in range(3):
  axs[ii].set_xlabel(r'$m_i$')
  ylim = axs[ii].get_ylim()
  axs[ii].plot(np.ones(2)*m_star,ylim,'--',color='grey')
  axs[ii].set_ylim(ylim)

plt.tight_layout()
plt.show()

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# calculate fit constants
print('#'*72)
print(f"Fit parameters for z|[{Z_lim[0]:0.4f},{Z_lim[1]:0.4f}) ~ {Z_med:0.4f}:")
# fR
w = 1/np.sum(errs**2,axis=0)
out = [[fR_a0,fR_b0],[fR_a0e,fR_b0e]] = jackknife_fit(mi_vals-m_star,fR_vals)
print('fR init fit:',out)
out = [[fR_aK,fR_bK],[fR_aKe,fR_bKe]] = jackknife_fit(xr,fR_fit,1,np.sqrt(w))
print('fR KLLR fit:',out)

# <g-r>_RS
out = [[gmr_RS_a0,gmr_RS_b0],[gmr_RS_a0e,gmr_RS_b0e]] = \
      jackknife_fit(mi_vals-m_star,gmr_RS)
print('<g-r>_RS init:',out)
out = [[gmr_RS_aK,gmr_RS_bK],[gmr_RS_aKe,gmr_RS_bKe]] = \
      jackknife_fit(xr,mns[0],1,1/errs[0])
print('<g-r>_RS KLLR:',out)
w = 1/errs[0]**2
out = gmr_RS_aS, gmr_RS_aSe = np.sum(sls[0]*w)/np.sum(w),np.sqrt(np.sum(1/w))
print('d<g-r>_RS/dm_i KLLR:',out)
# <g-r>_BC
out = [[gmr_BC_a0,gmr_BC_b0],[gmr_BC_a0e,gmr_BC_b0e]] = \
      jackknife_fit(mi_vals-m_star,gmr_BC)
print('<g-r>_BC init:',out)
out = [[gmr_BC_aK,gmr_BC_bK],[gmr_BC_aKe,gmr_BC_bKe]] = \
      jackknife_fit(xr,mns[1],1,1/errs[1])
print('<g-r>_BC KLLR:',out)
w = 1/errs[1]**2
out = gmr_BC_aS, gmr_BC_aSe = np.sum(sls[1]*w)/np.sum(w),np.sqrt(np.sum(1/w))
print('d<g-r>_BC/dm_i KLLR:',out)

# scatter
out = [[lg_sig_RS_a0,lg_sig_RS_b0],[lg_sig_RS_a0e,lg_sig_RS_b0e]] = \
      jackknife_fit(mi_vals-m_star,np.log10(sig_RS))
print('lg sig_RS init:',out)
out = [[lg_sig_RS_aK,lg_sig_RS_bK],[lg_sig_RS_aKe,lg_sig_RS_bKe]] = \
      jackknife_fit(xr,np.log10(sigs[0]),1,sigs[0]/errs[0])
print('lg sig_RS KLLR:',out)
out = [[lg_sig_BC_a0,lg_sig_BC_b0],[lg_sig_BC_a0e,lg_sig_BC_b0e]] = \
      jackknife_fit(mi_vals-m_star,np.log10(sig_BC))
print('lg sig_BC init:',out)
out = [[lg_sig_BC_aK,lg_sig_BC_bK],[lg_sig_BC_aKe,lg_sig_BC_bKe]] = \
      jackknife_fit(xr,np.log10(sigs[1]),1,sigs[0]/errs[1])
print('lg sig_BC KLLR:',out)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# save output

fname_out = f"output_md3m/md3m_Z{Z_med:0.4f}.h5"
with h5py.File(fname_out,'w') as ds:
  for label,dset in zip(\
    ['fR_a','fR_a_err','fR_b','fR_b_err',
     'gmr_RS_a','gmr_RS_a_err','gmr_RS_b','gmr_RS_b_err',
     'gmr_BC_a','gmr_BC_a_err','gmr_BC_b','gmr_BC_b_err',
     'lg_sig_RS_a','lg_sig_RS_a_err','lg_sig_RS_b','lg_sig_RS_b_err',
     'lg_sig_BC_a','lg_sig_BC_a_err','lg_sig_BC_b','lg_sig_BC_b_err'
    ],
    [[fR_a0,fR_aK],[fR_a0e,fR_aKe],[fR_b0,fR_bK],[fR_b0e,fR_bKe],
     [gmr_RS_a0,gmr_RS_aK,gmr_RS_aS],[gmr_RS_a0e,gmr_RS_aKe,gmr_RS_aSe],
     [gmr_RS_b0,gmr_RS_bK],[gmr_RS_b0e,gmr_RS_bKe],
     [gmr_BC_a0,gmr_BC_aK,gmr_BC_aS],[gmr_BC_a0e,gmr_BC_aKe,gmr_BC_aSe],
     [gmr_BC_b0,gmr_BC_bK],[gmr_BC_b0e,gmr_BC_bKe],
     [lg_sig_RS_a0,lg_sig_RS_aK],[lg_sig_RS_a0e,lg_sig_RS_aKe],
     [lg_sig_RS_b0,lg_sig_RS_bK],[lg_sig_RS_b0e,lg_sig_RS_bKe],
     [lg_sig_BC_a0,lg_sig_BC_aK],[lg_sig_BC_a0e,lg_sig_BC_aKe],
     [lg_sig_BC_b0,lg_sig_BC_bK],[lg_sig_BC_b0e,lg_sig_BC_bKe]
    ]):
    ds.create_dataset(label,data=dset)
  # create attributes
  for label,attr in zip(['Z_med','Z_lim','Z_curr','m_star','dZ'],
                        [ Z_med , Z_lim , Z_curr , m_star , dZ ]):
    ds.attrs[label] = attr

print(fname_out,"created.\n")

