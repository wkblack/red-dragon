# reading in actual TNG data, instead of the SAM LGalaxies calculated
# imports

import h5py

from glob import glob
exec(open("mpl_tools.py").read()) # import matplotlib tools and defaults
from m_star_model import m_star as R14


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# cosmology bits
h = .7 # unitless Hubble constant
H0 = 100 * h # (km/s)/Mpc
c = 299792.458 # km/s

Mf = 10**10/h # Msol, factor all masses in TNG are divided by


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# import TNG dataset for a given redshift

Z = 0.1

TNG_snaps = {
  0.5 : "067",
  0.4 : "072",
  0.3 : "078",
  0.2 : "084",
  0.1 : "091",
  0.0 : "099"
}

snap = TNG_snaps[Z]

f_dir = f'/global/cscratch1/sd/wkblack/TNG300-1/groupcats/{snap}/'
f_tmp = f_dir + f"fof_subhalo_tab_{snap}.*.hdf5" # template fname

fname_vet = f_dir + f"vetted.h5"


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# caltulate kink point in luminosity function

m_star = R14(Z) # mag, apparent; fitting from Buzzard paper, apx 20.257
mu = 25 + 5 * np.log10(c*Z/H0) # distance modulus; apx 42 for z=.5
emstar = m_star - mu # absolute mag; characteristic luminosity m_*


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# combine data

# commands for single file: 
# M_gas_wind, M_DM, _, M_tracers, M_stars, M_BH = np.transpose(f['Subhalo/SubhaloMassType'][()])
# U,B,V,K,g,r,i,z = np.transpose(f['Subhalo/SubhaloStellarPhotometrics'][()])
# SFR = f['Subhalo/SubhaloSFR'][()]

def read_files(fnames):
  """
  read in data from fnames
  return stellar masses, photometric bands, and specific star formation rates
  """
  loc_Mass = 'Subhalo/SubhaloMassType'
  M_star, bands, SFR = [], np.empty((0,8)), []
  # iterate through all datasets
  for fname in fnames:
    with h5py.File(fname,'r') as ds:
      if 'SubhaloMassType' not in ds['Subhalo'].keys():
        print("NOTE: Empty file:",fname)
        continue
      masses = ds[loc_Mass][()][:,4] # (no wind)
      band_vals = ds['Subhalo/SubhaloStellarPhotometrics'][()] # mag 
      SFRs = ds['Subhalo/SubhaloSFR'][()] # Msol/yr
      # concatenate
      mask = masses > 0
      M_star = np.append(M_star,masses[mask],axis=0)
      bands = np.append(bands,band_vals[mask],axis=0)
      SFR = np.append(SFR,SFRs[mask],axis=0)
  # tidy up and complete
  M_star *= Mf # Msol
  sSFR = SFR/M_star # per year
  return M_star, np.transpose(bands), sSFR


if 0: # load all files individually
  fnames = glob(f_tmp)
  if len(fnames) < 1:
    print("ERROR: No files found.")
    assert(1==0)
  else:
    print(f"Loading {len(fnames)} files...")
    # load files 
    M_star, bands, sSFR = read_files(fnames)
    U,B,V,K,g,r,i,z = bands
    print("Files loaded.")
else:
  print("Loading vetted file...")
  with h5py.File(fname_vet,'r') as ds:
    g,r,i,z = [ds[c][:] for c in ['g','r','i','z']]
    M_star, sSFR = [ds[key][:] for key in ['M_star','sSFR']]
  print("Vetted file loaded.")


bands_des = g,r,i,z


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# add error to observational bands

def simulate_error(bands):
  """
  return synthetic errors based on mean error at a given magnitude
  
  Parameters
  ----------
  bands = [g, r, i, z] (true apparent magnitude)
  
  Returns
  -------
  measurement errors = [g_err, r_err, i_err, z_err] mag
  """
  ZP = 22.5 # 'zero point' magnitude from Buzzard
  a = 1/3. # pm ~.004; linear slope of log error
  lg_eps = [-1.34898794, -1.27240629, -1.08144321, -0.86578751] # Z=0.5
  lg_eps_err = [0.00764304, 0.00389136, 0.00470332, 0.00580421] # ditto
  b = np.outer(lg_eps,np.ones(len(bands[0])))
  x = bands - ZP
  return 10**(a*x + b)


def apply_error(bands):
  " approximate error as Gaussian normal about zero "
  err_mag = simulate_error(bands)
  return bands + np.random.normal(0,err_mag)


bands_true = gt,rt,it,zt = bands_des + mu # apparent magnitude
bands_err = ge,re,ie,ze = simulate_error(bands_true)
bands_obs = go,ro,io,zo = apply_error(bands_true)


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# vet data

def vet(i_max=-19.25+mu):
  """
  create vetted dataset
  i_max = -19.25 + mu, apx max of m_star + 1.75 for z|[.05,.70]
                      (increases by ~.5 from z=.1 to .5)
                      (increases by ~1 from z=.05 to .7)
  """
  mask = np.maximum(it,io) < i_max
  assert(len(mask[mask])>0)
  with h5py.File(fname_vet,'w') as ds:
    for tag,dat in zip(['U','B','V','K','g','r','i','z',
                        'M_star', 'sSFR'],
                       [ U,  B,  V,  K,  g,  r,  i,  z, 
                         M_star,   sSFR ]):
      ds.create_dataset(tag,data=dat[mask])
    ds.attrs['Z'] = Z # redshift
    ds.attrs['mu'] = mu # distance modulus
    ds.attrs['m_star (apparent)'] = m_star
    ds.attrs['m_star (absolute)'] = emstar
  print(fname_vet,'created.')


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# analyze data

lg_sSFR_label = r'$\log_{10} {\rm sSFR \cdot yr}$' # label for sSFR


def plot_sSFR_CM(log_color=False):
  # set up colorscheme
  if log_color:
    x = np.log10(sSFR)
  else:
    x = sSFR > 10**-11
  # plot
  plt.scatter(i,g-r,2,lw=0,alpha=.25,c=x,cmap='coolwarm_r')
  plt.colorbar(label=lg_sSFR_label)
  plt.xlabel(r'$m_i$')
  plt.ylabel(r'$g-r$')
  plt.tight_layout()
  plt.show()


if __name__ == '__main__':
  plot_sSFR_CM()
  # vet()
  print('~fin')
