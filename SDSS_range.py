"""
fitting for SDSS Z|(.3,.4), comparing Buzzard vs 1D cuts
"""
import rd_gamma

import KLC

from fit_CCM import Classification

exec(open("mpl_tools.py").read()) # import matplotlib tools and defaults

dir_SDSS = '/global/cscratch1/sd/wkblack/SDSS/'
if 1: # larger redshift range
  fname_csv = dir_SDSS + 'SDSS_Zp3__5.csv'
  fname_h5 = dir_SDSS + 'SDSS_Zp3__5.h5'
else: # .3 to .4
  fname_csv = dir_SDSS + 'SDSS_Zp3__4.csv'
  fname_h5 = dir_SDSS + 'SDSS_Zp3__p4.h5'

fname_rd = 'output/fit_SDSS_2K.h5'

rd = rd_gamma.dragon(fname_rd)

import h5py
with h5py.File(fname_h5,'r') as ds:
  Z,Z_err,bands,bands_err = [ds[key][:] for key in ds.keys()]


P_red_wyvren = rd.wyvren_pred1D(Z,bands,bands_err)


cols = -np.diff(bands,axis=1)
if 0:
  Q1 = cols[:,1] > 1.4 # g-r apx
  Q2 = cols[:,2] > .51 # r-i apx
else:
  Q0 = P_red_wyvren[:,0] > .5 # u-g
  Q1 = P_red_wyvren[:,1] > .5 # g-r
  Q2 = P_red_wyvren[:,2] > .5 # r-i
  Q3 = P_red_wyvren[:,3] > .5 # i-z

import pandas as pd
df = pd.read_csv(fname_csv,skiprows=1)
sSFR = df['ssfr'].values - 9 # per year
Q = sSFR < -11 + Z # a bit simple, but should work.

Z_bins = np.arange(.3,.4+.005,.01)
Z_mids = (Z_bins[1:] + Z_bins[:-1])/2.

# set up RS, as selected by RD
P_red = rd.P_red(Z,bands,bands_err)
RS_rd = P_red > .5

Qs = [RS_rd,Q1,Q2,Q0,Q3]
lbls = ['RD','$g-r$','$r-i$','$u-g$','$i-z$']
cols = ['k',*mpl_colors[:4]]

def fit_range(kw=.0125,jj_max=5,fname_out='SDSS_bACC.pdf',
              N_runs=50,N_points=50,xlim=None):
  " continuous fitting w/ Bootstrap error "
  for jj in range(jj_max):
    print(f"Fitting {lbls[jj]} ({jj+1}/{jj_max})")
    RS = Qs[jj]
    xv,klc = KLC.KLC(Z,Q,RS,kw,N_runs=N_runs,N_points=N_points)
    TP, FP, FN, TN = [klc[:,:,ii] for ii in range(4)]
    TPR = TP / (TP + FN)
    TNR = TN / (TN + FP)
    bACC = (TPR + TNR)/2. 
    # plot result
    mn,st = [f(bACC,axis=1) for f in [np.mean,np.std]]
    plt.plot(xv,mn,cols[jj],label=lbls[jj])
    plt.fill_between(xv,mn-st,mn+st,color=cols[jj],alpha=.125,lw=0)
  print("Tidying up for display")
  plt.xlabel('Redshift')
  plt.ylabel('Balanced Accuracy')
  plt.legend()
  if fname_out is not None:
    plt.savefig(fname_out)
    print("Saved",fname_out)
  if xlim is None:
    xlim = np.quantile(Z,[0,1])
    plt.xlim(np.round(xlim,3))
  else:
    plt.xlim(xlim)
  plt.show()


def plot_significance(kw=.0125,jj_max=5,fname_out='SDSS_signif.pdf',
                      N_runs=50,N_points=50,xlim=None):
  " significance of above fitting "
  # grab RD as baseline
  jj = 0
  print(f"Fitting {lbls[jj]} ({jj+1}/{jj_max})")
  RS = Qs[jj]
  xv,klc = KLC.KLC(Z,Q,RS,kw,N_runs=N_runs,N_points=N_points)
  TP, FP, FN, TN = [klc[:,:,ii] for ii in range(4)]
  TPR = TP / (TP + FN)
  TNR = TN / (TN + FP)
  bACC = (TPR + TNR)/2. 
  mn_RD,st_RD = [f(bACC,axis=1) for f in [np.mean,np.std]]
  # compare to other fields
  for jj in range(1,jj_max):
    print(f"Fitting {lbls[jj]} ({jj+1}/{jj_max})")
    RS = Qs[jj]
    xv,klc = KLC.KLC(Z,Q,RS,kw,N_runs=N_runs,N_points=N_points)
    TP, FP, FN, TN = [klc[:,:,ii] for ii in range(4)]
    TPR = TP / (TP + FN)
    TNR = TN / (TN + FP)
    bACC = (TPR + TNR)/2. 
    # plot result
    mn,st = [f(bACC,axis=1) for f in [np.mean,np.std]]
    sig = (mn - mn_RD)/np.sqrt(st_RD**2 + st**2)
    plt.plot(xv,sig,cols[jj],label=lbls[jj])
  print("Tidying up for display")
  plt.xlabel('Redshift')
  plt.ylabel('Significance')
  plt.legend()
  if fname_out is not None:
    plt.savefig(fname_out)
    print("Saved",fname_out)
  if xlim is None:
    xlim = np.quantile(Z,[0,1])
    plt.xlim(np.round(xlim,3))
  else:
    plt.xlim(xlim)
  plt.plot(xlim,ones,'k')
  plt.show()
  pass

# well, this isn't a terribly pretty result. 
# makes it seem like these Gaussians don't work all that well... 
# really, why does a static dumb g-r cut work better than a Gaussian mixture 
# at selecing the quenche dpopulation\rhet 

# SDSS here only has extremely bright galaxies---not the faint ones which would induce more scatter (I think).

if __name__ == '__main__':
  # fit_range(.02,3)
  plot_significance(.02,3)
