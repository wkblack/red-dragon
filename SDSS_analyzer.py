# analyze the color distribution from SDSS
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt

# read in file
f_dir = "/mnt/c/Users/maill/Downloads/tmp/"
f_tmp = "SDSS_Zp%i_all.csv"
zv = 0.1 # redshift of file
fname = f_dir + f_tmp % (10*zv)

df = pd.read_csv(fname,skiprows=1)
objid,ra,dec,Z,err_Z,u,g,r,i,z,err_u,err_g,err_r,err_i,err_z, \
 SFR,SFR_min,SFR_max,logMass,minLogMass,maxLogMass \
  = [df[key].values for key in df.keys()]

sSFR = SFR/10**logMass # 1/yr; specific star formation rate
sSFR_max = SFR_max/10**minLogMass
sSFR_min = SFR_min/10**maxLogMass

# set up sSFR masks
pivot = 10**-11.3
mrr = mask_really_red = sSFR_max < pivot
mrd = mask_red = (~mask_really_red) * (sSFR < pivot)
msr = mask_sorta_red = (~mask_really_red) * (~mask_red) * (sSFR_min < pivot)
mrb = mask_really_blue = (~mask_really_red) * (~mask_red) * (~mask_sorta_red)
masks = [mrr,mrd,msr,mrb] # compilation of masks


########################################################################
# functions for analysis

lbl_lg_sSFR = r'$\log_{10} \, {\rm sSFR} \cdot {\rm yr}$'
lbl_Z = r'z|[%0.3g,%0.3g)' % (zv-.005,zv+.005)

def plot_sSFR(N=80,plot_many=False,plot_err=False):
  x_min,x,x_max = [np.log10(s[s>0]) for s in [sSFR_min,sSFR,sSFR_max]]
  xlim = plt.xlim(min(min(x_min),min(x)),-8) # max(x_max))
  bins = np.linspace(*xlim,N)
  if plot_many:
    for ii in range(10):
      bins_ii = np.linspace(*xlim,2**ii)
      plt.hist(x,bins_ii,alpha=16/256,color='b',log=True)
  else:
    n,b,p = plt.hist(x,bins,color='k',histtype='step',log=True)
    if plot_err:
      plt.hist(x_min,bins,color='r',histtype='step',log=True)
      plt.hist(x_max,bins,color='b',histtype='step',log=True)
  # tidy up and display
  plt.xlabel(lbl_lg_sSFR)
  plt.ylim(None,len(Z)) # set max at total count of data
  plt.tight_layout()
  plt.show()


def plot_CC(pivot=10**-11.3,graded_colors=False,
            colors=['r','orange','g','b'],
            sizes=[1,1,3,2]):
  x,y = g-r,r-i
  if graded_colors: # https://www.colorhexa.com/ff0000-to-0000ff
    for ii in range(4):
      x_ii,y_ii = [v[masks[ii]] for v in [x,y]]
      plt.scatter(x_ii,y_ii,sizes[ii],lw=0,alpha=.125,color=colors[ii])
  else:
    plt.scatter(x,y,2,lw=0,alpha=.125,c=sSFR<pivot,cmap='coolwarm')
  # set up axes
  plt.xlabel(r'$g-r$')
  plt.xlim(np.quantile(x,[.01,.99]))
  plt.ylabel(r'$r-i$')
  plt.ylim(np.quantile(y,[.01,.99]))
  plt.title(lbl_Z)
  plt.tight_layout()
  plt.show()


if __name__ == '__main__':
  # plot_sSFR(plot_err=True)
  plot_CC(graded_colors=True) #,colors=['r','w','w','b'])
