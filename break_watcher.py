# read in photometric bands from file, find redshifts where 4kA break passes through them
import numpy as np

if 1: # Sloan filters
  fdir = '/global/u1/w/wkblack/easyGalaxy/ezgal/data/filters/'
  bands = ['u','g','r','i','z']
  fend = ['sloan_'+c for c in bands]
  files = [fdir + end for end in fend]
  all_data = [np.genfromtxt(f, dtype=None, delimiter=[8,20], 
              autostrip=True) for f in files]
else:
  pass # DES filters


exec(open("mpl_tools.py").read()) # import matplotlib tools and defaults

z_vals = [[],[],[],[],[],[]] # compilation of redshift values for all band ends


plotting = True

if 1: # do both
  band_breaks = [3645, 4000] # Balmer, 4k
else:
  band_breaks = [3645 if 0 else 4000]


if 1:
  for ii in range(len(all_data)-1):
    # look at when equal point between two curves is redshifted from 4kA
    x1,y1 = np.transpose(all_data[ii])
    x2,y2 = np.transpose(all_data[ii+1])
    x_min = max(min(x1),min(x2)) # lowest point shared by both
    x_max = min(max(x1),max(x2)) # largest point shared by both
    x_vals = np.linspace(x_min,x_max,1000) # points to check
    v1 = np.interp(x_vals,x1,y1) # interpolated first band
    v2 = np.interp(x_vals,x2,y2) # interpolated second band
    diff = np.abs(v1-v2)
    x_eq = x_vals[diff==min(diff)][0]
    print(f"equal point of {bands[ii]},{bands[ii+1]}: lambda_obs = {x_eq:.1f} Angstrom")
    for val in band_breaks:
      z = (x_eq - val) / val # redshift
      if plotting:
        plt.scatter(z,0,alpha=.25,color=mpl_colors[ii+1],lw=0)
      z_vals[ii+1] += [z]
      print(f"\tfor lambda_0 = {val} Angstrom, z = {z:.3f}")
    print()


if 1:
  for ii in range(len(all_data)):
    # look at single band half maximum containment
    x1,y1 = np.transpose(all_data[ii])
    xv = np.linspace(min(x1),max(x1),1000)
    yv = np.interp(xv,x1,y1) # interpolated band
    for lim in [.5,.68]:#,.95]:
      x_in = xv[yv >= max(yv)*(1-lim)] # wavelengths > band half maximum
      print(f"For band {bands[ii]}:")
      for val in band_breaks:
        z = (x_in - val) / val # redshifts
        mn,mx = np.quantile(z,[0,1])
        z_vals[ii] += [mn]
        z_vals[ii+1] += [mx]
        if plotting:
          plt.plot((xv-val)/val,yv,10,color=mpl_colors[ii])
          plt.scatter(mn,0,alpha=.25,color=mpl_colors[ii],lw=0)
          plt.scatter(mx,0,alpha=.25,color=mpl_colors[ii+1],lw=0)
        print(f"\tfor lambda_0 = {val} Ang, z|[{mn:0.3f},{mx:0.3f}]")


if plotting: # plot bands
  plt.gca().get_yaxis().set_visible(False)
  plt.xlabel('Redshift')
  plt.ylim(-.01,.5)
  plt.show()


for zv in z_vals:
  print(np.round([np.mean(zv),np.std(zv)],3))

