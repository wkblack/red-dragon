# do an MCMC -esque fitting for hard cuts on CM & CC spaces
# to find optimized parameters, errors on those parameters, and
# errors on bACC for that fitting
# 8 Oct 2021, W.K.Black

########################################################################
# import statements

exec(open("mpl_tools.py").read()) # import matplotlib tools and defaults
import emcee
from scipy.special import erf
from scipy.optimize import minimize


########################################################################
# read in data 
# include quenched probabilities

def get_SDSS():
  pass


def get_TNG():
  pass


########################################################################
# routines for analyzing in different spaces

def bACC(t,p):
  # true,predicted
  return .5 * ( sum(t*p)/sum(t) + sum((1-t)*(1-p))/sum(1-t) )


def P_above_line(theta,x,y,x0,y0,x_err=0,y_err=0):
  """
  give probability that a given set of points (x,y) with errors
  are above a line y0 + m * (x - x0) + b
  
  parameters: 
  P_above_line(theta,x,y,x0,y0,x_err=0,y_err=0)
  """
  # unpack variables
  m,b = theta
  if not hasattr(x_err,'__len__'):
    x_err = np.ones_like(x).astype(float)*x_err
  if not hasattr(y_err,'__len__'):
    y_err = np.ones_like(y).astype(float)*y_err
  x,y,x0,y0,x_err,y_err = [np.array(v) for v in [x,y,x0,y0,x_err,y_err]]
  # check which points have means above line
  mask_above = (y > y0 + m * (x - x0) + b).astype(bool)
  P_above = mask_above.astype(float)
  mask_err = np.logical_or(x_err>0,y_err>0)
  if len(mask_err[mask_err]) > 0:
    # calculate how many sigma x & y are from the line (inverted)
    inv_Nsigx = x_err/(x-(x0+(y-y0-b)/m)) # 1/(x sigmas)
    inv_Nsigy = y_err/(y-(y0+(x-x0)*m+b)) # 1/(y sigmas)
    # find number of sigmas from line, in direction of maximum error 
    Ntot = (inv_Nsigx**2+inv_Nsigy**2)[mask_err]**-(1/2) # sigmas from line
    # correct for above vs below
    P_above[mask_err] = .5*(1+erf(Ntot/np.sqrt(2)))
    P_above[mask_err*~mask_above] = 1 - P_above[mask_err*~mask_above]
  return P_above


def prior(theta):
  m,b = theta
  if (m<-10) or (m>10) or (b<-10) or (b>10):
    return -np.inf
  else:
    return 0


def line_likelihood(theta,x,y,x0,y0,x_err,y_err,P_truth):
  # cheat the tools, use bACC
  P_pred = P_above_line(theta,x,y,x0,y0,x_err,y_err)
  return bACC(P_truth,P_pred) + prior(theta)


def fit_line(x,y,P_truth,x_err,y_err,N_runs=100):
  # fit (x,y) data using `P_truth` as goal for optimizing
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # optimize starting point
  nll = lambda *args: -line_likelihood(*args)
  x0,y0 = [np.mean([np.mean(v[m]) for m in [P_truth>=.5,P_truth<.5]]) for v in [x,y]]
  initial = np.random.random(2)/100
  arg_list = (x,y,x0,y0,x_err,y_err,P_truth)
  soln = minimize(nll, initial, args=arg_list)
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # run emcee
  N_walkers,N_dim = 32, len(soln.x)
  pos = soln.x * (1 + .01 * np.random.randn(N_walkers,N_dim))
  sampler = emcee.EnsembleSampler(N_walkers,N_dim,line_likelihood,args=arg_list)
  out = sampler.run_mcmc(pos, N_runs, progress=True)
  # from spread of output parameters, estimate bACC
  # or just get it from 'likelihood' output?
  # out.log_prob ?
  pass
  

########################################################################

def fit_CM(data):
  # fit input data in CM space
  pass


def fit_CC(data):
  # fit input data in CC space
  pass


########################################################################
# main routines

