# short program to look at accuracy in getting true mean and scatter
exec(open("mpl_tools.py").read()) # import matplotlib tools and defaults
if 1:
  N_boot = 10**4
else: # for testing
  N_boot = 50

means,scats = np.zeros((2,20,N_boot))

mu_true,sig_true = 0,1
# mu_true,sig_true = 12,3

for ii in range(2,20): # N samples
  for jj in range(N_boot): # bootstrap
    vals = np.random.normal(mu_true,sig_true,ii)
    # means[ii,jj],scats[ii,jj] = [f(vals) for f in [np.mean,np.std]]
    means[ii,jj] = np.mean(vals)
    scats[ii,jj] = np.std(vals,ddof=1)


plt.errorbar(np.arange(20)-.05,np.mean(means,axis=1),np.std(means,axis=1),fmt='o')
mn,st = [f(scats,axis=1) for f in [np.mean,np.std]]
for N_sig in [1,2,3]:
  plt.errorbar(np.arange(20)+.05,mn,N_sig*st,lw=1/N_sig,fmt='o',
               capsize=5/N_sig,color=mpl_colors[1])

xlim = plt.xlim(1.5,19.5)
plt.plot(xlim,ones*mu_true,'k')
plt.plot(xlim,ones*sig_true,'k')

# Poisson expectations
xv = np.linspace(*xlim,100)
plt.plot(xv,mu_true-sig_true/np.sqrt(xv),':',color=mpl_colors[0])
plt.plot(xv,mu_true+sig_true/np.sqrt(xv),':',color=mpl_colors[0])

plt.plot(xv,sig_true/np.sqrt(xv),':',color=mpl_colors[0])

plt.xlabel(r"$N_{\rm samples}$")
plt.ylabel(r"bootstrap results")
plt.show()


mn,st = [f(np.log10(scats),axis=1) for f in [np.nanmean,np.nanstd]]
plt.errorbar(range(20),mn,st,fmt='o',color=mpl_colors[1])
xlim = plt.xlim(1.5,19.5)
plt.plot(xlim,ones*np.log10(sig_true),'k')
plt.plot(xv,np.log10(sig_true)-.5*np.log10(xv),':',color=mpl_colors[0]) # decimal log Poisson error
ylim = plt.ylim(plt.ylim())
for ii in range(5):
  plt.fill_between(range(20),mn-(ii+1)*st,mn+(ii+1)*st,alpha=.25/(ii+1),color=mpl_colors[1],lw=0)

plt.xlabel(r"$N_{\rm samples}$")
plt.ylabel(r'$\log_{10} \sigma$')
plt.show()


