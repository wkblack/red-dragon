import numpy as np
exec(open("mpl_tools.py").read())

import fit_CCM
data = fit_CCM.read_SDSS()
sSFR = data['sSFR']
bands = u,g,r,i,z = [data[c] for c in ['u','g','r','i','z']]
X = np.transpose(bands)

band_errs = [data['err_'+c] for c in ['u','g','r','i','z']]
X_err = np.transpose(band_errs)

x = [3551,4686,6166,7480,8932] # Angstrom; band centers for SDSS
x = np.array(x)/1e4 # renormalize to micrometers

N_gals = len(sSFR)
N_order = 5

lg_sSFR_min = -11 - np.log10(60) # since max sSFR ~ 60 * 1e-11
lg_sSFR = np.log10(sSFR[:N_gals])
lg_sSFR[lg_sSFR<lg_sSFR_min] = lg_sSFR_min

zf_vals = np.zeros((N_gals,N_order))

for ii in range(N_gals):
  R = X[ii]
  err = X_err[ii]
  for jj in range(N_order):
    # plt.scatter(x,R)
    zf = np.polyfit(x,R,jj,w=1/err)
    zf_vals[ii,jj] = zf[0]
    # plt.plot(x,np.poly1d(zf)(x))
    R -= np.poly1d(zf)(x)
    # plt.show()
  # assert(1==0)


labels = ['offset','slope','curvature','jolt','snap','crackle','pop']

fig,axs = plt.subplots(1, N_order, sharex=True, figsize=double_width)
axs[0].set_ylabel('magnitude')
for ii in range(N_order):
  ax = axs[ii]
  zf = zf_vals[:,ii]
  ax.scatter(lg_sSFR,zf,2,lw=0,alpha=.125)
  ax.set_xlabel(fit_CCM.lbl_lg_sSFR)
  ax.set_title(labels[ii])
  mask = lg_sSFR < -11
  ax.errorbar(-12,np.mean(zf[mask]),np.std(zf[mask]),color='r',fmt='o')
  ax.errorbar(-10,np.mean(zf[~mask]),np.std(zf[~mask]),color='b',fmt='o')
  ylim = ax.set_ylim(np.quantile(zf,[.01,.99]))
  ax.plot(-11*ones,ylim,'grey')

fname_out = 'snap_crackle_pop.pdf'
plt.savefig(fname_out)
print('Saved',fname_out)
plt.show()


